# Marketplace application

This repository contains a full stack marketplace application, tailored for selling fresh produce.
The programs provide the necessary means to manage the products and orders (on the backend) as well as
a web application for the customers to browse offering and place orders. The project is split into
4 programs (user account service, products service, shopping cart service and a user-facing web application),
which use gPRC as communication protocol.

## Tecnology stack and dependencies

The 4 applications are written in Go. The common PostgreSQL databse is used for storing
persistent data (in 3 non user-facing applications) and Redis is applied for managing
sessions and rate limiting. The web application uses Go standard library for rendering HTML.
On the frontend, HTMX and Alpine.js are used for dynamic content and interactivity.
The web application is styled using plain CSS.

I have tried to program the applications in such a way that they don't store any state, so they
should be freely runnable in a cluster and can be scaled vertically as needed, as long as
the common database and Redis instances are used.

The web application does not handle TLS and so it is recommended to use a reverse proxy	
(e.g. nginx) to handle security.

## Project structure

Since the project is split into 4 applications that can be run independently, all the
relevant files are located in the corresponding directories. The following is the structure:
- /frontend: Styles for the application and static pages I've used to make Go templates
- /web: User-facing application
- /user-account-service: Application responsible for user accounts, authorization and related
- /goods-and-suppliers: Application responsible for managing suppliers, products and orders
- /shopping-cart: Application responsible for managing shopping carts and orders
- /sql: SQL scripts for creating the database and tables

The protobuf definitions are contained in the corresponding backend applications.

## Running the project

The following general steps are needed to be undertaken to run the project:

1. Install and run PostgreSQL and Redis on your backend server. Set it up to have the structure
   as defined in the SQL scripts.
2. [Install Go](https://golang.org/doc/install) on your machine
3. Configure the web server to your liking
4. Build the applications using `go build` in the corresponding directories
5. Run the applications using `./<app_name>` on your backend server

Since the Go produces static binaries, you can just copy them to your server and run them there.
This is a very general description, so head to the corresponding directories for more information.

## Contributing

Please contact me before contributing

## License

[AGPLv3](LICENSE)