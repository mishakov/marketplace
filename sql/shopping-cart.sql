/*  Marketplace application; database schemas and functions
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

CREATE TABLE IF NOT EXISTS user_product_lists (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    name TEXT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    etag UUID NOT NULL DEFAULT gen_random_uuid(),
    primary_cart BOOLEAN NOT NULL DEFAULT FALSE
);

-- user_id index
CREATE INDEX user_product_lists_user_id_idx ON user_product_lists (user_id);

CREATE OR REPLACE FUNCTION get_or_create_primary_product_list(_user_id UUID)
RETURNS UUID AS $$
DECLARE
    product_list_id UUID;
BEGIN
    -- Attempt to retrieve the existing primary product list
    SELECT id INTO product_list_id
    FROM user_product_lists
    WHERE user_id = _user_id AND primary_cart = TRUE
    LIMIT 1;
    
    -- If no primary product list exists, create a new one
    IF product_list_id IS NULL THEN
        INSERT INTO user_product_lists (user_id, primary_cart)
        VALUES (_user_id, TRUE)
        RETURNING id INTO product_list_id;
    END IF;
    
    -- Return the product_list_id
    RETURN product_list_id;
END;
$$ LANGUAGE plpgsql;

-- Only one primary cart per user
CREATE UNIQUE INDEX unique_primary_cart_per_user
ON user_product_lists (user_id)
WHERE primary_cart = TRUE;

CREATE TABLE IF NOT EXISTS product_list_items (
    list_id UUID NOT NULL REFERENCES user_product_lists (id) ON DELETE CASCADE,
    product_id UUID NOT NULL REFERENCES products (id) ON DELETE CASCADE,
    quantity INTEGER NOT NULL,
    price NUMERIC(10, 2) NOT NULL,
    PRIMARY KEY (list_id, product_id)
);

-- Trigger to update etag and updated_at when product_list_items are inserted
CREATE OR REPLACE FUNCTION insert_user_product_lists_trigger()
RETURNS TRIGGER AS $$
BEGIN
    UPDATE user_product_lists
    SET
        updated_at = NOW(),
        etag = gen_random_uuid()
    WHERE id = NEW.list_id;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Trigger to run the insert_user_product_lists_trigger function when product_list_items are inserted
CREATE TRIGGER insert_user_product_lists_trigger
AFTER INSERT ON product_list_items
FOR EACH ROW
EXECUTE FUNCTION insert_user_product_lists_trigger();

-- Trigger to update etag and updated_at when product_list_items are updated
CREATE OR REPLACE FUNCTION update_user_product_lists_trigger()
RETURNS TRIGGER AS $$
BEGIN
    UPDATE user_product_lists
    SET 
        updated_at = NOW(),
        etag = gen_random_uuid()
    WHERE id = NEW.list_id;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Trigger to run the update_user_product_lists_trigger function when product_list_items are updated
CREATE TRIGGER update_user_product_lists_trigger
AFTER UPDATE ON product_list_items
FOR EACH ROW
EXECUTE FUNCTION update_user_product_lists_trigger();

-- Trigger to update etag and updated_at when product_list_items are deleted
CREATE OR REPLACE FUNCTION delete_user_product_lists_trigger()
RETURNS TRIGGER AS $$
BEGIN
    UPDATE user_product_lists
    SET 
        updated_at = NOW(),
        etag = gen_random_uuid()
    WHERE id = OLD.list_id;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Trigger to run the delete_user_product_lists_trigger function when product_list_items are deleted
CREATE TRIGGER delete_user_product_lists_trigger
AFTER DELETE ON product_list_items
FOR EACH ROW
EXECUTE FUNCTION delete_user_product_lists_trigger();
