/*  Marketplace application; database schemas and functions
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

-- Create categories
DO $$ BEGIN
    PERFORM create_category(NULL, 'vegetables', 'Vegetables', 'en', 'category_vegetables.jpg', 1);
    PERFORM create_category(NULL, 'fruits', 'Fruits', 'en', 'category_fruits.jpg', 2);
    PERFORM create_category(NULL, 'dairy', 'Dairy', 'en', 'category_dairy.jpg', 3);
    PERFORM create_category(NULL, 'meat', 'Meat', 'en', 'category_meat.jpg', 4);
    PERFORM create_category(NULL, 'bakery', 'Bakery', 'en', 'category_bakery.jpg', 5);
    PERFORM create_category(NULL, 'beverages', 'Beverages', 'en', 'category_beverages.jpg', 6);
    -- Add more categories as needed
END $$;

-- Create suppliers
DO $$ BEGIN
    PERFORM create_supplier('POINT(41.3851 2.1734)', 'en', 'Fresh Farm Produce', TRUE);
    PERFORM create_supplier('POINT(41.3929 2.1644)', 'en', 'Organic Greens', TRUE);
    PERFORM create_supplier('POINT(41.3793 2.1394)', 'en', 'Local Dairy Delights', TRUE);
    PERFORM create_supplier('POINT(41.3875 2.1686)', 'en', 'Farm to Table Meats', TRUE);
    -- Add more suppliers as needed
END $$;

-- Create units
DO $$ BEGIN
    PERFORM create_unit('kg', 'en', 'Kilogram');
    PERFORM create_unit('bunch', 'en', 'Bunch');
    PERFORM create_unit('litre', 'en', 'Litre');
    PERFORM create_unit('pack', 'en', 'Pack');
    -- Add more units as needed
END $$;

-- Create products for Fresh Farm Produce
DO $$ BEGIN
    -- Category: Vegetables
    PERFORM create_product(
        (SELECT id FROM suppliers WHERE name_text_id = (SELECT id FROM text_content WHERE content = 'Fresh Farm Produce' AND language_id = (SELECT id FROM languages WHERE language_tag = 'en'))),
        (SELECT id FROM product_categories WHERE name_tag = 'vegetables'),
        7, 2.99,
        (SELECT id FROM units WHERE name_tag = 'kg'), -- Fetch unit UUID from units table
        3.99,
        'en',
        'Carrots', 'Store in a cool place', 'Local Farm Co.', 'Fresh and nutritious carrots'
    );
    PERFORM create_product(
        (SELECT id FROM suppliers WHERE name_text_id = (SELECT id FROM text_content WHERE content = 'Fresh Farm Produce' AND language_id = (SELECT id FROM languages WHERE language_tag = 'en'))),
        (SELECT id FROM product_categories WHERE name_tag = 'vegetables'),
        5, 1.49,
        (SELECT id FROM units WHERE name_tag = 'kg'), -- Fetch unit UUID from units table
        8.99,
        'en',
        'Tomatoes', 'Keep away from direct sunlight', 'Local Farm Co.', 'Juicy and ripe tomatoes'
    );
    -- Add more products for Fresh Farm Produce as needed
END $$;

-- Create products for Organic Greens
DO $$ BEGIN
    -- Category: Vegetables
    PERFORM create_product(
        (SELECT id FROM suppliers WHERE name_text_id = (SELECT id FROM text_content WHERE content = 'Organic Greens' AND language_id = (SELECT id FROM languages WHERE language_tag = 'en'))),
        (SELECT id FROM product_categories WHERE name_tag = 'vegetables'),
        4, 3.99,
        (SELECT id FROM units WHERE name_tag = 'bunch'), -- Fetch unit UUID from units table
        5.55, 
        'en',
        'Spinach', 'Keep refrigerated', 'Organic Farms', 'Fresh and organic spinach'
    );
    PERFORM create_product(
        (SELECT id FROM suppliers WHERE name_text_id = (SELECT id FROM text_content WHERE content = 'Organic Greens' AND language_id = (SELECT id FROM languages WHERE language_tag = 'en'))),
        (SELECT id FROM product_categories WHERE name_tag = 'vegetables'),
        6, 2.49,
        (SELECT id FROM units WHERE name_tag = 'bunch'), -- Fetch unit UUID from units table
        20.10, 
        'en',
        'Kale', 'Best consumed within a week', 'Organic Farms', 'Nutrient-rich organic kale'
    );
    PERFORM create_product(
        (SELECT id FROM suppliers WHERE name_text_id = (SELECT id FROM text_content WHERE content = 'Organic Greens' AND language_id = (SELECT id FROM languages WHERE language_tag = 'en'))),
        (SELECT id FROM product_categories WHERE name_tag = 'dairy'),
        2, 4.99,
        (SELECT id FROM units WHERE name_tag = 'litre'), -- Fetch unit UUID from units table
        12.34, 
        'en',
        'Cow Milk', 'Keep refrigerated', 'Organic Farms', 'Fresh and organic cow milk'
    );
    PERFORM create_product(
        (SELECT id FROM suppliers WHERE name_text_id = (SELECT id FROM text_content WHERE content = 'Organic Greens' AND language_id = (SELECT id FROM languages WHERE language_tag = 'en'))),
        (SELECT id FROM product_categories WHERE name_tag = 'dairy'),
        3, 3.99,
        (SELECT id FROM units WHERE name_tag = 'litre'), -- Fetch unit UUID from units table
        1.01, 
        'en',
        'Goat Milk', 'Keep refrigerated', 'Organic Farms', 'Fresh and organic goat milk'
    );
END $$;

-- Continue creating products for other suppliers and categories as needed
