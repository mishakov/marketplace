/*  Marketplace application; database schemas and functions
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

CREATE TYPE user_role AS ENUM ('admin', 'user', 'guest');

CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    first_name VARCHAR(255),
    middle_name VARCHAR(255),
    last_name VARCHAR(255),

    email VARCHAR(255) UNIQUE,
    registration_date TIMESTAMP NOT NULL DEFAULT NOW(),
    password VARCHAR(255),

    mobile VARCHAR(255),

    verified_email BOOLEAN NOT NULL DEFAULT FALSE,

    role user_role NOT NULL DEFAULT 'guest'

--    registration_referrer VARCHAR(255),
--    postcode VARCHAR(255),
--    shopping_cart_etag UUID NOT NULL DEFAULT gen_random_uuid()
);

CREATE INDEX IF NOT EXISTS users_email_idx ON users (email);

CREATE TABLE IF NOT EXISTS email_verification_tokens (
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    token VARCHAR(255) NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    expiration_date TIMESTAMP NOT NULL DEFAULT NOW() + INTERVAL '1 day',
    creation_date TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS email_tokens (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    token VARCHAR(255) NOT NULL,
    expiration_date TIMESTAMP,
    valid BOOLEAN NOT NULL DEFAULT TRUE,
    failed_count INTEGER NOT NULL DEFAULT 0,
    creation_date TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS auth_tokens (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    token VARCHAR(255) NOT NULL,
    expiration_date TIMESTAMP NOT NULL,
    creation_date TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS refresh_tokens (
    session_id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    session_token UUID NOT NULL DEFAULT gen_random_uuid(),
    expiration_date TIMESTAMP NOT NULL DEFAULT NOW() + INTERVAL '30 days',
    UNIQUE (session_id, session_token)
);

CREATE TABLE IF NOT EXISTS password_reset_tokens (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    token UUID NOT NULL DEFAULT gen_random_uuid(),
    expiration_date TIMESTAMP NOT NULL DEFAULT NOW() + INTERVAL '15 minutes',
    creation_date TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS newsletter (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    identifier VARCHAR(255) NOT NULL UNIQUE,
    name VARCHAR(255) NOT NULL
);

CREATE INDEX IF NOT EXISTS newsletter_identifier_idx ON newsletter (identifier);

CREATE TABLE IF NOT EXISTS newsletter_subscriptions (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    newsletter_id UUID NOT NULL REFERENCES newsletter (id) ON DELETE CASCADE,
    creation_date TIMESTAMP NOT NULL DEFAULT NOW()
);