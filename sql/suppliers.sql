/*  Marketplace application; database schemas and functions
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

CREATE TABLE IF NOT EXISTS suppliers (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    description_text_id UUID UNIQUE REFERENCES text_content (id) ON DELETE RESTRICT,
    name_text_id UUID NOT NULL UNIQUE REFERENCES text_content (id) ON DELETE RESTRICT,
    image_url text,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deletion_date TIMESTAMPTZ CHECK (deletion_date > creation_date AND (deletion_date IS NULL OR deletion_date <= NOW())),
    public BOOLEAN NOT NULL DEFAULT FALSE,

    position GEOGRAPHY(POINT) NOT NULL,

    products_updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS dropoff_points (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    address_text text,
    position GEOGRAPHY(POINT) NOT NULL,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS dropoff_supplier (
    supplier_id UUID NOT NULL REFERENCES suppliers (id) ON DELETE CASCADE,
    dropoff_point_id UUID NOT NULL REFERENCES dropoff_points (id) ON DELETE CASCADE,
    PRIMARY KEY (supplier_id, dropoff_point_id)
);

CREATE TABLE IF NOT EXISTS supplier_categories (
    supplier_id UUID NOT NULL REFERENCES suppliers (id) ON DELETE CASCADE,
    category_id UUID NOT NULL REFERENCES product_categories (id) ON DELETE CASCADE,
    has_public_products BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (supplier_id, category_id)
);

CREATE OR REPLACE FUNCTION create_supplier(
    supplier_position GEOGRAPHY(POINT),
    language_tag VARCHAR(255),
    name TEXT,
    public BOOLEAN DEFAULT FALSE,
    description TEXT DEFAULT NULL
) RETURNS UUID AS $$
DECLARE
    description_text_id UUID;
    name_text_id UUID;
    new_supplier_id UUID;
BEGIN
    description_text_id := create_text_content(description, language_tag);
    name_text_id := create_text_content(name, language_tag);

    INSERT INTO suppliers (description_text_id, name_text_id, public, position)
    VALUES
        (description_text_id, name_text_id, public, supplier_position)
    RETURNING id INTO new_supplier_id;

    RETURN new_supplier_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW public_suppliers AS
    SELECT * FROM suppliers WHERE suppliers.public = TRUE AND suppliers.deletion_date IS NULL;

CREATE TYPE supplier_user_roles_enum AS ENUM ('manager', 'user');

CREATE TABLE IF NOT EXISTS supplier_user_roles (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    supplier_id UUID NOT NULL REFERENCES suppliers (id) ON DELETE CASCADE,
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    role supplier_user_roles_enum NOT NULL DEFAULT 'manager',
    UNIQUE (supplier_id, user_id)
);

CREATE INDEX IF NOT EXISTS supplier_user_roles_suplier_user_id_idx ON supplier_user_roles (supplier_id, user_id);

CREATE TABLE IF NOT EXISTS units (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name_text_id UUID NOT NULL REFERENCES text_content (id) ON DELETE CASCADE,
    name_tag VARCHAR(255) UNIQUE NOT NULL
);

CREATE INDEX IF NOT EXISTS units_name_tag_idx ON units (name_tag);

CREATE OR REPLACE FUNCTION create_unit(
    name_tag VARCHAR(255),
    language_tag VARCHAR(255),
    name TEXT
) RETURNS UUID AS $$
DECLARE
    name_text_id UUID;
    new_unit_id UUID;
BEGIN
    name_text_id := create_text_content(name, language_tag);

    INSERT INTO units (name_text_id, name_tag)
    VALUES (name_text_id, name_tag)
    RETURNING id INTO new_unit_id;

    RETURN new_unit_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW units_names AS
    SELECT
        u.id as id,
        COALESCE(trans.content, text.content) as name,
        l.id as language_id,
        (SELECT MAX(u) FROM (VALUES (text.updated_at), (trans.updated_at)) AS updated_at(u)) AS updated_at
    FROM units u
    CROSS JOIN languages l
    LEFT JOIN text_content text ON text.id = u.name_text_id
    LEFT JOIN translations trans ON trans.text_content_id = text.id AND l.id = trans.language_id;

CREATE TABLE IF NOT EXISTS product_categories (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    parent UUID REFERENCES product_categories (id) ON DELETE SET NULL DEFAULT NULL,
    name_tag VARCHAR(255) UNIQUE NOT NULL,
    name_text_id UUID NOT NULL REFERENCES text_content (id) ON DELETE CASCADE,
    image_url VARCHAR(255) NOT NULL,
    order_position INTEGER,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deletion_date TIMESTAMPTZ CHECK (deletion_date > creation_date AND (deletion_date IS NULL OR deletion_date <= NOW()))
);

CREATE OR REPLACE FUNCTION trigger_drop_category_text_content()
RETURNS TRIGGER AS $$
BEGIN
    DELETE FROM text_content WHERE id = OLD.name_text_id;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_drop_category_text_content
AFTER DELETE ON product_categories
FOR EACH ROW
EXECUTE FUNCTION trigger_drop_category_text_content();

CREATE OR REPLACE FUNCTION create_category(
    parent_id UUID,
    name_tag VARCHAR(255),
    name TEXT,
    name_language_tag VARCHAR(255),
    image_url VARCHAR(255),
    order_position INTEGER
) RETURNS UUID AS $$
DECLARE
    new_name_text_id UUID;
    language_id UUID;
    new_category_id UUID;
BEGIN
    new_name_text_id := create_text_content(name, name_language_tag);

    INSERT INTO product_categories (parent, name_text_id, image_url, order_position, name_tag)
    VALUES (parent_id, new_name_text_id, image_url, order_position, name_tag)
    RETURNING id INTO new_category_id;

    RETURN new_category_id;
END;
$$ LANGUAGE plpgsql;

CREATE INDEX IF NOT EXISTS product_categories_parent_idx ON product_categories (parent);

CREATE OR REPLACE VIEW product_categories_names AS (
    SELECT
        c.id as id,
        COALESCE(trans.content, text.content) as name,
        l.id as language_id,
        (SELECT MAX(u) FROM (VALUES (text.updated_at), (trans.updated_at)) AS updated_at(u)) AS updated_at
    FROM product_categories c
    CROSS JOIN languages l
    LEFT JOIN text_content text ON text.id = c.name_text_id
    LEFT JOIN translations trans ON trans.text_content_id = text.id AND l.id = trans.language_id
);

CREATE TABLE IF NOT EXISTS products (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    supplier_id UUID NOT NULL REFERENCES suppliers (id) ON DELETE CASCADE,
    category_id UUID NOT NULL REFERENCES product_categories (id) ON DELETE NO ACTION,

    text_language_id UUID NOT NULL REFERENCES languages (id) ON DELETE RESTRICT,
    name text NOT NULL,
    description text,
    composition text,
    storage_conditions text,
    manufacturer text,

    storage_time_days INTEGER NOT NULL,
    unit_quantity NUMERIC(10, 2) NOT NULL,
    unit_type UUID NOT NULL REFERENCES units (id) ON DELETE RESTRICT,

    price NUMERIC(10, 2) NOT NULL,

    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deletion_date TIMESTAMPTZ CHECK (deletion_date > creation_date AND (deletion_date IS NULL OR deletion_date <= NOW())),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),

    public BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS product_translations (
    product_id UUID NOT NULL REFERENCES products (id) ON DELETE CASCADE,
    language_id UUID NOT NULL REFERENCES languages (id) ON DELETE CASCADE,

    name TEXT NOT NULL,
    description TEXT,
    composition TEXT,
    storage_conditions TEXT,
    manufacturer TEXT,

    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),

    PRIMARY KEY (product_id, language_id)
);

CREATE OR REPLACE FUNCTION check_translation_language()
RETURNS TRIGGER AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM product_translations pt
        WHERE pt.product_id = NEW.product_id
          AND pt.language_id = NEW.language_id
    ) THEN
        RAISE EXCEPTION 'A translation in the same language already exists for this product.';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER product_translation_language_check
BEFORE INSERT OR UPDATE ON product_translations
FOR EACH ROW
EXECUTE FUNCTION check_translation_language();

-- Check translations if product language changes
CREATE OR REPLACE FUNCTION check_translation_language_on_product_update()
RETURNS TRIGGER AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM product_translations pt
        WHERE pt.product_id = NEW.id
          AND pt.language_id = NEW.text_language_id
    ) THEN
        RAISE EXCEPTION 'A translation in the same language already exists for this product.';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER product_translation_language_check_on_product_update
BEFORE UPDATE ON products
FOR EACH ROW
EXECUTE FUNCTION check_translation_language_on_product_update();

CREATE OR REPLACE FUNCTION create_product(
    supplier_id UUID,
    category_id UUID,
    storage_time_days INTEGER,
    unit_quantity NUMERIC(10, 2),
    unit_type UUID,
    _price NUMERIC(10, 2),
    _language_tag VARCHAR(255),
    _name TEXT,
    _storage_conditions TEXT,
    _manufacturer TEXT,
    _description TEXT DEFAULT NULL,
    _composition TEXT DEFAULT NULL
) RETURNS UUID AS $$
DECLARE
    language_id UUID;
DECLARE
    name_text_id UUID;
    description_text_id UUID;
    composition_text_id UUID;
    storage_conditions_text_id UUID;
    manufacturer_text_id UUID;
    new_product_id UUID;
BEGIN
    -- Get language ID from the tag
    SELECT id INTO language_id FROM languages WHERE languages.language_tag = _language_tag;

    -- Insert the product
    INSERT INTO products (
        supplier_id, category_id,
        name, description, composition,
        storage_time_days, storage_conditions, manufacturer,
        unit_quantity, unit_type, text_language_id, price
    )
    VALUES (
        supplier_id, category_id,
        _name, _description, _composition,
        storage_time_days, _storage_conditions, _manufacturer,
        unit_quantity, unit_type, language_id, _price
    )
    RETURNING id INTO new_product_id;

    RETURN new_product_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION delete_related_text_content()
RETURNS TRIGGER AS $$
BEGIN
    DELETE FROM text_content WHERE id = OLD.name_text_id;
    DELETE FROM text_content WHERE id = OLD.description_text_id;
    DELETE FROM text_content WHERE id = OLD.composition_text_id;
    DELETE FROM text_content WHERE id = OLD.storage_conditions_text_id;
    DELETE FROM text_content WHERE id = OLD.manufacturer_text_id;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_delete_related_text_content
AFTER DELETE ON product_categories
FOR EACH ROW
EXECUTE FUNCTION delete_related_text_content();

CREATE INDEX IF NOT EXISTS products_category_id_idx ON products (category_id);
CREATE INDEX IF NOT EXISTS products_supplier_id_idx ON products (supplier_id);

CREATE OR REPLACE VIEW public_products AS
    SELECT * FROM products WHERE products.public = TRUE;

CREATE OR REPLACE VIEW public_products_with_parent_categories AS
WITH RECURSIVE CategoryTree AS (
    SELECT id, parent, id AS root_category
    FROM product_categories

    UNION ALL

    SELECT pc.id, pc.parent, ct.root_category
    FROM product_categories pc
    JOIN CategoryTree ct ON pc.id = ct.parent
)
SELECT pp.id AS product_id, pp.supplier_id, ct.root_category AS category_id
FROM public_products pp
JOIN CategoryTree ct ON pp.category_id = ct.id;

CREATE TRIGGER products_update_timestamp
BEFORE UPDATE ON products
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_updated_at();

CREATE OR REPLACE PROCEDURE trigger_recalculate_categories(
    _supplier_id UUID
) LANGUAGE plpgsql AS $$
BEGIN
    DELETE FROM supplier_categories WHERE supplier_id = _supplier_id;
    INSERT INTO supplier_categories (supplier_id, category_id, has_public_products)
    SELECT DISTINCT ON (category_id) 
        p.supplier_id, 
        p.category_id, 
        COUNT(pp.id) > 0
    FROM products p
    LEFT JOIN public_products pp ON pp.id = p.id
    WHERE p.supplier_id = _supplier_id
    GROUP BY p.supplier_id, p.category_id;
END;
$$;

CREATE OR REPLACE FUNCTION trigger_recalculate_categories_on_product_update()
RETURNS TRIGGER AS $$
BEGIN
    IF OLD.public <> NEW.public THEN
        CALL trigger_recalculate_categories(NEW.supplier_id);
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_recalculate_categories_on_product_update
AFTER UPDATE ON products
FOR EACH ROW
EXECUTE PROCEDURE trigger_recalculate_categories_on_product_update();

CREATE OR REPLACE FUNCTION trigger_recalculate_categories_on_product_delete()
RETURNS TRIGGER AS $$
BEGIN
    CALL trigger_recalculate_categories(OLD.supplier_id);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_recalculate_categories_on_product_delete
AFTER DELETE ON products
FOR EACH ROW
EXECUTE PROCEDURE trigger_recalculate_categories_on_product_delete();

CREATE OR REPLACE FUNCTION trigger_recalculate_categories_on_product_insert()
RETURNS TRIGGER AS $$
BEGIN
    CALL trigger_recalculate_categories(NEW.supplier_id);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_recalculate_categories_on_product_insert
AFTER INSERT ON products
FOR EACH ROW
EXECUTE PROCEDURE trigger_recalculate_categories_on_product_insert();

-- Trigger update for all products
-- DO
-- $do$
-- DECLARE
--     _id UUID;
-- BEGIN
-- 	FOR _id IN (SELECT id FROM suppliers) loop
--      	CALL trigger_recalculate_categories(_id);
-- 	END LOOP;
-- END
-- $do$;

CREATE INDEX IF NOT EXISTS products_supplier_id_idx ON products (supplier_id);
        
CREATE OR REPLACE VIEW product_manufacturer_texts AS
    SELECT
        p.id AS id,
        COALESCE(trans.content, text.content) AS manufacturer,
        l.id AS language_id
    FROM products p
    CROSS JOIN languages l
    LEFT JOIN text_content text ON text.id = p.storage_conditions_text_id
    LEFT JOIN translations trans ON trans.text_content_id = text.id AND l.id = trans.language_id;

CREATE VIEW translated_public_products AS
    SELECT
        p.id AS id,
        p.supplier_id AS supplier_id,
        p.category_id AS category_id,
        pcn.name AS category_name,
        p.unit_quantity AS unit_quantity,
        p.unit_type AS unit_id,
        un.name AS unit_name,
        COALESCE (pn.name, p.name) AS name,
        COALESCE (pn.description, p.description) AS description,
        COALESCE (pn.composition, p.composition) AS composition,
        COALESCE (pn.storage_conditions, p.storage_conditions) AS storage_conditions,
        COALESCE (pn.manufacturer, p.manufacturer) AS manufacturer,
        p.creation_date AS creation_date,
        l.id AS language_id,
        GREATEST(pn.updated_at, un.updated_at, p.updated_at) AS updated_at,
        p.price AS price,
        p.storage_time_days AS storage_time_days
    FROM products p
    CROSS JOIN languages l
    LEFT JOIN product_categories_names pcn ON p.category_id = pcn.id AND l.id = pcn.language_id
    LEFT JOIN units_names un ON p.unit_type = un.id AND l.id = un.language_id
    LEFT JOIN product_translations pn ON p.id = pn.product_id AND l.id = pn.language_id
    WHERE p.public = TRUE;

CREATE TABLE IF NOT EXISTS product_images (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    product_id UUID NOT NULL REFERENCES products (id) ON DELETE CASCADE,
    image_url VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS product_bullet_points (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    product_id UUID NOT NULL REFERENCES products (id) ON DELETE CASCADE,
    text_id UUID NOT NULL REFERENCES text_content (id) ON DELETE CASCADE,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE OR REPLACE VIEW translated_bullet_points AS
    SELECT
        p.id as id,
        p.product_id as product_id,
        p.text_id as text_id,
        l.id AS language_id,
        COALESCE(t.content, text.content) AS text
    FROM product_bullet_points p
    CROSS JOIN languages l
    LEFT JOIN text_content text ON text.id = p.text_id
    LEFT JOIN translations t ON t.text_content_id = text.id AND l.id = t.language_id;

CREATE INDEX IF NOT EXISTS product_images_product_id_idx ON product_images (product_id);

CREATE OR REPLACE VIEW public_product_images AS
    SELECT * FROM product_images WHERE product_images.product_id IN (SELECT products.id FROM products WHERE products.public = TRUE);

CREATE TABLE IF NOT EXISTS product_lots (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    product_id UUID NOT NULL REFERENCES products (id) ON DELETE CASCADE,
    quantity INTEGER NOT NULL,
    sold_quantity INTEGER NOT NULL DEFAULT 0 CHECK (sold_quantity <= quantity),
    available BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE INDEX IF NOT EXISTS product_lots_product_id_idx ON product_lots (product_id);

CREATE OR REPLACE VIEW product_quantities AS
    SELECT
        p.id AS product_id,
        COALESCE(SUM(pl.quantity) - SUM(pl.sold_quantity), 0) AS quantity
    FROM products p
    LEFT JOIN product_lots pl ON pl.product_id = p.id AND pl.available = TRUE
    GROUP BY p.id;

CREATE OR REPLACE FUNCTION trigger_refresh_shopping_cart_quantities()
RETURNS TRIGGER AS $$
DECLARE
    _product_id UUID;
    _quantity INTEGER;
BEGIN
    SELECT product_id FROM product_lots WHERE id = OLD.id INTO _product_id;
    SELECT quantity FROM product_quantities WHERE product_id = _product_id INTO _quantity;

    IF _quantity = 0 THEN
        DELETE FROM product_list_items WHERE product_id = _product_id;
    ELSE
        UPDATE product_list_items
        SET
            quantity = _quantity
        WHERE product_id = _product_id AND quantity > _quantity;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION trigger_refresh_shopping_cart_quantities_delete()
RETURNS TRIGGER AS $$
DECLARE
    _product_id UUID;
    _quantity INTEGER;
BEGIN
    SELECT product_id FROM product_lots WHERE id = OLD.id INTO _product_id;
    SELECT quantity FROM product_quantities WHERE product_id = _product_id INTO _quantity;

    IF _quantity = 0 THEN
        DELETE FROM product_list_items WHERE product_id = _product_id;
    ELSE
        UPDATE product_list_items
        SET
            quantity = _quantity
        WHERE product_id = _product_id AND quantity > _quantity;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_refresh_shopping_cart_quantities
BEFORE DELETE ON product_lots
FOR EACH ROW
EXECUTE PROCEDURE trigger_refresh_shopping_cart_quantities_delete();

CREATE TRIGGER trigger_refresh_shopping_cart_quantities_update
AFTER UPDATE ON product_lots
FOR EACH ROW
EXECUTE PROCEDURE trigger_refresh_shopping_cart_quantities();

CREATE OR REPLACE FUNCTION get_suppliers_in_radius(
    center GEOGRAPHY(POINT),
    radius_meters INTEGER
) RETURNS SETOF suppliers AS $$
    SELECT * FROM suppliers WHERE ST_DWithin(position, center, radius_meters) ORDER BY ST_Distance(position, center) ASC;
$$ LANGUAGE SQL;