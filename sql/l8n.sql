/*  Marketplace application; database schemas and functions
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

CREATE TABLE languages (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    native_name VARCHAR(255) NOT NULL,
    language_tag VARCHAR(255) NOT NULL UNIQUE,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE text_content (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    language_id UUID NOT NULL REFERENCES languages (id) ON DELETE CASCADE,
    content TEXT NOT NULL,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE translations (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    language_id UUID NOT NULL REFERENCES languages (id) ON DELETE CASCADE,
    text_content_id UUID NOT NULL REFERENCES text_content (id) ON DELETE CASCADE,
    content TEXT NOT NULL,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE (language_id, text_content_id)
);

CREATE OR REPLACE FUNCTION trigger_set_updated_at()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER text_context_update_timestamp
BEFORE UPDATE ON text_content
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_updated_at();

CREATE TRIGGER translations_update_timestamp
BEFORE UPDATE ON translations
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_updated_at();

CREATE OR REPLACE VIEW text_context_in_languages AS
    SELECT
        text_content.id AS text_content_id,
        COALESCE(translations.content, text_content.content) AS content,
        l.id AS language_id,
        (SELECT MAX(updated_at) FROM (VALUES (text_content.updated_at), (translations.updated_at)) AS updated_at(updated_at)) AS updated_at
    FROM text_content
    CROSS JOIN languages l
    LEFT JOIN translations ON translations.text_content_id = text_content.id AND translations.language_id = l.id;

CREATE OR REPLACE FUNCTION create_text_content(
    IN content TEXT,
    IN language_tag VARCHAR(255)
)
RETURNS UUID AS $$
#variable_conflict use_variable
DECLARE
    language_id UUID;
    text_content_id UUID;
BEGIN
    SELECT id INTO language_id FROM languages WHERE languages.language_tag = language_tag;

    IF language_id IS NULL THEN
        RAISE EXCEPTION 'Language not found';
    END IF;

    IF content IS NOT NULL THEN
        INSERT INTO text_content (language_id, content)
        VALUES (language_id, content)
        RETURNING id INTO text_content_id;
    ELSE
        text_content_id := NULL;
    END IF;

    RETURN text_content_id;
END;
$$ LANGUAGE plpgsql;
        