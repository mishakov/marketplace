/*  Marketplace application; shopping cart service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"context"
	"database/sql"
	"log"
	"marketplace/shopping-cart/cart"

	"github.com/pborman/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type grpcServer struct {
	cart.UnimplementedShoppingCartServiceServer
}

var getCartListQuery = `SELECT get_or_create_primary_product_list($1)`
var getCartListQueryPlain = `SELECT id FROM user_product_lists WHERE user_id = $1 AND primary_cart = TRUE`

func (s *grpcServer) GetShoppingCart(ctx context.Context, req *cart.ShoppingCartRequest) (*cart.ShoppingCart, error) {
	user_id := req.UserID

	var etag string

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	defer transaction.Rollback()

	// Get ID of the product list that is user's shopping cart and also its etag
	var cartListId string

	err = transaction.QueryRow(getCartListQueryPlain, user_id).Scan(&cartListId)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		// User doesn't exist or has no shopping cart
		etag := uuid.NIL.String()
		return &cart.ShoppingCart{
			Etag: etag,
		}, nil
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	err = transaction.QueryRow("SELECT etag FROM user_product_lists WHERE id = $1", cartListId).Scan(&etag)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	rows, err := transaction.Query(`SELECT
		s.product_id, s.quantity, s.price, sup.id, q.quantity
		FROM product_list_items s
		JOIN products p ON p.id = s.product_id
		JOIN suppliers sup ON sup.id = p.supplier_id
		INNER JOIN product_quantities q ON q.product_id = s.product_id 
		WHERE s.list_id = $1`, cartListId)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	defer rows.Close()

	shopping_cart := make([]*cart.ShoppingCartItem, 0)
	for rows.Next() {
		var item cart.ShoppingCartItem
		err := rows.Scan(&item.ProductID, &item.Quantity, &item.Price, &item.SupplierID, &item.Stock)
		if err != nil {
			log.Println(err)
			return nil, status.Error(codes.Internal, "internal server error")
		}

		shopping_cart = append(shopping_cart, &item)
	}

	err = rows.Err()
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	transaction.Commit()

	return &cart.ShoppingCart{
		Etag:  etag,
		Items: shopping_cart,
	}, nil
}

func (s *grpcServer) GetShoppingCartInfo(ctx context.Context, req *cart.ShoppingCartRequest) (*cart.ShoppingCartInfo, error) {
	user_id := req.UserID

	var etag string

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	defer transaction.Rollback()

	// Get ID of the product list that is user's shopping cart and also its etag
	var cartListId string

	err = transaction.QueryRow(getCartListQueryPlain, user_id).Scan(&cartListId)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		// User doesn't exist or has no shopping cart
		etag := uuid.NIL.String()
		return &cart.ShoppingCartInfo{
			Etag: etag,
		}, nil
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	err = transaction.QueryRow("SELECT etag FROM user_product_lists WHERE id = $1", cartListId).Scan(&etag)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var totalPrice float32
	var totalQuantity int

	err = transaction.QueryRow(`SELECT COALESCE(SUM(pli.quantity * p.price), 0), COUNT(pli.product_id)
								FROM product_list_items pli
								JOIN products p ON p.id = pli.product_id
		                        WHERE pli.list_id = $1`, cartListId).Scan(&totalPrice, &totalQuantity)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	transaction.Commit()

	return &cart.ShoppingCartInfo{
		Etag:       etag,
		TotalPrice: totalPrice,
		TotalItems: int32(totalQuantity),
	}, nil
}

func (s *grpcServer) IncrementProduct(ctx context.Context, req *cart.ProductRequest) (*cart.ProductState, error) {
	user_id := req.UserID
	product_id := req.ProductID

	var etag string

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	defer transaction.Rollback()

	var (
		stock int32
		price float32
	)

	var cartListId string

	// Get ID of the product list that is user's shopping cart
	err = transaction.QueryRow(getCartListQuery, user_id).Scan(&cartListId)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "user not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	err = transaction.QueryRow("SELECT q.quantity, p.price FROM products p JOIN product_quantities q on p.id = q.product_id WHERE product_id = $1", product_id).Scan(&stock, &price)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "product not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var quantity int32

	if stock != 0 {
		err := transaction.QueryRow(`INSERT INTO product_list_items (list_id, product_id, quantity, price)
		VALUES ($1, $2, 1, 
			(SELECT price FROM products WHERE id = $2))
		ON CONFLICT (user_id, product_id)
		DO UPDATE
		SET quantity = LEAST(product_list_items.quantity + 1, $3)
		RETURNING quantity`, cartListId, product_id, stock).Scan(&quantity)
		if err != nil {
			log.Println(err)
			return nil, status.Error(codes.Internal, "internal server error")
		}
	}

	err = transaction.QueryRow("SELECT etag FROM user_product_lists WHERE id = $1", cartListId).Scan(&etag)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "user not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	transaction.Commit()

	return &cart.ProductState{
		Quantity: quantity,
		Price:    price,
		Stock:    stock,
		Etag:     etag,
	}, nil
}

func (s *grpcServer) SetProductQuantity(ctx context.Context, req *cart.ProductSetRequest) (*cart.ProductState, error) {
	user_id := req.UserID
	product_id := req.ProductID
	wanted_quantity := req.Quantity

	var etag string

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	defer transaction.Rollback()

	var (
		stock int32
		price float32
	)

	var cartListId string

	// Get ID of the product list that is user's shopping cart
	err = transaction.QueryRow(getCartListQuery, user_id).Scan(&cartListId)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "user not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	err = transaction.QueryRow("SELECT COALESCE(q.quantity, 0), p.price FROM products p JOIN product_quantities q on p.id = q.product_id WHERE product_id = $1", product_id).Scan(&stock, &price)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "product not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var quantity int32

	if wanted_quantity == 0 {
		_, err := transaction.ExecContext(ctx, `DELETE FROM product_list_items WHERE list_id = $1 AND product_id = $2`, cartListId, product_id)
		if err != nil {
			log.Println(err)
			return nil, status.Error(codes.Internal, "internal server error")
		}
	} else if stock != 0 {
		err := transaction.QueryRow(`INSERT INTO product_list_items (list_id, product_id, quantity, price)
		VALUES ($1, $2, $3, 
			(SELECT price FROM products WHERE id = $2))
		ON CONFLICT (list_id, product_id)
		DO UPDATE
		SET quantity = LEAST($3, $4)
		RETURNING quantity`, cartListId, product_id, wanted_quantity, stock).Scan(&quantity)
		if err != nil {
			log.Println(err)
			return nil, status.Error(codes.Internal, "internal server error")
		}
	}

	err = transaction.QueryRow("SELECT etag FROM user_product_lists WHERE id = $1", cartListId).Scan(&etag)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "user not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	transaction.Commit()

	return &cart.ProductState{
		Quantity: quantity,
		Price:    price,
		Stock:    stock,
		Etag:     etag,
	}, nil
}

func (s *grpcServer) GetProductState(ctx context.Context, req *cart.ProductRequest) (*cart.ProductState, error) {
	user_id := req.UserID
	product_id := req.ProductID

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	defer transaction.Rollback()

	var cartListId string

	// Get ID of the product list that is user's shopping cart
	err = transaction.QueryRow(getCartListQueryPlain, user_id).Scan(&cartListId)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "user not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var state cart.ProductState
	query := `SELECT pli.quantity, p.price, upl.etag, COALESCE(pli.quantity, 0)
	FROM products p
	LEFT JOIN product_list_items pli ON p.id = pli.product_id AND pli.list_id = $1
	JOIN user_product_lists upl ON upl.id = $1
	WHERE p.id = $2`

	err = transaction.QueryRow(query, cartListId, product_id).Scan(&state.Stock, &state.Price, &state.Etag, &state.Quantity)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "product not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	transaction.Commit()

	return &state, nil
}

func (s *grpcServer) DeleteProduct(ctx context.Context, req *cart.ProductRequest) (*cart.ProductState, error) {
	user_id := req.UserID
	product_id := req.ProductID

	var etag string

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}
	defer transaction.Rollback()

	var (
		stock int32
		price float32
	)

	var cartListId string

	// Get ID of the product list that is user's shopping cart
	err = transaction.QueryRow(getCartListQuery, user_id).Scan(&cartListId)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "user not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	_, err = transaction.ExecContext(ctx, `DELETE FROM product_list_items WHERE list_id = $1 AND product_id = $2`, cartListId, product_id)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	err = transaction.QueryRow("SELECT etag FROM user_product_lists WHERE id = $1", cartListId).Scan(&etag)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "user not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	err = transaction.QueryRow(`SELECT
									COALESCE(l.quantity, 0),
									p.price
								FROM products p
								LEFT JOIN product_quantities l ON p.id = l.product_id
								WHERE p.id = $1`, product_id).Scan(&stock, &price)

	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Error(codes.NotFound, "product not found")
	default:
		log.Println(err)
		return nil, status.Error(codes.Internal, "internal server error")
	}

	transaction.Commit()

	return &cart.ProductState{
		Quantity: 0,
		Price:    price,
		Stock:    stock,
		Etag:     etag,
	}, nil
}
