/*  Marketplace application; shopping cart service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"marketplace/shopping-cart/cart"
	"net"
	"os"

	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"github.com/joho/godotenv"
)

var (
	port = flag.Int("port", 4001, "the server port")
)

var db *sql.DB

func main() {
	godotenv.Load()

	if os.Getenv("DB_USERNAME") == "" || os.Getenv("DB_PASS") == "" || os.Getenv("DB_HOST") == "" {
		panic("DB_USERNAME, DB_PASS, and DB_HOST must be set")
	}

	var connStr = "postgresql://" + os.Getenv("DB_USERNAME") + ":" + os.Getenv("DB_PASS") + "@" + os.Getenv("DB_HOST") + "/playground?sslmode=disable"
	var err error
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	reflection.Register(s)
	cart.RegisterShoppingCartServiceServer(s, &grpcServer{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
