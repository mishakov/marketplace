/*  Marketplace application; shopping cart service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"log"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func getShoppingCart(c *gin.Context) {
	user_id := c.MustGet("user_id")

	var etag string

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}
	defer transaction.Rollback()

	err = transaction.QueryRow("SELECT refresh_shopping_cart($1)", user_id).Scan(&etag)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	etag_header := c.GetHeader("If-None-Match")
	if etag_header == etag {
		c.Status(304)
		return
	}

	type ShoppingCartItem struct {
		ProductID string  `json:"product_id"`
		LotID     string  `json:"lot_id"`
		Quantity  int     `json:"quantity"`
		Price     float64 `json:"price"`
	}

	rows, err := transaction.Query("SELECT l.product_id, s.lot_id, s.quantity, s.price FROM shopping_cart s INNER JOIN product_lots l ON s.lot_id = l.id WHERE s.user_id = $1", user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}
	defer rows.Close()

	shopping_cart := make([]ShoppingCartItem, 0)
	for rows.Next() {
		var item ShoppingCartItem
		err := rows.Scan(&item.ProductID, &item.LotID, &item.Quantity, &item.Price)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})

			return
		}

		shopping_cart = append(shopping_cart, item)
	}

	err = rows.Err()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	transaction.Commit()

	c.Header("ETag", etag)

	c.JSON(200, gin.H{
		"status": "success",
		"items":  shopping_cart,
	})
}

func addToShoppingCart(c *gin.Context) {
	var request struct {
		LotID    string `json:"lot_id" binding:"required" validate:"uuid4"`
		Quantity int    `json:"quantity" binding:"required" validate:"min=1"`
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request",
			"detail": err.Error(),
		})

		return
	}

	user_id := c.MustGet("user_id")

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}
	defer transaction.Rollback()

	_, err = transaction.Exec("SELECT refresh_shopping_cart($1)", user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	var new_uuid string

	err = transaction.QueryRow("SELECT add_to_shopping_cart($1, $2, $3)", user_id, request.LotID, request.Quantity).Scan(&new_uuid)
	switch {
	case err == nil:
		break
	case strings.Contains(err.Error(), "Not enough product in stock"):
		c.JSON(409, gin.H{
			"status": "error",
			"error":  "not enough product in stock",
		})

		return
	case strings.Contains(err.Error(), "Product lot not found"):
		c.JSON(404, gin.H{
			"status": "error",
			"error":  "product lot not found",
		})

		return
	case err != nil:
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	transaction.Commit()

	c.Header("ETag", new_uuid)

	c.JSON(201, gin.H{
		"status": "success",
	})
}

func removeFromShoppingCart(c *gin.Context) {
	lot_id := c.Param("product_id")
	_, err := uuid.Parse(lot_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request",
			"detail": "invalid product id",
		})

		return
	}

	user_id := c.MustGet("user_id")

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}
	defer transaction.Rollback()

	result, err := transaction.Exec("DELETE FROM shopping_cart WHERE user_id = $1 AND lot_id = $2", user_id, lot_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	rows_affected, err := result.RowsAffected()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	if rows_affected == 0 {
		c.JSON(404, gin.H{
			"status": "error",
			"error":  "product not found in shopping cart",
		})

		return
	}

	var etag string

	err = transaction.QueryRow("SELECT refresh_shopping_cart($1)", user_id).Scan(&etag)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	transaction.Commit()

	c.Header("ETag", etag)

	c.JSON(200, gin.H{
		"status": "success",
	})
}

func updateShoppingCart(c *gin.Context) {
	var request struct {
		Quantity int `json:"quantity" binding:"required" validate:"min=1"`
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request",
			"detail": err.Error(),
		})

		return
	}

	lot_id := c.Param("product_id")
	_, err := uuid.Parse(lot_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request",
			"detail": "invalid product id",
		})

		return
	}

	user_id := c.MustGet("user_id")

	transaction, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}
	defer transaction.Rollback()

	_, err = transaction.Exec("SELECT refresh_shopping_cart($1)", user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	var new_uuid string
	var inserted bool

	err = transaction.QueryRow("SELECT * FROM set_shopping_cart_quantity($1, $2, $3)", user_id, lot_id, request.Quantity).Scan(&new_uuid, &inserted)
	switch {
	case err == nil:
		break
	case strings.Contains(err.Error(), "Not enough product in stock"):
		c.JSON(409, gin.H{
			"status": "error",
			"error":  "not enough product in stock",
		})

		return
	case strings.Contains(err.Error(), "Product lot not found"):
		c.JSON(404, gin.H{
			"status": "error",
			"error":  "product lot not found",
		})

		return
	case err != nil:
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	transaction.Commit()

	c.Header("ETag", new_uuid)
	if inserted {
		c.JSON(201, gin.H{
			"status": "success",
		})
	} else {
		c.JSON(200, gin.H{
			"status": "success",
		})
	}
}
