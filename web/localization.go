/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"embed"
	"io/fs"
	"strings"

	"gopkg.in/yaml.v3"
)

type ProductCardTranslation struct {
	In100g                 string `yaml:"in_100g"`
	StorageTimeTitle       string `yaml:"storage_time_title"`
	CompositionTitle       string `yaml:"composition_title"`
	StorageConditionsTitle string `yaml:"storage_conditions_title"`
	SupplierTitle          string `yaml:"supplier_title"`
}

type GeneralTranslation struct {
	Title              string `yaml:"website_title"`
	DaysString         string `yaml:"days_string"`
	LogoURL            string `yaml:"logo_url"`
	LogoAlt            string `yaml:"logo_alt"`
	BannerText         string `yaml:"banner_text"`
	BannerURL          string `yaml:"banner_url"`
	KmFromYou          string `yaml:"kilometers_from_you"`
	DeliveryAddress    string `yaml:"delivery_address"`
	AddressPlaceholder string `yaml:"address_placeholder"`
	HomePage           string `yaml:"home_page"`
	AssetsServer       string `yaml:"assets_server"`
}

type ProductTranslation struct {
	SoldOut    string `yaml:"sold_out"`
	OutOfStock string `yaml:"out_of_stock"`
}

type ShoppingCartTranslation struct {
	ToSupplierText string `yaml:"to_supplier_text"`
	TotalText      string `yaml:"total"`
	EmptyCartTitle string `yaml:"empty_cart_title"`
	EmptyCartText  string `yaml:"empty_cart_text"`
	CheckoutText   string `yaml:"checkout"`
	EmptyLogoURL   string `yaml:"empty_logo_url"`
}

type LocationPageTranslation struct {
	Title        string `yaml:"title"`
	SubmitText   string `yaml:"submit"`
	ContinueText string `yaml:"continue"`
	SaveText     string `yaml:"save"`
}

type LoginPageTranslation struct {
	Login                string `yaml:"login"`
	Register             string `yaml:"register"`
	Email                string `yaml:"email"`
	Continue             string `yaml:"continue"`
	ErrorInvalidCode     string `yaml:"error_invalid_code"`
	ErrorTooManyRequests string `yaml:"error_too_many_requests"`
	ErrorUnknown         string `yaml:"error_unknown"`
	RequestCodeText      string `yaml:"request_code_text"`
	PasscodeText         string `yaml:"passcode_text"`
	EmailPlaceholder     string `yaml:"email_placeholder"`
}

type CheckoutPageTranslation struct {
	Title         string `yaml:"title"`
	Description   string `yaml:"description"`
	HeaderText    string `yaml:"header_text"`
	TotalText     string `yaml:"total_text"`
	ToPaymentText string `yaml:"to_payment_text"`
}

type Translation struct {
	ProductCard  ProductCardTranslation  `yaml:"product_card"`
	General      GeneralTranslation      `yaml:"general"`
	Product      ProductTranslation      `yaml:"product"`
	ShoppingCart ShoppingCartTranslation `yaml:"shopping_cart"`
	LocationPage LocationPageTranslation `yaml:"location_page"`
	LoginPage    LoginPageTranslation    `yaml:"login_page"`
	CheckoutPage CheckoutPageTranslation `yaml:"checkout_page"`
}

var TranslationMap = map[string]Translation{}

func loadLocalization(f embed.FS, path string) (Translation, error) {
	var t Translation

	file, err := f.ReadFile(path)
	if err != nil {
		return Translation{}, err
	}

	err = yaml.Unmarshal(file, &t)
	if err != nil {
		return Translation{}, err
	}

	return t, nil
}

func initLocalization() error {
	err := fs.WalkDir(files, "localization", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() || !strings.HasSuffix(path, ".yaml") {
			return nil
		}

		lang := strings.TrimSuffix(d.Name(), ".yaml")
		t, err := loadLocalization(files, path)
		if err != nil {
			return err
		}

		TranslationMap[lang] = t

		return nil
	})

	return err
}

func getTranslation(lang string) *Translation {
	t, ok := TranslationMap[lang]
	if !ok {
		return nil
	}

	return &t
}
