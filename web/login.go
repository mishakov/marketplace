/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"log"
	"marketplace/user-account-service/auth"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/mux"
)

type LoginPage struct {
	PageTitle        string
	AssetsServer     string
	ContinueText     string
	PageDescription  string
	ProcessLink      string
	Overlay          bool
	LangTag          string
	EmailPlaceholder string
}
type PasscodeData struct {
	Email        string
	ContinueText string
	ErrorMessage string
	ProcessLink  string
	PasscodeText string
}

func validContinueTo(continueTo string) bool {
	if continueTo == "checkout" {
		return true
	}

	return false
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "private, no-cache")

	vars := mux.Vars(r)
	lang := vars["lang"]

	var err error

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusNotFound)
		return
	}

	args := r.URL.Query()
	continueTo := args.Get("continue_to")
	if !validContinueTo(continueTo) {
		// Unexpected continue_to value
		continueTo = ""
	}

	// Check if user is already logged and not guest
	loggedIn, err := isLoggedIn(r)
	if loggedIn && err == nil {
		// Redirect to continue_to
		continueToAddr := "/" + lang + "/"
		if continueTo != "" {
			continueToAddr += continueTo

			if args.Get("overlay") == "true" {
				continueToAddr += "?overlay=true"
			}
		}

		http.Redirect(w, r, continueToAddr, http.StatusFound)
		return
	}

	overlay := args.Get("overlay") == "true" && r.Header.Get("HX-Request") == "true"

	processLink := "/" + lang + "/process_email"
	if continueTo != "" {
		processLink += "?continue_to=" + continueTo
	}

	if overlay {
		if continueTo != "" {
			processLink += "&"
		} else {
			processLink += "?"
		}
		processLink += "overlay=true"
	}

	page := LoginPage{
		PageTitle:        translation.General.Title,
		ProcessLink:      processLink,
		Overlay:          overlay,
		LangTag:          lang,
		ContinueText:     translation.LoginPage.RequestCodeText,
		EmailPlaceholder: translation.LoginPage.EmailPlaceholder,
		AssetsServer:     translation.General.AssetsServer,
	}

	if !overlay {
		err = loginPageTemplate.Execute(w, page)
	} else {
		err = loginPageTemplate.ExecuteTemplate(w, "loginOverlay", page)
	}
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}

func processEmailHandler(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("HX-Request") != "true" {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusNotFound)
		return
	}

	// Check if user is already logged and not guest
	loggedIn, err := isLoggedIn(r)
	if loggedIn && err == nil {
		http.Error(w, "Already logged in", http.StatusBadRequest)
		return
	}

	email := r.FormValue("email")
	overlay := r.FormValue("overlay")
	if overlay != "true" {
		overlay = ""
	}
	continueTo := r.FormValue("continue_to")

	ctx := r.Context()
	conn, err := authPool.Get(ctx)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := auth.NewAuthServiceClient(conn)

	sessionResp, err := client.RequestAuthorization(ctx, &auth.AuthorizationRequest{
		Email:   email,
		LangTag: lang,
	})

	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if sessionResp.Status == auth.AuthorizationStatus_TOO_MANY_REQUESTS {
		http.Error(w, "Too many requests", http.StatusTooManyRequests)
		return
	}

	if sessionResp.Status == auth.AuthorizationStatus_INVALID_EMAIL {
		http.Error(w, "Invalid email", http.StatusBadRequest)
		return
	}

	params := url.Values{}
	validContinue := validContinueTo(continueTo)
	if overlay == "true" {
		params.Add("overlay", "true")
	}
	if validContinue {
		params.Add("continue_to", continueTo)
	}

	uri := url.URL{
		Path:     "/" + lang + "/continue_email",
		RawQuery: params.Encode(),
	}

	data := PasscodeData{
		Email:        email,
		ProcessLink:  uri.String(),
		ContinueText: translation.LoginPage.Continue,
		PasscodeText: translation.LoginPage.PasscodeText,
	}

	err = loginPageTemplate.ExecuteTemplate(w, "passcodeForm", data)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}

func continueEmailHandler(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("HX-Request") != "true" {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusNotFound)
		return
	}

	// Check if user is already logged and not guest
	loggedIn, err := isLoggedIn(r)
	if loggedIn && err == nil {
		http.Error(w, "Already logged in", http.StatusBadRequest)
		return
	}

	var activeUserID *string
	// Check if user has anonymous session
	userID, _, err := validateSessionCookie(r)
	if err == nil {
		activeUserID = &userID
	}

	email := r.FormValue("email")
	passcode := r.FormValue("passcode")
	overlay := r.FormValue("overlay")
	continueTo := r.FormValue("continue_to")

	ctx := r.Context()
	conn, err := authPool.Get(ctx)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := auth.NewAuthServiceClient(conn)
	result, err := client.VerifyAuthorization(ctx, &auth.VerifyRequest{
		Email:        email,
		Code:         passcode,
		ActiveUserID: activeUserID,
	})

	params := url.Values{}
	validContinue := validContinueTo(continueTo)
	if overlay == "true" {
		params.Add("overlay", "true")
	}
	if validContinue {
		params.Add("continue_to", continueTo)
	}

	if err != nil {
		uri := url.URL{
			Path:     "/" + lang + "/continue_email",
			RawQuery: params.Encode(),
		}

		data := PasscodeData{
			Email:        email,
			ContinueText: translation.LoginPage.Continue,
			ProcessLink:  uri.String(),
			ErrorMessage: translation.LoginPage.ErrorUnknown,
			PasscodeText: translation.LoginPage.PasscodeText,
		}

		err := loginPageTemplate.ExecuteTemplate(w, "passcodeForm", data)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
		return
	}

	switch result.Status {
	case auth.AuthorizationStatus_INVALID_EMAIL:
		uri := url.URL{
			Path: "/" + lang + "/process_email",
		}
		// Concatenate params if any
		uri.RawQuery = params.Encode()

		data := LoginPage{
			PageTitle:        translation.General.Title,
			ContinueText:     translation.LoginPage.RequestCodeText,
			ProcessLink:      uri.String(),
			Overlay:          overlay == "true",
			LangTag:          lang,
			EmailPlaceholder: translation.LoginPage.EmailPlaceholder,
			AssetsServer:     translation.General.AssetsServer,
		}

		err = loginPageTemplate.ExecuteTemplate(w, "loginForm", data)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	case auth.AuthorizationStatus_INVALID_CODE:
		uri := url.URL{
			Path:     "/" + lang + "/continue_email",
			RawQuery: params.Encode(),
		}

		data := PasscodeData{
			Email:        email,
			ContinueText: translation.LoginPage.Continue,
			ProcessLink:  uri.String(),
			ErrorMessage: translation.LoginPage.ErrorInvalidCode,
			PasscodeText: translation.LoginPage.PasscodeText,
		}

		err := loginPageTemplate.ExecuteTemplate(w, "passcodeForm", data)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	case auth.AuthorizationStatus_TOO_MANY_REQUESTS:
		uri := url.URL{
			Path:     "/" + lang + "/continue_email",
			RawQuery: params.Encode(),
		}

		data := PasscodeData{
			Email:        email,
			ContinueText: translation.LoginPage.Continue,
			ProcessLink:  uri.String(),
			ErrorMessage: translation.LoginPage.ErrorTooManyRequests,
			PasscodeText: translation.LoginPage.PasscodeText,
		}

		err := loginPageTemplate.ExecuteTemplate(w, "passcodeForm", data)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	case auth.AuthorizationStatus_SUCCESS:
		var expiresAtTime time.Time
		if result.ExpiresAt != nil {
			expiresAtTime = result.ExpiresAt.AsTime()
		}

		// Set session cookie
		setSessionCookie(w, result.SessionID, expiresAtTime)

		// Redirect to continue_to
		if overlay == "true" && validContinue {
			http.Redirect(w, r, "/"+lang+"/"+continueTo+"?overlay=true&retarget=true", http.StatusFound)
		} else if validContinue {
			// HX-Location redirect to continue_to
			w.Header().Set("HX-Location", "/"+lang+"/"+continueTo)
			w.WriteHeader(http.StatusNoContent)
		} else {
			// Do a pseudo-redirect to main page
			w.Header().Set("HX-Location", "/"+lang+"/")
			w.WriteHeader(http.StatusNoContent)
		}
	default:
		log.Println("Unexpected status:", result.Status)

		uri := url.URL{
			Path:     "/" + lang + "/continue_email",
			RawQuery: params.Encode(),
		}

		data := PasscodeData{
			Email:        email,
			ContinueText: translation.LoginPage.Continue,
			ProcessLink:  uri.String(),
			ErrorMessage: translation.LoginPage.ErrorUnknown,
			PasscodeText: translation.LoginPage.PasscodeText,
		}

		err := loginPageTemplate.ExecuteTemplate(w, "passcodeForm", data)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}
}
