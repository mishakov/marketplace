/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"

	"github.com/gorilla/mux"
)

type Address struct {
	Type         string  `json:"type"`
	Address      string  `json:"address"`
	Longitude    float32 `json:"longitude"`
	Latitude     float32 `json:"latitude"`
	DropOffPoint string  `json:"drop_off_point,omitempty"`
}

func (a Address) Signature() (string, error) {
	addressJSON, err := json.Marshal(a)
	if err != nil {
		return "", err
	}

	h := hmac.New(sha256.New, []byte(locationSecret))
	h.Write(addressJSON)
	signature := base64.URLEncoding.EncodeToString(h.Sum(nil))
	return signature, nil
}

type LocationSuggestion struct {
	AddressText string
	Key         string // Signature
	Lon         float32
	Lat         float32
}

type LocationPage struct {
	PageTitle       string
	PageDescription string
	AssetsServer    string
	LangTag         string
	Title           string
	Suggestions     []LocationSuggestion
	SubmitText      string
	SubmitURL       string
	Overlay         bool // Indicates whether the full page is rendered or just the overlay template

	Lon         float32
	Lat         float32
	ShowSubmit  bool
	Key         string
	AddressText string
}

type SuggestionsList struct {
	Suggestions []LocationSuggestion
}

var GeoapifyAPIKey string

type Rank struct {
	Confidence            float32 `json:"confidence"`
	ConfidenceCityLevel   float32 `json:"confidence_city_level"`
	ConfidenceStreetLevel float32 `json:"confidence_street_level"`
	MatchType             string  `json:"match_type"`
}

type Location struct {
	// Only the fields we need
	Formatted string  `json:"formatted"`
	Lat       float32 `json:"lat"`
	Lon       float32 `json:"lon"`
	Rank      Rank    `json:"rank"`
}

type AutocompleteResponse struct {
	Results []Location `json:"results"`
}

var locationSecret string

func getLocationSuggestions(text string, languageTag string) ([]LocationSuggestion, error) {
	params := url.Values{}
	params.Add("text", text)
	params.Add("apiKey", GeoapifyAPIKey)
	params.Add("lang", languageTag)
	params.Add("bias", "countrycode:fr")
	params.Add("format", "json")

	resp, err := http.Get("https://api.geoapify.com/v1/geocode/autocomplete?" + params.Encode())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		b, err := io.ReadAll(resp.Body)

		if err != nil {
			return nil, err
		}

		log.Println(string(b))
		return nil, io.EOF
	}

	var autocompleteResponse AutocompleteResponse
	err = json.NewDecoder(resp.Body).Decode(&autocompleteResponse)

	if err != nil {
		return nil, err
	}

	suggestions := make([]LocationSuggestion, len(autocompleteResponse.Results))
	for i, result := range autocompleteResponse.Results {
		address := Address{
			Type:      "address",
			Address:   result.Formatted,
			Longitude: result.Lon,
			Latitude:  result.Lat,
		}

		// Serialize and sign the address
		addressJSON, err := json.Marshal(address)
		if err != nil {
			return nil, err
		}

		h := hmac.New(sha256.New, []byte(locationSecret))
		h.Write(addressJSON)
		signature := base64.URLEncoding.EncodeToString(h.Sum(nil))

		suggestions[i] = LocationSuggestion{
			Key:         signature,
			AddressText: result.Formatted,
			Lon:         result.Lon,
			Lat:         result.Lat,
		}
	}

	return suggestions, nil
}

func signAddress(address Address) (string, error) {
	address.Type = "address"
	address.DropOffPoint = ""
	addressJSON, err := json.Marshal(address)
	if err != nil {
		return "", err
	}

	h := hmac.New(sha256.New, []byte(locationSecret))
	h.Write(addressJSON)
	signature := base64.URLEncoding.EncodeToString(h.Sum(nil))

	return signature, nil
}

func guessLocationFromIP(ip string, languageTag string) (Address, error) {
	params := url.Values{}
	params.Add("apiKey", GeoapifyAPIKey)
	params.Add("ip", ip)
	params.Add("lang", languageTag)
	params.Add("format", "json")

	resp, err := http.Get("https://api.geoapify.com/v1/ipinfo?" + params.Encode())
	if err != nil {
		log.Println(err)
		return Address{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		b, err := io.ReadAll(resp.Body)

		if err != nil {
			return Address{}, err
		}

		log.Println(string(b))
		return Address{}, io.EOF
	}

	type addressWrapper struct {
		Address Address `json:"location"`
	}
	w := addressWrapper{}

	var location Address
	err = json.NewDecoder(resp.Body).Decode(&w)

	if err != nil {
		log.Println(err)
		return Address{}, err
	}

	log.Println(location)

	return w.Address, nil
}

func locationPageHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "private, no-cache")

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusNotFound)
		return
	}

	overlay := r.URL.Query().Get("overlay") == "true" && r.Header.Get("HX-Request") == "true"
	submitURL := "/" + lang + "/location"
	if !overlay {
		submitURL += "?standalone=true"
	}

	address := Address{
		Type:      "address",
		Address:   "",
		Longitude: -1.6800199,
		Latitude:  48.11134,
	}
	showSubmit := false

	// Get the location from the session
	userAddress, err := getLocation(r.Context(), r)
	var hasValidLocation bool

	if err == nil {
		address = userAddress
		showSubmit = true
		hasValidLocation = true
	} else {
		hasValidLocation = false
	}

	if !hasValidLocation {
		// Guess the location from the IP address
		ip := r.RemoteAddr

		IPaddress, err := guessLocationFromIP(ip, lang)
		if err == nil && (IPaddress.Latitude != 0.0 || IPaddress.Longitude != 0.0) {
			address = IPaddress
			showSubmit = false
			hasValidLocation = false
		}
	}

	// Serialize and sign the address
	var signature string
	if showSubmit {
		signature, _ = signAddress(address)
	}

	var submitText string
	if overlay {
		submitText = translation.LocationPage.SaveText
	} else {
		submitText = translation.LocationPage.ContinueText
	}

	page := LocationPage{
		PageTitle:    translation.General.Title,
		LangTag:      lang,
		Title:        translation.LocationPage.Title,
		AssetsServer: translation.General.AssetsServer,
		SubmitText:   submitText,
		SubmitURL:    submitURL,
		Overlay:      overlay,
		ShowSubmit:   showSubmit,
		Lon:          address.Longitude,
		Lat:          address.Latitude,
		Key:          signature,
		AddressText:  address.Address,
	}

	if !overlay {
		err = LocationPageTemplate.Execute(w, page)
	} else {
		err = LocationPageTemplate.ExecuteTemplate(w, "locationOverlay", page)
	}
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}

func locationSuggestionsHandler(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("HX-Request") != "true" {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusNotFound)
		return
	}

	text := r.URL.Query().Get("address")

	suggestions, err := getLocationSuggestions(text, lang)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	data := SuggestionsList{
		Suggestions: suggestions,
	}

	err = LocationPageTemplate.ExecuteTemplate(w, "locationSuggestions", data)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}

func locationSetHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "private, no-cache")

	// Unsupported if not HX-Request
	if r.Header.Get("HX-Request") != "true" {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusNotFound)
		return
	}

	ctx := r.Context()

	// Get the address, longitude and latitude from the request
	err := r.ParseForm()
	if err != nil {
		log.Println(err)
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	addressText := r.Form.Get("address")
	longitude := r.Form.Get("lon")
	latitude := r.Form.Get("lat")
	signature := r.Form.Get("key")

	lonFloat, err := strconv.ParseFloat(longitude, 32)
	if err != nil {
		log.Println(err)
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	latFloat, err := strconv.ParseFloat(latitude, 32)
	if err != nil {
		log.Println(err)
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	// Check the signature
	address := Address{
		Type:      "address",
		Address:   addressText,
		Longitude: float32(lonFloat),
		Latitude:  float32(latFloat),
	}

	addressJSON, err := json.Marshal(address)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	h := hmac.New(sha256.New, []byte(locationSecret))
	h.Write(addressJSON)
	expectedMac := h.Sum(nil)
	decodedSignature, err := base64.URLEncoding.DecodeString(signature)
	if err != nil {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	if !hmac.Equal(decodedSignature, expectedMac) {
		http.Error(w, "Invalid key", http.StatusUnauthorized)
		return
	}

	address.DropOffPoint = "f4db5dc5-805b-4e60-94d5-cafe20695f85"

	// Save the location
	err = setLocation(ctx, w, address)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	// Redirect to the home page if standalone is set
	standalone := r.URL.Query().Get("standalone")
	if standalone == "true" {
		w.Header().Set("HX-Redirect", "/"+lang+"/suppliers")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	// Otherwise, return 204 and send go back event to return
	w.Header().Set("HX-Trigger", "go-back")
	w.WriteHeader(http.StatusNoContent)
}
