/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"encoding/base64"

	"github.com/google/uuid"
)

func slugidEncode(_uuid uuid.UUID) string {
	return base64.URLEncoding.EncodeToString(_uuid[:])[:22] // b64 padding
}

func slugidDecode(slug string) (uuid.UUID, error) {
	uuid_, err := base64.URLEncoding.DecodeString(slug + "==") // b64 padding
	if err != nil {
		return uuid.Nil, err
	}

	return uuid.UUID(uuid_), nil
}
