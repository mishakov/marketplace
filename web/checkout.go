/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"log"
	"marketplace/goods-and-suppliers/proto"
	cart "marketplace/shopping-cart/cart"
	"net/http"
	"net/url"
	"strconv"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CheckoutPage struct {
	PageTitle       string
	PageDescription string
	LangTag         string
	Overlay         bool // Indicates whether the full page is rendered or just the overlay template
	HeaderText      string
	AssetsServer    string

	TotalText     string
	Items         map[string]*ShoppingCartItem
	ToPaymentText string
}

type CheckoutItem struct {
	ID         string
	LangTag    string
	ImageURL   string
	Quantity   int
	Price      float32
	Max        int
	Name       string
	TotalPrice float32
}

func checkoutHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "private, no-cache")

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusNotFound)
		return
	}

	args := r.URL.Query()
	overlay := args.Get("overlay") == "true"

	// Check if user is logged in
	loggedIn, err := isLoggedIn(r)
	if err != nil || !loggedIn {
		// Redirect to login
		params := url.Values{}
		params.Add("continue_to", "checkout")
		if overlay {
			params.Add("overlay", "true")
		}

		uri := url.URL{
			Path:     "/" + lang + "/login",
			RawQuery: params.Encode(),
		}

		// Redirect to login
		http.Redirect(w, r, uri.String(), http.StatusFound)
		return
	}

	ctxData := ContextData{
		LanguageTag: lang,
	}

	ctx := context.WithValue(r.Context(), dataKey, &ctxData)

	conn, err := suppliersPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	reqData := RequestData{
		SupplierConn: conn,
		errChan:      make(chan error, 1),
		translation:  translation,
	}

	userID, _, err := validateSessionCookie(r)
	if err != nil {
		// This is an edge case, since we already checked if the user is logged in
		// Redirect to login and let the user try again
		params := url.Values{}
		params.Add("continue_to", "checkout")
		if overlay {
			params.Add("overlay", "true")
		}

		uri := url.URL{
			Path:     "/" + lang + "/login",
			RawQuery: params.Encode(),
		}

		// Redirect to login
		http.Redirect(w, r, uri.String(), http.StatusFound)
		return
	}

	// Get the user's shopping cart
	reqData.shoppingCartWg.Add(1)
	go getShoppingCartItems(ctx, &reqData, userID, uuid.Nil)

	reqData.shoppingCartWg.Wait()

	client := proto.NewProductServiceClient(reqData.SupplierConn)

	// Fill items with data from the database
	for _, ptr := range reqData.shoppingCart {
		reqData.wg.Add(1)
		go func(ptr *ShoppingCartItem) {
			defer reqData.wg.Done()

			data, err := client.GetProductShort(ctx, &proto.ProductRequest{
				Id:          ptr.ID,
				LanguageTag: ctxData.LanguageTag,
			})

			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}

			ptr.Name = data.Name
			ptr.UnitText = strconv.Itoa(int(data.UnitQuantity)) + " " + data.UnitName
			ptr.PriceText = strconv.FormatFloat(float64(ptr.Price)*float64(ptr.Quantity), 'f', 2, 32) + " €"

			if data.ImageURL == nil {
				ptr.ImageURL = imgPlaceholder
			} else {
				ptr.ImageURL = *data.ImageURL
			}
		}(ptr)
	}

	reqData.wg.Wait()

	page := CheckoutPage{
		PageTitle:       translation.CheckoutPage.Title,
		PageDescription: translation.CheckoutPage.Description,
		LangTag:         lang,
		Overlay:         overlay,
		HeaderText:      translation.CheckoutPage.HeaderText,
		TotalText:       translation.CheckoutPage.TotalText + "€" + strconv.FormatFloat(float64(reqData.cartTotal), 'f', 2, 32),
		Items:           reqData.shoppingCart,
		ToPaymentText:   translation.CheckoutPage.ToPaymentText,
		AssetsServer:    translation.General.AssetsServer,
	}

	if overlay {
		err = CheckoutPageTemplate.ExecuteTemplate(w, "checkoutOverlay", page)
	} else {
		err = CheckoutPageTemplate.Execute(w, page)
	}

	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}

func checkoutSetQuantityHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: This should work perfectly fine without HTMX, the page just needs to be reloaded
	hx_request := r.Header.Get("HX-Request")
	if hx_request != "true" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	userID, _, err := validateSessionCookie(r)
	if err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	lang := vars["lang"]

	trans := getTranslation(lang)
	if trans == nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	productID := vars["product_id"]
	_, err = uuid.Parse(productID)

	if err != nil {
		http.Error(w, "Invalid product ID", http.StatusBadRequest)
		return
	}

	quantity, err := strconv.Atoi(r.FormValue("quantity"))
	if err != nil || quantity < 0 {
		http.Error(w, "Invalid quantity", http.StatusBadRequest)
		return
	}

	price, err := strconv.ParseFloat(r.FormValue("price"), 32)
	if err != nil || price < 0 {
		http.Error(w, "Invalid price", http.StatusBadRequest)
		return
	}

	max, err := strconv.Atoi(r.FormValue("max"))
	if err != nil || max < 0 {
		http.Error(w, "Invalid max", http.StatusBadRequest)
		return
	}

	ctxData := ContextData{
		LanguageTag: lang,
	}

	ctx := context.WithValue(r.Context(), dataKey, &ctxData)

	conn, err := shoppingCartPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := cart.NewShoppingCartServiceClient(conn)

	setResult, err := client.SetProductQuantity(ctx, &cart.ProductSetRequest{
		UserID:    userID,
		ProductID: productID,
		Quantity:  int32(quantity),
	})

	if err != nil {
		st, ok := status.FromError(err)

		if !ok {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}

		switch st.Code() {
		case codes.NotFound:
			http.Error(w, "Not found", http.StatusNotFound)
			return
		default:
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	}

	if setResult.Stock == int32(max) && setResult.Price == float32(price) && setResult.Quantity == int32(quantity) {
		// Client is in sync with the server, no need to update the page
		// Just ask to update the price
		w.Header().Set("HX-Trigger", "reload-price")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	// Render new product card
	supConn, err := suppliersPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer supConn.Close()

	supClient := proto.NewProductServiceClient(supConn)

	data, err := supClient.GetProductShort(ctx, &proto.ProductRequest{
		Id:          productID,
		LanguageTag: ctxData.LanguageTag,
	})

	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	// Render new product card
	page := CheckoutItem{
		ID:         productID,
		LangTag:    lang,
		ImageURL:   *data.ImageURL,
		Quantity:   int(setResult.Quantity),
		Price:      setResult.Price,
		Max:        int(setResult.Stock),
		Name:       data.Name,
		TotalPrice: setResult.Price * float32(setResult.Quantity),
	}

	w.Header().Set("HX-Trigger", "reload-price")
	err = CheckoutPageTemplate.ExecuteTemplate(w, "checkoutItem", page)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}
