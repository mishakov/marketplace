/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"fmt"
	"log"
	"marketplace/goods-and-suppliers/proto"
	cart "marketplace/shopping-cart/cart"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func productHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "private, no-cache")

	vars := mux.Vars(r)
	productID := vars["product_id"]
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusBadRequest)
		return
	}

	_, err := uuid.Parse(productID)
	if err != nil {
		http.Error(w, "Invalid product ID", http.StatusBadRequest)
		return
	}

	// Request values
	values := r.URL.Query()

	// Early load styles
	// for _, style := range ProductPageStyles {
	// 	w.Header().Add("Link", fmt.Sprintf("<%s>; rel=preload; as=style", style))
	// }

	// if ProductPageStyles != nil {
	// 	w.WriteHeader(http.StatusEarlyHints)
	// }

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	conn, err := suppliersPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	wg := sync.WaitGroup{}

	var cartQuantity *cart.ProductState
	wg.Add(1)
	go func() {
		defer wg.Done()
		userID, _, err := validateSessionCookie(r)
		if err != nil {
			return
		}

		cartConn, err := shoppingCartPool.Get(ctx)
		if err != nil {
			return
		}
		defer cartConn.Close()

		client := cart.NewShoppingCartServiceClient(cartConn)

		cartQuantity, _ = client.GetProductState(ctx, &cart.ProductRequest{
			UserID:    userID,
			ProductID: productID,
		})
	}()

	client := proto.NewProductServiceClient(conn)

	response, err := client.GetProduct(r.Context(), &proto.ProductRequest{
		Id:          productID,
		LanguageTag: lang,
	})

	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}

		switch st.Code() {
		case codes.NotFound:
			http.Error(w, "Product not found", http.StatusNotFound)
			return
		default:
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	}

	var showInc, showDec bool
	var priceText string
	var quantity int
	stock := 10

	wg.Wait()
	if cartQuantity != nil {
		if cartQuantity.Quantity > 0 {
			priceText = strconv.Itoa(int(cartQuantity.Quantity)) + " x "
			showDec = true
		}

		if cartQuantity.Stock > 0 {
			priceText += strconv.FormatFloat(float64(response.Price), 'f', 2, 32) + " €"
		} else {
			priceText += translation.Product.SoldOut
		}
		showInc = cartQuantity.Quantity < cartQuantity.Stock
		quantity = int(cartQuantity.Quantity)
	} else if stock > 0 {
		priceText = strconv.FormatFloat(float64(response.Price), 'f', 2, 32) + " €"
		showInc = true
	} else {
		priceText = translation.Product.SoldOut
	}

	card := ProductCardParams{
		PageTitle:      translation.General.Title + " - " + response.Name,
		PageStyles:     ProductPageStyles,
		LangTag:        lang,
		InlineQuantity: true,
		Price:          response.Price,
		AssetsServer:   translation.General.AssetsServer,

		ID:           productID,
		Title:        response.Name,
		Description:  response.Description,
		Images:       response.Images,
		BulletPoints: response.BulletPoints,
		QuantityText: strconv.Itoa(int(response.UnitQuantity)) + " " + response.UnitName,

		PriceText: priceText,
		Quantity:  quantity,
		Stock:     int(stock),
		ShowDec:   showDec,
		ShowInc:   showInc,
	}

	if len(card.Images) == 0 {
		card.Images = append(card.Images, imgPlaceholder)
	}

	if response.NutritionalValues != nil {
		*card.ValuesQuantityText = translation.ProductCard.In100g

		for _, value := range response.NutritionalValues {
			card.NutritionalValues = append(card.NutritionalValues, NutritionalValue{
				Value: value.Value,
				Name:  value.Name,
			})
		}
	}

	if response.Composition != nil {
		card.Details = append(card.Details, ProductDetail{
			Class: "composition",
			Title: translation.ProductCard.CompositionTitle,
			Text:  *response.Composition,
		})
	}

	if response.StorageTime != 0 {
		card.Details = append(card.Details, ProductDetail{
			Class: "storage-time",
			Title: translation.ProductCard.StorageTimeTitle,
			Text:  strconv.Itoa(int(response.StorageTime)) + " days",
		})
	}

	if response.StorageConditions != nil {
		card.Details = append(card.Details, ProductDetail{
			Class: "storage-conditions",
			Title: translation.ProductCard.StorageConditionsTitle,
			Text:  *response.StorageConditions,
		})
	}

	supConn := proto.NewSupplierServiceClient(conn)
	nameResponse, err := supConn.GetSupplierDescription(r.Context(), &proto.SupplierRequest{
		Id:          response.SupplierId,
		LanguageTag: lang,
	})

	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	card.Details = append(card.Details, ProductDetail{
		Class: "supplier",
		Title: translation.ProductCard.SupplierTitle,
		Text:  nameResponse.Name,
	})

	for _, image := range card.Images {
		w.Header().Add("Link", fmt.Sprintf("<%s>; rel=preload; as=image", image))
	}

	// Render template
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	if r.Header.Get("HX-Boosted") == "true" {
		err = ProductCardTemplate.ExecuteTemplate(w, "content-slim", card)
	} else if values.Get("overlay") == "true" {
		card.Overlay = true
		err = ProductCardTemplate.ExecuteTemplate(w, "overlay", card)
	} else {
		err = ProductCardTemplate.Execute(w, card)
	}

	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}

type Price struct {
	ID       string
	Quantity int32
	Stock    int32
	Price    float32
}

// func incrementProductHandler(w http.ResponseWriter, r *http.Request) {
// 	hx_request := r.Header.Get("HX-Request")
// 	if hx_request != "true" {
// 		http.Error(w, "Bad request", http.StatusBadRequest)
// 		return
// 	}

// 	languageTag := "en"
// 	trans := getTranslation(languageTag)
// 	if trans == nil {
// 		http.Error(w, "Internal error", http.StatusInternalServerError)
// 		return
// 	}

// 	vars := mux.Vars(r)
// 	productID := vars["product_id"]
// 	_, err := uuid.Parse(productID)

// 	ctx := r.Context()

// 	if err != nil {
// 		http.Error(w, "Invalid product ID", http.StatusBadRequest)
// 		return
// 	}

// 	userID, err := validateSessionCookie(r)
// 	if err != nil {
// 		userID, err = createSession(w, r)
// 		if err != nil {
// 			log.Print(err)
// 			http.Error(w, "Internal error", http.StatusInternalServerError)
// 			return
// 		}
// 	}

// 	conn, err := shoppingCartPool.Get(ctx)
// 	if err != nil {
// 		log.Print(err)
// 		http.Error(w, "Internal error", http.StatusInternalServerError)
// 		return
// 	}
// 	defer conn.Close()

// 	client := cart.NewShoppingCartServiceClient(conn)

// 	reply, err := client.IncrementProduct(ctx, &cart.ProductRequest{
// 		UserID:    userID,
// 		ProductID: productID,
// 	})

// 	if err != nil {
// 		st, ok := status.FromError(err)
// 		if !ok {
// 			log.Print(err)
// 			http.Error(w, "Internal error", http.StatusInternalServerError)
// 			return
// 		}

// 		switch st.Code() {
// 		case codes.NotFound:
// 			p := Price{
// 				PriceText: trans.Product.SoldOut,
// 				ID:        productID,
// 			}

// 			w.Header().Set("Content-Type", "text/html; charset=utf-8")
// 			err = ProductCardTemplate.ExecuteTemplate(w, "price-box", p)
// 			if err != nil {
// 				log.Print(err)
// 				http.Error(w, "Internal error", http.StatusInternalServerError)
// 				return
// 			}
// 			return
// 		default:
// 			log.Print(err)
// 			http.Error(w, "Internal error", http.StatusInternalServerError)
// 			return
// 		}
// 	}

// 	var priceText string
// 	if reply.Quantity > 0 {
// 		priceText += strconv.Itoa(int(reply.Quantity)) + " x "
// 	}

// 	priceText += strconv.FormatFloat(float64(reply.Price), 'f', 2, 32) + " €"

// 	p := Price{
// 		PriceText: priceText,
// 		ID:        productID,
// 		ShowDec:   reply.Quantity > 0,
// 		ShowInc:   reply.Quantity < reply.Stock,
// 	}

// 	w.Header().Set("Content-Type", "text/html; charset=utf-8")
// 	err = ProductCardTemplate.ExecuteTemplate(w, "price-box", p)
// 	if err != nil {
// 		log.Print(err)
// 		http.Error(w, "Internal error", http.StatusInternalServerError)
// 		return
// 	}
// }

func setProductQuantityHandler(w http.ResponseWriter, r *http.Request) {
	hx_request := r.Header.Get("HX-Request")
	if hx_request != "true" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	languageTag := vars["lang"]

	trans := getTranslation(languageTag)
	if trans == nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	productID := vars["product_id"]
	_, err := uuid.Parse(productID)

	if err != nil {
		http.Error(w, "Invalid product ID", http.StatusBadRequest)
		return
	}

	quantity, err := strconv.Atoi(r.FormValue("quantity"))
	if err != nil || quantity < 0 {
		http.Error(w, "Invalid quantity", http.StatusBadRequest)
		return
	}

	values := r.URL.Query()
	in_card := values.Get("in_card") == "true"

	s := SupplierItem{
		ID:      productID,
		LangTag: languageTag,
	}

	wg := sync.WaitGroup{}

	if !in_card {
		wg.Add(1)
		go func() {
			defer wg.Done()

			conn, err := suppliersPool.Get(r.Context())
			if err != nil {
				log.Print(err)
				return
			}
			defer conn.Close()

			client := proto.NewProductServiceClient(conn)

			response, err := client.GetProductShort(r.Context(), &proto.ProductRequest{
				Id:          productID,
				LanguageTag: languageTag,
			})

			if err != nil {
				log.Print(err)
				return
			}

			s.Title = response.Name
			s.Description = strconv.FormatFloat(float64(response.UnitQuantity), 'f', 2, 32) + " " + response.UnitName
			s.URL = "/" + languageTag + "/products/" + productID
			s.OverlayURL = "/" + languageTag + "/products/" + productID + "?overlay=true"
			if response.ImageURL != nil {
				s.ImageURL = *response.ImageURL
			} else {
				s.ImageURL = imgPlaceholder
			}
		}()
	}

	ctx := r.Context()

	userID, _, err := validateSessionCookie(r)
	if err != nil {
		userID, _, err = createSession(w, r)
		if err != nil {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	}

	conn, err := shoppingCartPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := cart.NewShoppingCartServiceClient(conn)

	reply, err := client.SetProductQuantity(ctx, &cart.ProductSetRequest{
		UserID:    userID,
		ProductID: productID,
		Quantity:  int32(quantity),
	})

	if err != nil {
		st, ok := status.FromError(err)

		if !ok {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}

		switch st.Code() {
		case codes.NotFound:
			// TODO: Show sold out
			p := struct {
				Text string
			}{
				Text: trans.Product.SoldOut,
			}

			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			err = ProductCardTemplate.ExecuteTemplate(w, "price-box-sold-out", p)
			if err != nil {
				log.Print(err)
				http.Error(w, "Internal error", http.StatusInternalServerError)
				return
			}
			return
		default:
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	}

	var priceText string
	if reply.Quantity > 0 {
		priceText += strconv.Itoa(int(reply.Quantity)) + " x "
	}

	priceText += strconv.FormatFloat(float64(reply.Price), 'f', 2, 32) + " €"

	s.Quantity = int(reply.Quantity)
	s.Price = reply.Price
	s.Stock = int(reply.Stock)
	didntChange := s.Quantity == quantity

	if !in_card {
		w.Header().Set("HX-Trigger", "cart-update")
	}
	// If added the product to the cart as expected, no need to render the price box
	price, err := strconv.ParseFloat(r.FormValue("price"), 32)
	if err != nil || s.Price != float32(price) {
		didntChange = false
	}
	var stock int64
	if didntChange {
		stock, err = strconv.ParseInt(r.FormValue("max"), 10, 32)
		if err != nil || s.Stock != int(stock) {
			didntChange = false
		}
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if didntChange {
		// Reply with 204 No Content
		w.WriteHeader(http.StatusNoContent)
	}
	wg.Wait()
	if didntChange {
		return
	}
	if !in_card {
		err = supplierListPageTemplate.ExecuteTemplate(w, "supplier-item", s)
	} else {
		s.InlineQuantity = true
		err = ProductCardTemplate.ExecuteTemplate(w, "price-box", s)
	}
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}

func shoppingCartDeleteHandler(w http.ResponseWriter, r *http.Request) {
	hx_request := r.Header.Get("HX-Request")
	if hx_request != "true" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	userID, _, err := validateSessionCookie(r)
	if err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	productID := vars["product_id"]
	_, err = uuid.Parse(productID)

	if err != nil {
		http.Error(w, "Invalid product ID", http.StatusBadRequest)
		return
	}

	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusBadRequest)
		return
	}

	ctxData := ContextData{
		LanguageTag: lang,
	}

	ctx := context.WithValue(r.Context(), dataKey, &ctxData)

	values := r.URL.Query()
	supplierIDEncoded := values.Get("supplier")
	supplierID, _ := slugidDecode(supplierIDEncoded)

	conn, err := shoppingCartPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := cart.NewShoppingCartServiceClient(conn)

	deleteResult, err := client.DeleteProduct(ctx, &cart.ProductRequest{
		UserID:    userID,
		ProductID: productID,
	})

	if err != nil {
		st, ok := status.FromError(err)

		if !ok {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}

		switch st.Code() {
		case codes.NotFound:
			http.Error(w, "Not found", http.StatusNotFound)
			return
		default:
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	}

	supplierConn, err := suppliersPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer supplierConn.Close()

	reqData := RequestData{
		SupplierConn: supplierConn,
		errChan:      make(chan error, 1),
		translation:  translation,
	}

	address, err := getLocation(ctx, r)
	if err == nil {
		// Skip on error
		reqData.Address = &address
	}

	reqData.shoppingCartWg.Add(1)
	go getShoppingCartItems(ctx, &reqData, userID, supplierID)

	productChan := make(chan SupplierItem, 1)

	go func() {
		client := proto.NewProductServiceClient(supplierConn)

		response, err := client.GetProductShort(ctx, &proto.ProductRequest{
			Id:          productID,
			LanguageTag: lang,
		})

		if err != nil {
			log.Print("Error getting product: " + err.Error())
			return
		}

		var imageURL string

		if response.ImageURL != nil {
			imageURL = *response.ImageURL
		} else {
			imageURL = imgPlaceholder
		}

		productChan <- SupplierItem{
			ID:          productID,
			Title:       response.Name,
			Description: strconv.FormatFloat(float64(response.UnitQuantity), 'f', 2, 32) + " " + response.UnitName,
			Stock:       int(deleteResult.Stock),
			Price:       deleteResult.Price,
			URL:         "/" + lang + "/products/" + productID,
			OverlayURL:  "/" + lang + "/products/" + productID + "?overlay=true",
			ImageURL:    imageURL,
			LangTag:     lang,
			SwapOOB:     true,
		}
	}()

	// Render the shopping cart
	var data SuppliersListPageData

	reqData.wg.Add(1)
	// TODO: Change fillShoppingCart function signature
	go fillShoppingCart(ctx, &reqData, &data.ShoppingCart)
	data.Total = data.ShoppingCart.Total

	reqData.wg.Wait()

	select {
	case <-ctx.Done():
		switch ctx.Err() {
		case context.DeadlineExceeded:
			http.Error(w, "Timeout", http.StatusGatewayTimeout)
			return
		case context.Canceled:
			http.Error(w, "Canceled", http.StatusGatewayTimeout)
			return
		}
	case err := <-reqData.errChan:
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	default:
		data.ShoppingCart.Partial = true
		err := supplierListPageTemplate.ExecuteTemplate(w, "shoppingCart", data.ShoppingCart)
		if err != nil {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		err = supplierListPageTemplate.ExecuteTemplate(w, "shoppingCartButtonStandalone", data.Total)
		if err != nil {
			log.Print(err)
			return
		}
	}

	item, ok := <-productChan

	if !ok {
		return
	}

	err = supplierPageTemplate.ExecuteTemplate(w, "supplier-item", item)
	if err != nil {
		log.Print(err)
		return
	}
}

// Sets the quantity of a product in the shopping cart and returns the cart and the product card for HTMX
func setProductQuantityCartHandler(w http.ResponseWriter, r *http.Request) {
	hx_request := r.Header.Get("HX-Request")
	if hx_request != "true" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	userID, _, err := validateSessionCookie(r)
	if err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	lang := vars["lang"]

	trans := getTranslation(lang)
	if trans == nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	productID := vars["product_id"]
	_, err = uuid.Parse(productID)

	if err != nil {
		http.Error(w, "Invalid product ID", http.StatusBadRequest)
		return
	}

	quantity, err := strconv.Atoi(r.FormValue("quantity"))
	if err != nil || quantity < 0 {
		http.Error(w, "Invalid quantity", http.StatusBadRequest)
		return
	}

	ctxData := ContextData{
		LanguageTag: lang,
	}

	ctx := context.WithValue(r.Context(), dataKey, &ctxData)

	values := r.URL.Query()
	supplierIDEncoded := values.Get("supplier")
	supplierID, _ := slugidDecode(supplierIDEncoded)

	conn, err := shoppingCartPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := cart.NewShoppingCartServiceClient(conn)

	setResult, err := client.SetProductQuantity(ctx, &cart.ProductSetRequest{
		UserID:    userID,
		ProductID: productID,
		Quantity:  int32(quantity),
	})

	if err != nil {
		st, ok := status.FromError(err)

		if !ok {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}

		switch st.Code() {
		case codes.NotFound:
			http.Error(w, "Not found", http.StatusNotFound)
			return
		default:
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	}

	supplierConn, err := suppliersPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer supplierConn.Close()

	reqData := RequestData{
		SupplierConn: supplierConn,
		errChan:      make(chan error, 1),
		translation:  trans,
	}

	address, err := getLocation(ctx, r)
	if err == nil {
		// Skip on error
		reqData.Address = &address
	}

	reqData.shoppingCartWg.Add(1)
	go getShoppingCartItems(ctx, &reqData, userID, supplierID)

	productChan := make(chan SupplierItem, 1)

	go func() {
		client := proto.NewProductServiceClient(supplierConn)

		response, err := client.GetProductShort(ctx, &proto.ProductRequest{
			Id:          productID,
			LanguageTag: lang,
		})

		if err != nil {
			log.Print("Error getting product: " + err.Error())
			return
		}

		var imageURL string

		if response.ImageURL != nil {
			imageURL = *response.ImageURL
		} else {
			imageURL = imgPlaceholder
		}

		productChan <- SupplierItem{
			ID:          productID,
			Title:       response.Name,
			Description: strconv.FormatFloat(float64(response.UnitQuantity), 'f', 2, 32) + " " + response.UnitName,
			Stock:       int(setResult.Stock),
			Price:       setResult.Price,
			Quantity:    int(setResult.Quantity),
			URL:         "/" + lang + "/products/" + productID,
			OverlayURL:  "/" + lang + "/products/" + productID + "?overlay=true",
			ImageURL:    imageURL,
			LangTag:     lang,
			SwapOOB:     true,
		}
	}()

	// Render the shopping cart
	var data SuppliersListPageData

	reqData.wg.Add(1)
	// TODO: Change fillShoppingCart function signature
	go fillShoppingCart(ctx, &reqData, &data.ShoppingCart)
	data.Total = data.ShoppingCart.Total

	reqData.wg.Wait()

	select {
	case <-ctx.Done():
		switch ctx.Err() {
		case context.DeadlineExceeded:
			http.Error(w, "Timeout", http.StatusGatewayTimeout)
			return
		case context.Canceled:
			http.Error(w, "Canceled", http.StatusGatewayTimeout)
			return
		}
	case err := <-reqData.errChan:
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	default:
		data.ShoppingCart.Partial = true
		err := supplierListPageTemplate.ExecuteTemplate(w, "shoppingCart", data.ShoppingCart)
		if err != nil {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
	}

	item, ok := <-productChan

	if !ok {
		return
	}

	err = supplierPageTemplate.ExecuteTemplate(w, "supplier-item", item)
	if err != nil {
		log.Print(err)
		return
	}
}
