/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"html/template"
	"log"
	"marketplace/goods-and-suppliers/proto"
	cart "marketplace/shopping-cart/cart"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jftuga/geodist"
	pool "github.com/processout/grpc-go-pool"
)

type Link struct {
	Text string
	URL  string
}

type PriceDescriptor struct {
	Price float32
	Unit  string
}

type SupplierItem struct {
	ID          string
	Title       string
	Description string
	ImageURL    string
	URL         string
	OverlayURL  string
	LangTag     string

	Price          float32
	PriceText      string
	Quantity       int
	Stock          int
	InlineQuantity bool
	SwapOOB        bool
}

type Supplier struct {
	ID                 uuid.UUID // UUID
	URL                string    // URL of supplier page with slug ID
	Name               string
	ImageURL           string
	DescriptionBullets []string
	Categories         []string
	Items              []SupplierItem
	NavLinks           []Link
}

type ShoppingCartSupplierView struct {
	Items       []ShoppingCartItem
	Name        string
	ImageURL    string
	Total       float32
	LangTag     string
	SupplierURL string
}

type ShoppingCart struct {
	SuppliersItems map[uuid.UUID]*ShoppingCartSupplierView
	ToSupplierText string
	TotalText      string
	LangTag        string
	Partial        bool
	TotalPriceText string
	Total          float32

	EmptyCartTitle string
	EmptyCartText  string
	EmptyLogoURL   string

	AddressTitle string
	AddressText  string
	CheckoutText string
}

type CategoryHeader struct {
	Text string
	URL  string
}

type SuppliersListPageData struct {
	Categories   []Category
	Suppliers    []Supplier
	ShoppingCart ShoppingCart
	Total        float32

	LogoURL    string
	LogoAlt    string
	LangTag    string
	ShowBanner bool

	BannerText string
	BannerURL  string

	PageTitle       string
	PageDescription *string
	PageStyles      []string
	CategoryText    *CategoryHeader
	AssetsServer    string
}

type SupplierPageData struct {
	Categories []Category
	Supplier   Supplier

	ShoppingCart ShoppingCart
	Total        float32

	LogoURL    string
	LogoAlt    string
	LangTag    string
	ShowBanner bool

	BannerText string
	BannerURL  string

	PageTitle       string
	PageDescription *string
	PageStyles      []string
	AssetsServer    string
}

var supplierListPageTemplate *template.Template

var suppliersStyles = []string{
	"http://127.0.0.1:5500/style.css",
	//"http://172.0.0.1:5500/product.css",
}

func fillSupplierCategories(ctx *context.Context, req *RequestData, supplier *Supplier) {
	defer req.wg.Done()

	data := (*ctx).Value(dataKey).(*ContextData)

	client := proto.NewSupplierServiceClient(req.SupplierConn)

	resultLimit := int32(5)

	categoriesRep, err := client.GetSupplierCategories(*ctx, &proto.SupplierCategoriesRequest{
		LanguageTag: data.LanguageTag,
		Id:          supplier.ID.String(),
		Limit:       &resultLimit,
	})

	if err != nil {
		req.errOnce.Do(func() {
			req.errChan <- err
		})
		return
	}

	supplier.Categories = make([]string, len(categoriesRep.Categories))
	for i, category := range categoriesRep.Categories {
		supplier.Categories[i] = category.Name
	}
}

func fillSupplierProducts(ctx *context.Context, req *RequestData, supplier *Supplier) {
	defer req.wg.Done()

	data := (*ctx).Value(dataKey).(*ContextData)

	client := proto.NewSupplierServiceClient(req.SupplierConn)

	productsRep, err := client.GetProductsSummary(*ctx, &proto.SupplierProductsRequest{
		LanguageTag: data.LanguageTag,
		Id:          supplier.ID.String(),
		Limit:       6,
		CategoryID:  req.ActiveCaterogy,
	})

	if err != nil {
		req.errOnce.Do(func() {
			req.errChan <- err
		})
		return
	}

	req.shoppingCartWg.Wait()

	supplier.Items = make([]SupplierItem, len(productsRep.Products))
	for i, product := range productsRep.Products {
		var imageUrl string

		if product.ImageUrl != nil {
			imageUrl = *product.ImageUrl
		} else {
			imageUrl = imgPlaceholder
		}

		var cartQuantity int
		stock := product.AvailableQuantity
		c, ok := req.shoppingCart[product.Id]
		if ok {
			cartQuantity = c.Quantity
		}

		var showIfOutOfStock string
		if product.AvailableQuantity < 1 {
			showIfOutOfStock = req.translation.Product.OutOfStock
		}

		supplier.Items[i] = SupplierItem{
			ID:          product.Id,
			Title:       product.Name,
			Description: product.Description,
			ImageURL:    imageUrl,
			URL:         "/" + data.LanguageTag + "/products/" + product.Id,
			OverlayURL:  "/" + data.LanguageTag + "/products/" + product.Id + "?overlay=true",
			Quantity:    cartQuantity,
			Stock:       int(stock),
			Price:       product.Price,
			PriceText:   showIfOutOfStock,
			LangTag:     data.LanguageTag,
		}
	}
}

func fillShoppingCart(ctx context.Context, reqData *RequestData, data *ShoppingCart) {
	defer reqData.wg.Done()

	var addressText string
	if reqData.Address != nil {
		addressText = reqData.Address.Address
	} else {
		addressText = reqData.translation.General.AddressPlaceholder
	}

	reqData.shoppingCartWg.Wait()

	ctxData := ctx.Value(dataKey).(*ContextData)

	if reqData.shoppingCart == nil {
		*data = ShoppingCart{
			EmptyCartTitle: reqData.translation.ShoppingCart.EmptyCartTitle,
			EmptyCartText:  reqData.translation.ShoppingCart.EmptyCartText,
			LangTag:        ctxData.LanguageTag,
			AddressTitle:   reqData.translation.General.DeliveryAddress,
			AddressText:    addressText,
			EmptyLogoURL:   reqData.translation.ShoppingCart.EmptyLogoURL,
			CheckoutText:   reqData.translation.ShoppingCart.CheckoutText,
		}
		return
	}

	client := proto.NewProductServiceClient(reqData.SupplierConn)

	items := reqData.shoppingCart
	bySupplier := make(map[uuid.UUID]*ShoppingCartSupplierView)

	var total float32

	for _, value := range items {
		view, ok := bySupplier[value.SupplierID]
		if !ok {
			supplierSlugID := slugidEncode(value.SupplierID)

			view = &ShoppingCartSupplierView{
				Items:       make([]ShoppingCartItem, 0),
				LangTag:     ctxData.LanguageTag,
				SupplierURL: "/" + ctxData.LanguageTag + "/suppliers/" + supplierSlugID,
			}

			bySupplier[value.SupplierID] = view
		}

		view.Items = append(view.Items, *value)
		view.Total += value.Price * float32(value.Quantity)
		total += value.Price * float32(value.Quantity)
	}

	if ctx.Err() != nil {
		return
	}

	for id, value := range bySupplier {
		reqData.wg.Add(1)
		go func(id uuid.UUID, value *ShoppingCartSupplierView) {
			defer reqData.wg.Done()

			supConn := proto.NewSupplierServiceClient(reqData.SupplierConn)

			supData, err := supConn.GetSupplierDescription(ctx, &proto.SupplierRequest{
				Id:          id.String(),
				LanguageTag: ctxData.LanguageTag,
			})

			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}

			value.Name = supData.Name
			if supData.ImageURL != nil {
				value.ImageURL = *supData.ImageURL
			} else {
				value.ImageURL = imgPlaceholder
			}

			for i := range value.Items {
				reqData.wg.Add(1)
				go func(idx int) {
					defer reqData.wg.Done()

					item := &value.Items[idx]

					data, err := client.GetProductShort(ctx, &proto.ProductRequest{
						Id:          item.ID,
						LanguageTag: ctxData.LanguageTag,
					})

					if err != nil {
						reqData.errOnce.Do(func() {
							reqData.errChan <- err
						})
						return
					}

					item.Name = data.Name
					item.UnitText = strconv.Itoa(int(data.UnitQuantity)) + " " + data.UnitName
					item.PriceText = strconv.FormatFloat(float64(item.Price)*float64(item.Quantity), 'f', 2, 32) + " €"

					if data.ImageURL == nil {
						item.ImageURL = imgPlaceholder
					} else {
						item.ImageURL = *data.ImageURL
					}
				}(i)
			}
		}(id, value)
	}

	*data = ShoppingCart{
		LangTag:        ctxData.LanguageTag,
		SuppliersItems: bySupplier,
		ToSupplierText: reqData.translation.ShoppingCart.ToSupplierText,
		EmptyCartTitle: reqData.translation.ShoppingCart.EmptyCartTitle,
		EmptyCartText:  reqData.translation.ShoppingCart.EmptyCartText,
		AddressTitle:   reqData.translation.General.DeliveryAddress,
		AddressText:    addressText,
		TotalText:      reqData.translation.ShoppingCart.TotalText,
		TotalPriceText: reqData.translation.ShoppingCart.TotalText + " " + strconv.FormatFloat(float64(total), 'f', 2, 32) + " €",
		EmptyLogoURL:   reqData.translation.ShoppingCart.EmptyLogoURL,
		CheckoutText:   reqData.translation.ShoppingCart.CheckoutText,
	}

	data.Total = total
}

func fillSupplierCart(ctx context.Context, reqData *RequestData, supplier *Supplier, cartRef *ShoppingCartSupplier) {
	defer reqData.wg.Done()

	ctxData := ctx.Value(dataKey).(*ContextData)

	var addressText string
	if reqData.Address != nil {
		addressText = reqData.Address.Address
	} else {
		addressText = reqData.translation.General.AddressPlaceholder
	}

	cart := ShoppingCartSupplier{
		EmptyCartTitle: reqData.translation.ShoppingCart.EmptyCartTitle,
		EmptyCartText:  reqData.translation.ShoppingCart.EmptyCartText,
		OrderText:      reqData.translation.ShoppingCart.CheckoutText,

		SupplierID:        supplier.ID,
		SupplierIDEncoded: slugidEncode(supplier.ID),
		SupplierName:      supplier.Name,
		ImageURL:          supplier.ImageURL,
		LangTag:           ctxData.LanguageTag,
		AddressTitle:      reqData.translation.General.DeliveryAddress,
		AddressText:       addressText,
		EmptyLogoURL:      reqData.translation.ShoppingCart.EmptyLogoURL,
	}

	reqData.shoppingCartWg.Wait()

	if cartRef == nil {
		*cartRef = cart
		return
	}

	client := proto.NewProductServiceClient(reqData.SupplierConn)

	items := reqData.shoppingCart
	cart.Items = make([]ShoppingCartItem, 0, len(items))

	for _, value := range items {
		if value.SupplierID != supplier.ID {
			continue
		}

		cart.Items = append(cart.Items, ShoppingCartItem{
			ID:         value.ID,
			Quantity:   value.Quantity,
			Price:      value.Price,
			SupplierID: value.SupplierID,
		})

		cart.Total += value.Price * float32(value.Quantity)
	}

	if ctx.Err() != nil {
		return
	}

	for i := range cart.Items {
		reqData.wg.Add(1)
		go func(idx int) {
			defer reqData.wg.Done()

			item := &cart.Items[idx]

			data, err := client.GetProductShort(ctx, &proto.ProductRequest{
				Id:          item.ID,
				LanguageTag: ctxData.LanguageTag,
			})

			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}

			item.Name = data.Name
			item.UnitText = strconv.Itoa(int(data.UnitQuantity)) + " " + data.UnitName
			item.PriceText = strconv.FormatFloat(float64(item.Price)*float64(item.Quantity), 'f', 2, 32) + " €"

			if data.ImageURL == nil {
				item.ImageURL = imgPlaceholder
			} else {
				item.ImageURL = *data.ImageURL
			}
		}(i)
	}

	*cartRef = cart
}

var imgPlaceholder = "https://loremflickr.com/320/320/fruit"

type contextKey string

const dataKey contextKey = "data"

type ContextData struct {
	LanguageTag string
	UserID      *string
}

type ShoppingCartItem struct {
	ID         string
	Name       string
	UnitText   string
	Quantity   int
	ImageURL   string
	Price      float32
	TotalPrice float32
	PriceText  string
	// Maximum quantity that can be ordered
	Max        int
	SupplierID uuid.UUID
}

type RequestData struct {
	SupplierConn   *pool.ClientConn
	wg             sync.WaitGroup
	errChan        chan error
	errOnce        sync.Once
	translation    *Translation
	ActiveCaterogy *string

	shoppingCartWg sync.WaitGroup
	shoppingCart   map[string]*ShoppingCartItem
	cartTotal      float32

	Address       *Address
	DropOffPoints []string
}

func suppliersHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "private, no-cache")

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusBadRequest)
		return
	}

	categoryEncoded := r.URL.Query().Get("category")
	categoryID, _ := slugidDecode(categoryEncoded)

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	conn, err := suppliersPool.Get(timeoutCtx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	ctxData := ContextData{
		LanguageTag: lang,
	}

	reqData := RequestData{
		SupplierConn: conn,
		errChan:      make(chan error, 1),
		translation:  translation,
	}

	ctx := context.WithValue(timeoutCtx, dataKey, &ctxData)

	var ShowBanner bool
	// Check cookie
	cookie, err := r.Cookie("banner_closed")
	if err != nil {
		ShowBanner = true
	} else {
		ShowBanner = cookie.Value != "true"
	}

	data := SuppliersListPageData{
		LangTag:      lang,
		LogoAlt:      translation.General.LogoAlt,
		LogoURL:      translation.General.LogoURL,
		BannerURL:    translation.General.BannerURL,
		BannerText:   translation.General.BannerText,
		PageStyles:   suppliersStyles,
		ShowBanner:   ShowBanner,
		AssetsServer: translation.General.AssetsServer,
	}

	reqData.wg.Add(1)
	go func() {
		defer reqData.wg.Done()

		client := proto.NewCategoriesServiceClient(conn)

		categoriesRep, err := client.GetCategories(ctx, &proto.CategoriesRequest{
			LanguageTag: lang,
		})

		if err != nil {
			reqData.errOnce.Do(func() {
				reqData.errChan <- err
			})
			return
		}

		categories := make([]Category, 0)
		for _, category := range categoriesRep.Categories {
			id, err := uuid.Parse(category.Id)
			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}

			slugID := slugidEncode(id)

			var url string
			if categoryID == id {
				url = "/" + lang + "/suppliers"
			} else {
				url = "/" + lang + "/suppliers?category=" + slugID
			}

			categories = append(categories, Category{
				ID:       id,
				Name:     category.Name,
				URL:      url,
				ImageURL: category.ImageUrl,
				Dim:      categoryID != uuid.Nil && categoryID != id,
			})
		}

		data.Categories = categories
	}()

	reqData.DropOffPoints = []string{"f4db5dc5-805b-4e60-94d5-cafe20695f85"}

	address, err := getLocation(ctx, r)
	if err == nil {
		// Skip on error
		reqData.Address = &address
	}

	func() {
		sessionID, err := r.Cookie("session_id")
		if err != nil {
			return
		}

		_, err = uuid.Parse(sessionID.Value)
		if err != nil {
			return
		}

		userID, err := getUserID(ctx, sessionID.Value)
		if err != nil {
			return
		}

		ctxData.UserID = userID

		reqData.shoppingCartWg.Add(1)
		go func() {
			defer reqData.shoppingCartWg.Done()

			conn, err := shoppingCartPool.Get(ctx)
			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}
			defer conn.Close()

			client := cart.NewShoppingCartServiceClient(conn)

			shoppingCartRep, err := client.GetShoppingCart(ctx, &cart.ShoppingCartRequest{
				UserID: *userID,
			})

			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}

			if shoppingCartRep.Items == nil {
				return
			}

			reqData.shoppingCart = make(map[string]*ShoppingCartItem, len(shoppingCartRep.Items))

			for _, item := range shoppingCartRep.Items {
				reqData.shoppingCart[item.ProductID] = &ShoppingCartItem{
					Quantity:   int(item.Quantity),
					Price:      item.Price,
					SupplierID: uuid.MustParse(item.SupplierID),
					ID:         item.ProductID,
				}
			}
		}()
	}()

	reqData.wg.Add(1)
	go func() {
		defer reqData.wg.Done()

		client := proto.NewSupplierServiceClient(conn)

		categoryUUIDString := categoryID.String()
		var category *string
		if categoryID != uuid.Nil {
			category = &categoryUUIDString
		}

		suppliersRep, err := client.GetSuppliers(ctx, &proto.SuppliersRequest{
			LanguageTag:     lang,
			CategoryID:      category,
			DropoffPointIDs: reqData.DropOffPoints,
		})

		if err != nil {
			reqData.errOnce.Do(func() {
				reqData.errChan <- err
			})
			return
		}

		suppliers := make([]Supplier, len(suppliersRep.Suppliers))
		for i, supplier := range suppliersRep.Suppliers {
			if ctx.Err() != nil {
				return
			}

			var distance float64
			var showDistance bool = false
			if reqData.Address != nil && supplier.Longitude != nil && supplier.Latitude != nil {
				user := geodist.Coord{Lat: float64(reqData.Address.Latitude), Lon: float64(reqData.Address.Longitude)}
				supplier := geodist.Coord{Lat: float64(*supplier.Latitude), Lon: float64(*supplier.Longitude)}

				_, km, err := geodist.VincentyDistance(user, supplier)
				if err == nil {
					distance = km
					showDistance = true
				}
			}

			var ImageUrl *string
			if supplier.ImageUrl != nil {
				ImageUrl = supplier.ImageUrl
			} else {
				ImageUrl = &imgPlaceholder
			}

			id := uuid.MustParse(supplier.Id)

			s := Supplier{
				ID:       id,
				URL:      "/" + lang + "/suppliers/" + slugidEncode(id),
				Name:     supplier.Name,
				ImageURL: *ImageUrl,
			}

			if showDistance {
				s.DescriptionBullets = []string{
					strconv.FormatFloat(distance, 'f', 2, 32) + translation.General.KmFromYou,
				}
			}

			suppliers[i] = s

			reqData.wg.Add(1)
			go fillSupplierCategories(&ctx, &reqData, &suppliers[i])

			reqData.wg.Add(1)
			go fillSupplierProducts(&ctx, &reqData, &suppliers[i])
		}

		data.Suppliers = suppliers
	}()

	reqData.wg.Add(1)
	fillShoppingCart(ctx, &reqData, &data.ShoppingCart)
	data.Total = data.ShoppingCart.Total

	if categoryID != uuid.Nil {
		reqData.wg.Add(1)
		go func() {
			defer reqData.wg.Done()

			client := proto.NewCategoriesServiceClient(conn)
			reply, err := client.GetCategory(ctx, &proto.CategoryRequest{
				Id:          categoryID.String(),
				LanguageTag: lang,
			})

			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}

			data.CategoryText = &CategoryHeader{
				Text: reply.Name,
				URL:  "/" + lang + "/suppliers",
			}
		}()
	}

	reqData.wg.Wait()

	select {
	case <-ctx.Done():
		switch ctx.Err() {
		case context.DeadlineExceeded:
			http.Error(w, "Timeout", http.StatusGatewayTimeout)
			return
		case context.Canceled:
			http.Error(w, "Canceled", http.StatusGatewayTimeout)
			return
		}
	case err := <-reqData.errChan:
		cancel()
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	default:
		w.Header().Set("Cache-Control", "private, must-revalidate")
		err := supplierListPageTemplate.Execute(w, data)
		if err != nil {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		return
	}
}

func supplierPageHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "private, no-cache")

	vars := mux.Vars(r)
	supplierIDEncoded := vars["supplier_id"]

	reqVars := r.URL.Query()

	supplierID, err := slugidDecode(supplierIDEncoded)
	if err != nil {
		http.Error(w, "Invalid supplier ID", http.StatusBadRequest)
		return
	}

	// Leave categoryID as nil if not correct
	categoryID, _ := slugidDecode(reqVars.Get("category"))

	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusBadRequest)
		return
	}

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	conn, err := suppliersPool.Get(timeoutCtx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	ctxData := ContextData{
		LanguageTag: lang,
	}

	var activeCategory *string
	var categoryIDstring string
	if categoryID != uuid.Nil {
		categoryIDstring = categoryID.String()
		activeCategory = &categoryIDstring
	}

	reqData := RequestData{
		SupplierConn:   conn,
		errChan:        make(chan error, 1),
		translation:    translation,
		ActiveCaterogy: activeCategory,
	}

	ctx := context.WithValue(timeoutCtx, dataKey, &ctxData)

	var ShowBanner bool
	// Check cookie
	cookie, err := r.Cookie("banner_closed")
	if err != nil {
		ShowBanner = true
	} else {
		ShowBanner = cookie.Value != "true"
	}

	data := SupplierPageData{
		LogoAlt:      translation.General.LogoAlt,
		LogoURL:      translation.General.LogoURL,
		LangTag:      lang,
		AssetsServer: translation.General.AssetsServer,
		BannerURL:    translation.General.BannerURL,
		BannerText:   translation.General.BannerText,
		PageStyles:   suppliersStyles,
		ShowBanner:   ShowBanner,

		ShoppingCart: ShoppingCart{
			EmptyCartTitle: translation.ShoppingCart.EmptyCartTitle,
			EmptyCartText:  translation.ShoppingCart.EmptyCartText,
			EmptyLogoURL:   translation.ShoppingCart.EmptyLogoURL,
		},

		Supplier: Supplier{
			ID: supplierID,
			NavLinks: []Link{
				{URL: "/" + lang + "/suppliers", Text: translation.General.HomePage},
			},
		},
	}

	address, err := getLocation(ctx, r)
	if err == nil {
		// Skip on error
		reqData.Address = &address
	}

	func() {
		sessionID, err := r.Cookie("session_id")
		if err != nil {
			return
		}

		_, err = uuid.Parse(sessionID.Value)
		if err != nil {
			return
		}

		userID, err := getUserID(ctx, sessionID.Value)
		if err != nil {
			return
		}

		ctxData.UserID = userID

		reqData.shoppingCartWg.Add(1)
		go func() {
			defer reqData.shoppingCartWg.Done()

			conn, err := shoppingCartPool.Get(ctx)
			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}
			defer conn.Close()

			client := cart.NewShoppingCartServiceClient(conn)

			shoppingCartRep, err := client.GetShoppingCart(ctx, &cart.ShoppingCartRequest{
				UserID: *userID,
			})

			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				log.Println(err)
				return
			}

			if shoppingCartRep.Items == nil {
				return
			}

			reqData.shoppingCart = make(map[string]*ShoppingCartItem, len(shoppingCartRep.Items))

			for _, item := range shoppingCartRep.Items {
				reqData.shoppingCart[item.ProductID] = &ShoppingCartItem{
					Quantity:   int(item.Quantity),
					Price:      item.Price,
					SupplierID: uuid.MustParse(item.SupplierID),
					ID:         item.ProductID,
				}
			}
		}()
	}()

	supConn := proto.NewSupplierServiceClient(conn)

	supData, err := supConn.GetSupplierDescription(ctx, &proto.SupplierRequest{
		Id:          supplierID.String(),
		LanguageTag: lang,
	})

	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	var ImageURL string
	if supData.ImageURL != nil {
		ImageURL = *supData.ImageURL
	} else {
		ImageURL = "https://via.placeholder.com/150"
	}

	var distance float64
	var showDistance bool = false
	if reqData.Address != nil && supData.Longitude != nil && supData.Latitude != nil {
		user := geodist.Coord{Lat: float64(reqData.Address.Latitude), Lon: float64(reqData.Address.Longitude)}
		supplier := geodist.Coord{Lat: float64(*supData.Latitude), Lon: float64(*supData.Longitude)}

		_, km, err := geodist.VincentyDistance(user, supplier)
		if err == nil {
			distance = km
			showDistance = true
		}
	}

	navLinks := []Link{
		{URL: "/" + lang + "/suppliers", Text: translation.General.HomePage},
	}

	if categoryID != uuid.Nil {
		navLinks = append(navLinks, Link{
			URL:  "/" + lang + "/suppliers/" + slugidEncode(supplierID),
			Text: supData.Name,
		})

		// Category will be appended later
	} else {
		navLinks = append(navLinks, Link{
			URL:  "",
			Text: supData.Name,
		})
	}

	// TODO
	data.Supplier = Supplier{
		ID:       supplierID,
		Name:     supData.Name,
		ImageURL: ImageURL,
		NavLinks: navLinks,
	}

	if showDistance {
		data.Supplier.DescriptionBullets = []string{
			strconv.FormatFloat(distance, 'f', 2, 32) + translation.General.KmFromYou,
		}
	}

	reqData.wg.Add(1)
	go fillShoppingCart(ctx, &reqData, &data.ShoppingCart)

	reqData.wg.Add(1)
	go fillSupplierProducts(&ctx, &reqData, &data.Supplier)

	client := proto.NewSupplierServiceClient(reqData.SupplierConn)

	categoriesRep, err := client.GetSupplierCategories(ctx, &proto.SupplierCategoriesRequest{
		LanguageTag: lang,
		Id:          supplierID.String(),
	})

	if err != nil {
		log.Println(err)
		reqData.errOnce.Do(func() {
			reqData.errChan <- err
		})
	}

	if err == nil && categoriesRep.Categories != nil {
		count := 5
		if len(categoriesRep.Categories) < count {
			count = len(categoriesRep.Categories)
		}

		data.Supplier.Categories = make([]string, count)

		for i := 0; i < count; i++ {
			data.Supplier.Categories[i] = categoriesRep.Categories[i].Name
		}

		data.Categories = make([]Category, count)
		for i, category := range categoriesRep.Categories {
			id, err := uuid.Parse(category.Id)
			if err != nil {
				reqData.errOnce.Do(func() {
					reqData.errChan <- err
				})
				return
			}

			var url string
			if categoryID == id {
				url = "/" + lang + "/suppliers/" + slugidEncode(supplierID)
				data.Supplier.NavLinks = append(data.Supplier.NavLinks, Link{
					URL:  "",
					Text: category.Name,
				})
			} else {
				url = "/" + lang + "/suppliers/" + slugidEncode(supplierID) + "?category=" + slugidEncode(id)
			}

			data.Categories[i] = Category{
				ID:       id,
				Name:     category.Name,
				URL:      url,
				ImageURL: category.ImageUrl,
				Dim:      categoryID != uuid.Nil && id != categoryID,
			}
		}
	}

	reqData.wg.Wait()

	data.Total = data.ShoppingCart.Total

	select {
	case <-ctx.Done():
		switch ctx.Err() {
		case context.DeadlineExceeded:
			http.Error(w, "Timeout", http.StatusGatewayTimeout)
			return
		case context.Canceled:
			http.Error(w, "Canceled", http.StatusGatewayTimeout)
			return
		}
	case err := <-reqData.errChan:
		cancel()
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	default:
		w.Header().Set("Cache-Control", "private")
		var err error
		if r.Header.Get("HX-Boosted") == "true" {
			err = supplierPageTemplate.ExecuteTemplate(w, "content", data)
		} else {
			err = supplierPageTemplate.Execute(w, data)
		}
		if err != nil {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		return
	}
}
