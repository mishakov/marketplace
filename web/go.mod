module marketplace/web

go 1.20

require (
	github.com/goccy/go-json v0.10.2
	github.com/google/uuid v1.3.1
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/jftuga/geodist v1.0.0
	github.com/joho/godotenv v1.5.1
	github.com/processout/grpc-go-pool v1.2.1
	github.com/redis/go-redis/v9 v9.1.0
	github.com/taskcluster/slugid-go v1.1.0
	google.golang.org/grpc v1.57.0
	google.golang.org/protobuf v1.31.0
	gopkg.in/yaml.v3 v3.0.1
	marketplace/goods-and-suppliers v0.0.0-00010101000000-000000000000
	marketplace/shopping-cart v0.0.0-00010101000000-000000000000
	marketplace/user-account-service v0.0.0-00010101000000-000000000000
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/pborman/uuid v1.2.0 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230822172742-b8732ec3820d // indirect
)

replace (
	marketplace/goods-and-suppliers => ../goods-and-suppliers
	marketplace/shopping-cart => ../shopping-cart
	marketplace/user-account-service => ../user-account-service
)
