/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"embed"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"

	pool "github.com/processout/grpc-go-pool"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

//go:embed html localization
var files embed.FS

var suppliersPool *pool.Pool

func suppliersConnFactory() (*grpc.ClientConn, error) {
	return grpc.Dial("localhost:4000", grpc.WithTransportCredentials(insecure.NewCredentials()))
}

var shoppingCartPool *pool.Pool

func shoppingCartConnFactory() (*grpc.ClientConn, error) {
	return grpc.Dial("localhost:4001", grpc.WithTransportCredentials(insecure.NewCredentials()))
}

var authPool *pool.Pool

func authConnFactory() (*grpc.ClientConn, error) {
	return grpc.Dial("localhost:4002", grpc.WithTransportCredentials(insecure.NewCredentials()))
}

func main() {
	// Parse .env file
	godotenv.Load()

	GeoapifyAPIKey = os.Getenv("GEOAPIFY_API_KEY")
	if GeoapifyAPIKey == "" {
		panic("GEOAPIFY_API_KEY not set")
	}

	flag.Parse()

	err := initLocalization()
	if err != nil {
		panic(err)
	}

	err = init_templates()
	if err != nil {
		panic(err)
	}

	suppliersPool, err = pool.New(suppliersConnFactory, 10, 50, 10)
	if err != nil {
		panic(err)
	}

	shoppingCartPool, err = pool.New(shoppingCartConnFactory, 10, 50, 10)
	if err != nil {
		panic(err)
	}

	authPool, err = pool.New(authConnFactory, 10, 50, 10)
	if err != nil {
		panic(err)
	}

	r := mux.NewRouter()
	r.NotFoundHandler = http.HandlerFunc(notFoundHandler)
	// r.StrictSlash(true) TODO: probably a good idea to enable this in production

	// Detect language and redirect to the correct path
	r.HandleFunc("/", languageRedirector).Methods("GET")

	l := r.PathPrefix("/{lang}").Subrouter()
	// TODO: Middleware to detect bad language

	l.HandleFunc("/products/{product_id}", productHandler).Methods("GET")
	// r.HandleFunc("/products/{product_id}/inc", incrementProductHandler)
	l.HandleFunc("/suppliers", suppliersHandler).Methods("GET")
	l.HandleFunc("/suppliers/{supplier_id}", supplierPageHandler).Methods("GET")
	l.HandleFunc("/shopping_cart", shoppingCartHandler).Methods("GET")
	l.HandleFunc("/shopping_cart/total", shoppingCartTotalHandler).Methods("GET")
	l.HandleFunc("/shopping_cart/{product_id}", shoppingCartDeleteHandler).Methods("DELETE")
	l.HandleFunc("/shopping_cart/{product_id}", setProductQuantityCartHandler).Methods("POST").Queries("render_cart", "true")
	l.HandleFunc("/shopping_cart/{product_id}", setProductQuantityHandler).Methods("POST")
	l.HandleFunc("/location/suggestions", locationSuggestionsHandler).Methods("GET")
	l.HandleFunc("/location", locationPageHandler).Methods("GET")
	l.HandleFunc("/location", locationSetHandler).Methods("POST")
	l.HandleFunc("/login", loginHandler).Methods("GET")
	l.HandleFunc("/process_email", processEmailHandler).Methods("POST")
	l.HandleFunc("/continue_email", continueEmailHandler).Methods("POST")
	l.HandleFunc("/checkout", checkoutHandler).Methods("GET")
	l.HandleFunc("/checkout/{product_id}", checkoutSetQuantityHandler).Methods("POST")
	l.HandleFunc("/close_banner", closeBannerHandler).Methods("POST")

	// l.HandleFunc("/checkout/{product_id}", checkoutDeleteProductHandler).Methods("DELETE")

	l.HandleFunc("/", homeHandler).Methods("GET")

	// mid := handlers.RecoveryHandler()(r)
	mid := handlers.ProxyHeaders(r)
	mid = handlers.CombinedLoggingHandler(log.Writer(), mid)

	http.Handle("/", mid)

	fmt.Println("Server started at :8081")
	err = http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatalln("There was an error starting the server:", err)
	}
}

type NutritionalValue struct {
	Value float32
	Name  string
}

type ProductDetail struct {
	Class string
	Title string
	Text  string
}

type ProductCardParams struct {
	PageTitle       string
	PageDescription *string
	PageStyles      []string
	LangTag         string
	InlineQuantity  bool
	Overlay         bool
	AssetsServer    string

	ID                 string
	Title              string
	Images             []string
	Description        *string
	BulletPoints       []string
	QuantityText       string
	ValuesQuantityText *string
	NutritionalValues  []NutritionalValue
	Details            []ProductDetail

	PriceText string
	Stock     int
	Quantity  int
	Price     float32
	ShowInc   bool
	ShowDec   bool
}

var ProductPageStyles = []string{
	"http://127.0.0.1:5500/product.css",
}

var ProductCardTemplate *template.Template
var supplierPageTemplate *template.Template
var LocationPageTemplate *template.Template
var loginPageTemplate *template.Template
var CheckoutPageTemplate *template.Template

func init_templates() error {
	var err error
	ProductCardTemplate, err = template.ParseFS(files, "html/layout.html", "html/product/card_full.html")

	if err != nil {
		log.Print(err)
		return err
	}

	supplierListPageTemplate, err = template.ParseFS(files, "html/layout.html", "html/product/card_full.html", "html/suppliers_view.html", "html/categories/categories.html", "html/shopping_cart.html")
	if err != nil {
		log.Print(err)
		return err
	}

	supplierPageTemplate, err = template.ParseFS(files, "html/layout.html", "html/product/card_full.html", "html/suppliers_view.html", "html/categories/categories.html", "html/shopping_cart.html", "html/supplier_page.html")
	if err != nil {
		log.Print(err)
		return err
	}

	LocationPageTemplate, err = template.ParseFS(files, "html/layout.html", "html/location_page.html")
	if err != nil {
		log.Print(err)
		return err
	}

	loginPageTemplate, err = template.ParseFS(files, "html/layout.html", "html/login.html")
	if err != nil {
		log.Print(err)
		return err
	}

	CheckoutPageTemplate, err = template.ParseFS(files, "html/layout.html", "html/checkout_page.html")
	if err != nil {
		log.Print(err)
		return err
	}

	return nil
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	// Cache-Control: private, no-cache
	w.Header().Set("Cache-Control", "private, no-cache")

	// err := ValidateLocationSession(r)
	// if err != nil {
	// 	http.Redirect(w, r, "location", http.StatusFound)
	// 	return
	// }

	http.Redirect(w, r, "suppliers", http.StatusFound)
}

func closeBannerHandler(w http.ResponseWriter, r *http.Request) {
	// Cache-Control: private, no-cache
	w.Header().Set("Cache-Control", "private, no-cache")

	// Set cookie
	http.SetCookie(w, &http.Cookie{
		Name:     "banner_closed",
		Value:    "true",
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
		Secure:   true,
		MaxAge:   60 * 60 * 24,
	})

	w.WriteHeader(http.StatusOK)
}
