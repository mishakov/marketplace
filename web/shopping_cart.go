/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"log"
	"marketplace/goods-and-suppliers/proto"
	cart "marketplace/shopping-cart/cart"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

type ShoppingCartSupplier struct {
	SupplierID        uuid.UUID
	SupplierIDEncoded string
	SupplierName      string
	ImageURL          string
	Items             []ShoppingCartItem
	OrderText         string
	TotalText         string
	Total             float32
	LangTag           string
	Partial           bool

	EmptyCartTitle string
	EmptyCartText  string

	AddressTitle string
	AddressText  string
	EmptyLogoURL string
}

func getShoppingCartItems(ctx context.Context, reqData *RequestData, userID string, supplierID uuid.UUID) {
	defer reqData.shoppingCartWg.Done()

	conn, err := shoppingCartPool.Get(ctx)
	if err != nil {
		reqData.errOnce.Do(func() {
			reqData.errChan <- err
		})
		return
	}
	defer conn.Close()

	client := cart.NewShoppingCartServiceClient(conn)

	var reqSupplierID *string
	supplierIDStr := supplierID.String()
	if supplierID != uuid.Nil {
		reqSupplierID = &supplierIDStr
	}

	shoppingCartRep, err := client.GetShoppingCart(ctx, &cart.ShoppingCartRequest{
		UserID:     userID,
		SupplierID: reqSupplierID,
	})

	if err != nil {
		reqData.errOnce.Do(func() {
			reqData.errChan <- err
		})
		return
	}

	if shoppingCartRep.Items == nil {
		return
	}

	reqData.shoppingCart = make(map[string]*ShoppingCartItem, len(shoppingCartRep.Items))

	for _, item := range shoppingCartRep.Items {
		reqData.shoppingCart[item.ProductID] = &ShoppingCartItem{
			Quantity:   int(item.Quantity),
			Price:      item.Price,
			SupplierID: uuid.MustParse(item.SupplierID),
			ID:         item.ProductID,
			TotalPrice: item.Price * float32(item.Quantity),
			Max:        int(item.Stock),
		}
		reqData.cartTotal += item.Price * float32(item.Quantity)
	}
}

func shoppingCartHandler(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("HX-Request") != "true" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	// Set cache-control
	w.Header().Set("Cache-Control", "private, no-cache")

	values := r.URL.Query()
	supplierIDEncoded := values.Get("supplier")
	supplierID, _ := slugidDecode(supplierIDEncoded)

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusBadRequest)
		return
	}

	ctxData := ContextData{
		LanguageTag: lang,
	}

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	ctx := context.WithValue(timeoutCtx, dataKey, &ctxData)

	conn, err := suppliersPool.Get(timeoutCtx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	data := SuppliersListPageData{}

	reqData := RequestData{
		SupplierConn: conn,
		errChan:      make(chan error, 1),
		translation:  translation,
	}

	address, err := getLocation(ctx, r)
	if err == nil {
		reqData.Address = &address
	}

	func() {
		sessionID, err := r.Cookie("session_id")
		if err != nil {
			return
		}

		_, err = uuid.Parse(sessionID.Value)
		if err != nil {
			return
		}

		userID, err := getUserID(ctx, sessionID.Value)
		if err != nil {
			return
		}

		ctxData.UserID = userID

		reqData.shoppingCartWg.Add(1)
		go getShoppingCartItems(ctx, &reqData, *userID, supplierID)
	}()

	if supplierID == uuid.Nil {
		reqData.wg.Add(1)
		// TODO: Change fillShoppingCart function signature
		go fillShoppingCart(ctx, &reqData, &data.ShoppingCart)
		data.Total = data.ShoppingCart.Total

		reqData.wg.Wait()

		select {
		case <-ctx.Done():
			switch ctx.Err() {
			case context.DeadlineExceeded:
				http.Error(w, "Timeout", http.StatusGatewayTimeout)
				return
			case context.Canceled:
				http.Error(w, "Canceled", http.StatusGatewayTimeout)
				return
			}
		case err := <-reqData.errChan:
			cancel()
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		default:
			data.ShoppingCart.Partial = true
			err := supplierListPageTemplate.ExecuteTemplate(w, "shoppingCart", data.ShoppingCart)
			if err != nil {
				log.Print(err)
				http.Error(w, "Internal error", http.StatusInternalServerError)
				return
			}
			err = supplierListPageTemplate.ExecuteTemplate(w, "shoppingCartButtonStandalone", data.Total)
			if err != nil {
				log.Print(err)
				return
			}
			return
		}
	} else {
		supConn := proto.NewSupplierServiceClient(conn)

		supData, err := supConn.GetSupplierDescription(ctx, &proto.SupplierRequest{
			Id:          supplierID.String(),
			LanguageTag: lang,
		})

		if err != nil {
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}

		var ImageURL string
		if supData.ImageURL != nil {
			ImageURL = *supData.ImageURL
		} else {
			ImageURL = "https://via.placeholder.com/150"
		}

		sup := Supplier{
			ID:       supplierID,
			Name:     supData.Name,
			ImageURL: ImageURL,
		}

		var cart ShoppingCartSupplier

		reqData.wg.Add(1)
		go fillSupplierCart(ctx, &reqData, &sup, &cart)

		reqData.wg.Wait()

		select {
		case <-ctx.Done():
			switch ctx.Err() {
			case context.DeadlineExceeded:
				http.Error(w, "Timeout", http.StatusGatewayTimeout)
				return
			case context.Canceled:
				http.Error(w, "Canceled", http.StatusGatewayTimeout)
				return
			}
		case err := <-reqData.errChan:
			cancel()
			log.Print(err)
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		default:
			cart.Partial = true
			err := supplierListPageTemplate.ExecuteTemplate(w, "shoppingCartSingle", cart)
			if err != nil {
				log.Print(err)
				http.Error(w, "Internal error", http.StatusInternalServerError)
				return
			}
			err = supplierListPageTemplate.ExecuteTemplate(w, "shoppingCartButtonStandalone", cart.Total)
			if err != nil {
				log.Print(err)
				return
			}
			return
		}
	}
}

func sendEmptyCart(t *Translation, w http.ResponseWriter) {
	EmptyCartText := t.ShoppingCart.EmptyCartText
	err := supplierPageTemplate.ExecuteTemplate(w, "shoppingCartTotalInner", EmptyCartText)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}

func shoppingCartTotalHandler(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("HX-Request") != "true" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	lang := vars["lang"]

	translation := getTranslation(lang)
	if translation == nil {
		http.Error(w, "Invalid language", http.StatusBadRequest)
		return
	}

	// Set cache-control
	w.Header().Set("Cache-Control", "private, no-cache")

	// Get user ID
	userID, _, err := validateSessionCookie(r)
	ctx := r.Context()

	if err != nil {
		sendEmptyCart(translation, w)
		return
	}

	conn, err := shoppingCartPool.Get(ctx)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	client := cart.NewShoppingCartServiceClient(conn)

	shoppingCartRep, err := client.GetShoppingCartInfo(ctx, &cart.ShoppingCartRequest{
		UserID: userID,
	})

	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	if shoppingCartRep.TotalItems == 0 {
		sendEmptyCart(translation, w)
		return
	}

	text := translation.ShoppingCart.TotalText + " €" + strconv.FormatFloat(float64(shoppingCartRep.TotalPrice), 'f', 2, 32)
	err = supplierPageTemplate.ExecuteTemplate(w, "shoppingCartTotalInner", text)
	if err != nil {
		log.Print(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}
