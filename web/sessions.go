/*
Marketplace application; web frontend 
Copyright (C) 2023  Mikhail Kovalev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"net/http"
	"time"

	"github.com/goccy/go-json"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"google.golang.org/protobuf/types/known/emptypb"

	"marketplace/user-account-service/auth"
)

type SessionData struct {
	UserID uuid.UUID `json:"user_id"`
	Role   string    `json:"role"`
}

var rdb = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "",
	DB:       0,
})

func getUserID(ctx context.Context, sessionID string) (*string, error) {
	sessionKey := "session:" + sessionID
	userID, err := rdb.Get(ctx, sessionKey).Result()
	if err != nil {
		return nil, err
	}

	var data SessionData
	err = json.Unmarshal([]byte(userID), &data)
	if err != nil {
		return nil, err
	}

	id := data.UserID.String()

	return &id, nil
}

// Validate session cookie and return user ID
func validateSessionCookie(r *http.Request) (string, string, error) {
	sessionID, err := r.Cookie("session_id")
	if err != nil {
		return "", "", err
	}

	ctx := r.Context()

	_, err = uuid.Parse(sessionID.Value)
	if err != nil {
		return "", "", err
	}

	userID, err := getUserID(ctx, sessionID.Value)
	if err != nil {
		return "", "", err
	}

	return *userID, sessionID.Value, nil
}

func setSessionCookie(w http.ResponseWriter, sessionID string, ExpiresAt time.Time) {
	// Set session cookie
	http.SetCookie(w, &http.Cookie{
		Name:     "session_id",
		Value:    sessionID,
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
		Secure:   true,
		Expires:  ExpiresAt,
	})
}

func createSession(w http.ResponseWriter, r *http.Request) (string, string, error) {
	ctx := r.Context()
	conn, err := authPool.Get(ctx)
	if err != nil {
		return "", "", err
	}
	defer conn.Close()

	client := auth.NewAuthServiceClient(conn)

	sessionRep, err := client.CreateSession(ctx, &emptypb.Empty{})

	if err != nil {
		return "", "", err
	}

	setSessionCookie(w, sessionRep.SessionID, time.Time{})

	return sessionRep.UserID, sessionRep.SessionID, nil
}

func isLoggedIn(r *http.Request) (bool, error) {
	// Get session cookie
	sessionCookie, err := r.Cookie("session_id")

	if err != nil {
		return false, err
	}

	// Get session data
	sessionKey := "session:" + sessionCookie.Value
	sessionData, err := rdb.Get(r.Context(), sessionKey).Result()
	if err != nil {
		return false, err
	}

	var data SessionData
	err = json.Unmarshal([]byte(sessionData), &data)
	if err != nil {
		return false, err
	}

	// Check if user is a normal user or admin (since admin is also a user)
	switch data.Role {
	case "user", "admin":
		return true, nil
	}

	return false, nil
}

func setLocation(ctx context.Context, w http.ResponseWriter, address Address) error {
	// Marshal json and base64 encode
	addressJSON, err := json.Marshal(address)
	if err != nil {
		return err
	}

	addressBase64 := base64.URLEncoding.EncodeToString(addressJSON)

	// Sign address
	h := hmac.New(sha256.New, []byte(locationSecret))
	h.Write([]byte(addressJSON))
	signature := base64.URLEncoding.EncodeToString(h.Sum(nil))

	// Set cookies
	http.SetCookie(w, &http.Cookie{
		Name:     "location",
		Value:    addressBase64,
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
		Secure:   true,
	})

	http.SetCookie(w, &http.Cookie{
		Name:     "location_signature",
		Value:    signature,
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
		Secure:   true,
	})

	return nil
}

// Gets user location from session
// TODO: Save second trip to Redis
func getLocation(ctx context.Context, r *http.Request) (Address, error) {
	// Get location cookie
	locationCookie, err := r.Cookie("location")
	if err != nil {
		return Address{}, err
	}

	// Get location signature cookie
	locationSignatureCookie, err := r.Cookie("location_signature")
	if err != nil {
		return Address{}, err
	}

	// Verify signature
	locationDecoded, err := base64.URLEncoding.DecodeString(locationCookie.Value)
	if err != nil {
		return Address{}, err
	}

	h := hmac.New(sha256.New, []byte(locationSecret))
	h.Write(locationDecoded)
	expectedMac := h.Sum(nil)
	decodedSignature, err := base64.URLEncoding.DecodeString(locationSignatureCookie.Value)
	if err != nil {
		return Address{}, err
	}

	if !hmac.Equal(decodedSignature, expectedMac) {
		return Address{}, errors.New("invalid signature")
	}

	// Unmarshal location
	var address Address
	err = json.Unmarshal(locationDecoded, &address)
	if err != nil {
		return Address{}, err
	}

	return address, nil
}

// Verify that the user has a valid location
func ValidateLocationSession(r *http.Request) error {
	// Get location cookie
	locationCookie, err := r.Cookie("location")
	if err != nil {
		return err
	}

	// Get location signature cookie
	locationSignatureCookie, err := r.Cookie("location_signature")
	if err != nil {
		return err
	}

	// Verify signature
	locationDecoded, err := base64.URLEncoding.DecodeString(locationCookie.Value)
	if err != nil {
		return err
	}

	h := hmac.New(sha256.New, []byte(locationSecret))
	h.Write(locationDecoded)
	expectedMac := h.Sum(nil)
	decodedSignature, err := base64.URLEncoding.DecodeString(locationSignatureCookie.Value)
	if err != nil {
		return err
	}

	if !hmac.Equal(decodedSignature, expectedMac) {
		return errors.New("invalid signature")
	}

	return nil
}
