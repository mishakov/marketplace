/* 
	Marketplace application; user management service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"marketplace/user-account-service/auth"
	"strings"

	"github.com/google/uuid"
	"google.golang.org/grpc/status"
)

type RequestUserData struct {
	Email string `json:"email" binding:"required" validate:"email"`
}

func (s *grpcServer) RequestAuthorization(ctx context.Context, req *auth.AuthorizationRequest) (*auth.AuthorizationResponse, error) {
	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}
	defer tx.Rollback()

	// Check if email exists
	var user_id string
	err = tx.QueryRowContext(ctx, `INSERT INTO users (email, role)
	VALUES ($1, 'user')
	ON CONFLICT (email) DO NOTHING
	RETURNING id;`, req.Email).Scan(&user_id)
	switch err {
	case nil:
		// User was created
	case sql.ErrNoRows:
		// User already exists
		err = tx.QueryRowContext(ctx, `SELECT id FROM users WHERE email = $1`, req.Email).Scan(&user_id)
		if err != nil {
			log.Println(err)
			return nil, status.Error(500, err.Error())
		}
	default:
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	// Check if a token already exists
	var count int
	err = tx.QueryRowContext(ctx, `SELECT COUNT(*) FROM email_tokens WHERE user_id = $1 AND expiration_date > NOW() AND valid = TRUE`, user_id).Scan(&count)

	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	if count == 0 {
		// Create a new token and send it to the user
		token := uuid.New()
		_, err = tx.ExecContext(ctx, `INSERT INTO email_tokens (user_id, expiration_date, token)
		VALUES ($1, NOW() + INTERVAL '15 minutes', $2)`, user_id, token)
		if err != nil {
			log.Println(err)
			return nil, status.Error(500, err.Error())
		}

		// Send email
		err = sendVerificationEmail(req.Email, token.String())
		if err != nil {
			log.Println(err)
			return nil, status.Error(500, err.Error())
		}
	}

	// Find out if the user is registered
	var registered bool
	err = tx.QueryRowContext(ctx, `SELECT verified_email FROM users WHERE id = $1`, user_id).Scan(&registered)
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	var status auth.AuthorizationStatus
	if registered {
		status = auth.AuthorizationStatus_LOGGING_IN
	} else {
		status = auth.AuthorizationStatus_REGISTERING
	}

	return &auth.AuthorizationResponse{
		Status: status,
	}, nil
}

func (s *grpcServer) VerifyAuthorization(ctx context.Context, req *auth.VerifyRequest) (*auth.VerifyResponse, error) {
	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}
	defer tx.Rollback()

	// Get email request
	var userID string
	var tokenID, token *string
	var failedCount *int
	err = tx.QueryRowContext(ctx, `SELECT u.id, et.id, et.token, et.failed_count FROM users u
								   LEFT JOIN email_tokens et ON u.id = et.user_id AND valid = TRUE AND expiration_date > NOW()
								   WHERE u.email = $1
								   ORDER BY et.expiration_date DESC
								   LIMIT 1`, req.Email).Scan(&userID, &tokenID, &token, &failedCount)
	switch err {
	case nil:
		// Continue
	case sql.ErrNoRows:
		// No user with that email
		return &auth.VerifyResponse{
			Status:  auth.AuthorizationStatus_INVALID_EMAIL,
			Message: "No user with given email",
		}, nil
	default:
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	if tokenID == nil {
		return &auth.VerifyResponse{
			Status:  auth.AuthorizationStatus_INVALID_EMAIL,
			Message: "No token found",
		}, nil
	}

	// Check if too many failed attempts
	if failedCount != nil && *failedCount >= 5 {
		return &auth.VerifyResponse{
			Status:  auth.AuthorizationStatus_TOO_MANY_REQUESTS,
			Message: "Too many failed attempts",
		}, nil
	}

	// Check if code is correct
	if *token != req.Code {
		// Increment failed attempts
		_, err = tx.ExecContext(ctx, `UPDATE email_tokens SET failed_count = COALESCE(failed_count, 0) + 1 WHERE id = $1`, tokenID)
		if err != nil {
			log.Println(err)
			return nil, status.Error(500, err.Error())
		}

		tx.Commit()

		return &auth.VerifyResponse{
			Status:  auth.AuthorizationStatus_INVALID_CODE,
			Message: "Invalid token",
		}, nil
	}

	// Delete authorization tokens
	_, err = tx.ExecContext(ctx, `DELETE FROM email_tokens WHERE user_id = $1`, userID)
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	// Mark email as verified since we know that the user has access to it and check user's role while we're at it
	var userRole string
	err = tx.QueryRowContext(ctx, `UPDATE users SET verified_email = TRUE WHERE id = $1 RETURNING role`, userID).Scan(&userRole)
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	// Transfer shopping cart items to user and invalidate old sessions
	if req.ActiveUserID != nil {
		// Check if old user had a primary cart
		var hasPrimaryCart bool
		err = tx.QueryRowContext(ctx, `SELECT EXISTS(SELECT 1 FROM user_product_lists WHERE user_id = $1 AND primary_cart = TRUE)`, *req.ActiveUserID).Scan(&hasPrimaryCart)
		if err != nil {
			log.Println(err)
			return nil, status.Error(500, err.Error())
		}

		// If old user had a primary cart, disable new user's primary cart
		if hasPrimaryCart {
			_, err = tx.ExecContext(ctx, `UPDATE user_product_lists SET primary_cart = FALSE WHERE user_id = $1`, userID)
			if err != nil {
				log.Println(err)
				return nil, status.Error(500, err.Error())
			}
		}

		_, err = tx.ExecContext(ctx, `UPDATE user_product_lists SET user_id = $1 WHERE user_id = $2`, userID, req.ActiveUserID)
		if err != nil {
			log.Println(err)
			return nil, status.Error(500, err.Error())
		}
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	// Invalidate old session
	if req.ActiveUserID != nil {
		// Get active sessions
		userKey := "user:" + *req.ActiveUserID
		sessionsList, err := rdb.Get(ctx, userKey).Result()
		if err != nil {
			log.Println(err)
		} else {
			// Sessions are stored as a comma separated list of session IDs
			sessions := strings.Split(sessionsList, ",")
			for _, sessionID := range sessions {
				sessionKey := "session:" + sessionID
				rdb.Del(ctx, sessionKey)
			}
		}
	}

	// Create new session
	sessionID := uuid.New().String()
	sessionKey := "session:" + sessionID

	data := SessionDescriptor{
		UserID:      userID,
		AccountType: userRole,
	}

	dataEncoded, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	err = rdb.Set(ctx, sessionKey, string(dataEncoded), 0).Err()
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	userKey := "user:" + userID
	err = rdb.Append(ctx, userKey, sessionID+",").Err()
	if err != nil {
		log.Println(err)
		rdb.Del(ctx, sessionKey)
		return nil, status.Error(500, err.Error())
	}

	return &auth.VerifyResponse{
		Status:    auth.AuthorizationStatus_SUCCESS,
		Message:   "Successfully verified email",
		UserID:    userID,
		SessionID: sessionID,
	}, nil
}
