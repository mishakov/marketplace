/* 
	Marketplace application; user management service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"database/sql"
	"errors"
	"log"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func refreshToken(c *gin.Context) {
	// Get the token from the cookie
	cookie, err := c.Cookie("refresh_token")
	if err != nil {
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "unauthorized",
		})
		return
	}

	// Check if the token is valid
	strings := strings.Split(cookie, ":")
	if len(strings) != 2 {
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "unauthorized",
		})
		return
	}

	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}
	defer tx.Rollback()

	// Get the refresh token from the database
	var secret, user_id string
	err = tx.QueryRow("SELECT session_token, user_id FROM refresh_tokens WHERE session_id = $1 AND expiration_date > NOW() FOR UPDATE", strings[0]).Scan(&secret, &user_id)
	switch err {
	case nil:
		// Token exists
		if secret != strings[1] {
			result, err := tx.Exec("DELETE FROM refresh_tokens WHERE session_id = $1", strings[0])
			if err != nil {
				log.Println(err)
				c.JSON(500, gin.H{
					"status": "error",
					"error":  "internal server error",
				})
				return
			}

			rows, err := result.RowsAffected()
			if err != nil {
				log.Println(err)
				c.JSON(500, gin.H{
					"status": "error",
					"error":  "internal server error",
				})
				return
			}

			if rows != 1 {
				log.Println("Error deleting refresh token")
				c.JSON(500, gin.H{
					"status": "error",
					"error":  "internal server error",
				})
				return
			}

			err = tx.Commit()
			if err != nil {
				log.Println(err)
				c.JSON(500, gin.H{
					"status": "error",
					"error":  "internal server error",
				})
				return
			}

			// Token reused
			c.JSON(401, gin.H{
				"status": "error",
				"error":  "unauthorized",
			})
			return
		}

		// Generate a new session token
		var newSecret string
		err = tx.QueryRow("UPDATE refresh_tokens SET session_token = gen_random_uuid(), expiration_date = NOW() + INTERVAL '30 days' WHERE session_id = $1 AND expiration_date > NOW() RETURNING session_token", strings[0]).Scan(&newSecret)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		// Generate new JWT
		token, err := genJWT(user_id, strings[0])

		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		// Commit the transaction
		err = tx.Commit()
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		// Set the new session token in the cookie
		c.SetCookie("refresh_token", strings[0]+":"+newSecret, 60*60*24*30, "/refresh_token", "api.foo.bar", true, true)

		c.JSON(201, gin.H{
			"status": "success",
			"token":  token,
		})
		return
	case sql.ErrNoRows:
		// Token does not exist
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "unauthorized",
		})
	default:
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}
}

type LoginRequest struct {
	Email    string `json:"email" binding:"required" validate:"email"`
	Password string `json:"password" binding:"required"`
}

func login(c *gin.Context) {
	var request LoginRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request parameters",
		})
		return
	}

	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	// Get the user from the database
	var user_id, password_hash string
	err = tx.QueryRow("SELECT id, password FROM users WHERE email = $1", request.Email).Scan(&user_id, &password_hash)
	switch err {
	case nil:
		// User exists
		err = bcrypt.CompareHashAndPassword([]byte(password_hash), []byte(request.Password))
		if err != nil {
			c.JSON(401, gin.H{
				"status": "error",
				"error":  "unauthorized",
			})
			return
		}

		// Generate a new session token
		var session_id, session_token string
		err = tx.QueryRow("INSERT INTO refresh_tokens (user_id) VALUES ($1) RETURNING session_id, session_token", user_id).Scan(&session_id, &session_token)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		// Generate new JWT
		token, err := genJWT(user_id, session_id)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		err = tx.Commit()
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		// Set the new session token in the cookie
		c.SetCookie("refresh_token", session_id+":"+session_token, 60*60*24*30, "/refresh_token", "api.foo.bar", true, true)

		c.JSON(201, gin.H{
			"status": "success",
			"token":  token,
		})
	case sql.ErrNoRows:
		// User does not exist
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "unauthorized",
		})
	default:
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}
}

type RegisterRequest struct {
	Token      string `json:"token" binding:"required" verify:"uuid"`
	FirstName  string `json:"first_name" binding:"required" verify:"alphaunicode,min=2,max=50"`
	MiddleName string `json:"middle_name" binding:"-" verify:"alphaunicode,min=2,max=50"`
	LastName   string `json:"last_name" binding:"required" verify:"alphaunicode,min=2,max=50"`
	Password   string `json:"password" binding:"required" verify:"min=8,max=50"`
}

func register(c *gin.Context) {
	var request RegisterRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  err.Error(),
		})
		return
	}

	// Verify the token
	_, err := uuid.Parse(request.Token)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request parameters",
		})
		return
	}

	// Hash the password
	password_hash, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}
	defer tx.Rollback()

	// Update the user
	var user_id string
	err = tx.QueryRow("UPDATE users SET first_name = $1, middle_name = $2, last_name = $3, password = $4, verified_email = true "+
		"WHERE id = (SELECT user_id FROM email_verification_tokens WHERE token = $5) RETURNING id", request.FirstName, request.MiddleName, request.LastName, password_hash, request.Token).Scan(&user_id)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(404, gin.H{
				"status": "error",
				"error":  "request token not found",
			})
			return
		}

		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	// Delete verification tokens
	_, err = tx.Exec("DELETE FROM email_verification_tokens WHERE user_id = $1", user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	// Generate a new session token
	var session_id, session_token string
	err = tx.QueryRow("INSERT INTO refresh_tokens (user_id) VALUES ($1) RETURNING session_id, session_token", user_id).Scan(&session_id, &session_token)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	// Generate new JWT
	token, err := genJWT(user_id, session_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	// Set the new session token in the cookie
	c.SetCookie("refresh_token", session_id+":"+session_token, 60*60*24*30, "/refresh_token", "api.foo.bar", true, true)

	c.JSON(201, gin.H{
		"status": "success",
		"token":  token,
	})
}

type TokenClaims struct {
	UserID    string `json:"user_id"`
	SessionID string `json:"session_id"`
	jwt.StandardClaims
}

var jwtKey []byte

func genJWT(user_id, session_id string) (string, error) {
	claims := TokenClaims{
		user_id,
		session_id,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 30).Unix(),
			Issuer:    "api.foo.bar",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(jwtKey)
}

func extractBearerToken(c *gin.Context) (string, error) {
	// Extract the token from the Authorization header
	header := c.GetHeader("Authorization")

	token := strings.Split(header, " ")
	if len(token) != 2 || token[0] != "Bearer" {
		return "", errors.New("invalid authorization header")
	}

	return token[1], nil
}

func verifyJWT(c *gin.Context) {
	tokenString, err := extractBearerToken(c)
	if err != nil {
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "bad authorization header",
		})
		c.Abort()
		return
	}

	token, err := jwt.ParseWithClaims(tokenString, &TokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			c.JSON(401, gin.H{
				"status": "error",
				"error":  "invalid signature",
			})
			c.Abort()
			return
		}

		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad token",
		})
		c.Abort()
		return
	}

	if !token.Valid {
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "invalid token",
		})
		c.Abort()
		return
	}

	claims, ok := token.Claims.(*TokenClaims)
	if !ok {
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		c.Abort()
		return
	}

	c.Set("user_id", claims.UserID)
	c.Set("session_id", claims.SessionID)
	c.Next()
}

func logout(c *gin.Context) {
	session_id := c.MustGet("session_id").(string)

	_, err := db.Exec("DELETE FROM refresh_tokens WHERE session_id = $1", session_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	c.SetCookie("refresh_token", "", -1, "/refresh_token", "api.foo.bar", true, true)
	c.JSON(200, gin.H{
		"status": "success",
	})
	return
}

func changePassword(c *gin.Context) {
	var request struct {
		OldPassword string `json:"old_password" binding:"required" verify:"min=8,max=50"`
		NewPassword string `json:"new_password" binding:"required" verify:"min=8,max=50"`
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request",
		})
		return
	}

	user_id := c.MustGet("user_id").(string)
	session_id := c.MustGet("session_id").(string)

	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}
	defer tx.Rollback()

	var old_password string
	err = tx.QueryRow("SELECT password FROM users WHERE id = $1 FOR UPDATE", user_id).Scan(&old_password)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(old_password), []byte(request.OldPassword))
	if err != nil {
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "incorrect password",
		})
		return
	}

	hashed_password, err := bcrypt.GenerateFromPassword([]byte(request.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	_, err = tx.Exec("UPDATE users SET password = $1 WHERE id = $2", string(hashed_password), user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	_, err = tx.Exec("DELETE FROM refresh_tokens WHERE user_id = $1 AND session_id <> $2", user_id, session_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	c.JSON(200, gin.H{
		"status": "success",
	})
	return
}

func getUserInfo(c *gin.Context) {
	user_id := c.MustGet("user_id").(string)

	var user struct {
		FirstName  string `json:"first_name"`
		MiddleName string `json:"middle_name"`
		LastName   string `json:"last_name"`
		Email      string `json:"email"`
	}

	err := db.QueryRow("SELECT first_name, middle_name, last_name, email FROM users WHERE id = $1", user_id).Scan(&user.FirstName, &user.MiddleName, &user.LastName, &user.Email)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(404, gin.H{
				"status": "error",
				"error":  "user not found",
			})
			return
		}

		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	c.JSON(200, gin.H{
		"status":  "success",
		"details": user,
	})
}

func requestPasswordReset(c *gin.Context) {
	var request struct {
		Email string `json:"email" binding:"required" verify:"email"`
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad request",
		})
		return
	}

	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}
	defer tx.Rollback()

	var user_id string
	err = tx.QueryRow("SELECT id FROM users WHERE email = $1", request.Email).Scan(&user_id)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(404, gin.H{
				"status": "error",
				"error":  "user not found",
			})
			return
		}

		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	// Check that there is not too much password reset requests
	var count_short, count_day int
	err = tx.QueryRow("WITH t1 AS (SELECT COUNT(*) FROM password_reset_tokens WHERE user_id = $1 AND creation_date > NOW() - INTERVAL '15 minutes'), "+
		"t2 AS (SELECT COUNT(*) FROM password_reset_tokens WHERE user_id = $1 AND creation_date > NOW() - INTERVAL '1 day') "+
		"SELECT t1.count, t2.count FROM t1, t2", user_id).Scan(&count_short, &count_day)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	if count_short >= 1 || count_day >= 5 {
		c.JSON(429, gin.H{
			"status": "error",
			"error":  "too many requests",
		})
		return
	}

	var reset_token string
	err = tx.QueryRow("INSERT INTO password_reset_tokens (user_id) VALUES ($1) RETURNING token", user_id).Scan(&reset_token)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	// Send email
	err = sendPasswordResetEmail(request.Email, reset_token)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "failed to send email",
		})
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	c.JSON(200, gin.H{
		"status": "success",
	})
}

func resetPassword(c *gin.Context) {
	var request struct {
		Password string `json:"password" binding:"required" verify:"min=8,max=64"`
		Token    string `json:"token" binding:"required" verify:"uuid"`
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  err.Error(),
		})
		return
	}

	password_hash, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "failed to hash password",
		})
		return
	}

	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	var user_id string
	err = tx.QueryRow("DELETE FROM password_reset_tokens WHERE token = $1 AND expiration_date > NOW() RETURNING user_id", request.Token).Scan(&user_id)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(404, gin.H{
				"status": "error",
				"error":  "token not valid",
			})
			return
		}

		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	_, err = tx.Exec("UPDATE users SET password = $1 WHERE id = $2", password_hash, user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	_, err = tx.Exec("DELETE FROM password_reset_tokens WHERE user_id = $1", user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	_, err = tx.Exec("DELETE FROM refresh_tokens WHERE user_id = $1", user_id)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	c.JSON(200, gin.H{
		"status": "success",
	})
}
