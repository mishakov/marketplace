/* 
	Marketplace application; user management service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"encoding/json"
	"log"

	"marketplace/user-account-service/auth"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

var rdb = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "",
	DB:       0,
})

type grpcServer struct {
	auth.UnimplementedAuthServiceServer
}

func NewAuthServiceServer() auth.AuthServiceServer {
	return &grpcServer{}
}

type SessionDescriptor struct {
	UserID      string `json:"user_id"`
	AccountType string `json:"role"`
}

func (s *grpcServer) CreateSession(ctx context.Context, in *emptypb.Empty) (*auth.SessionDescriptor, error) {
	tx, err := db.Begin()
	if err != nil {
		return nil, status.Error(500, err.Error())
	}
	defer tx.Rollback()

	var userID string
	err = tx.QueryRow("INSERT INTO users(role) VALUES ('guest') RETURNING id").Scan(&userID)

	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	sessionID := uuid.New().String()

	sessionKey := "session:" + sessionID
	userKey := "user:" + userID

	data := SessionDescriptor{
		UserID:      userID,
		AccountType: "guest",
	}

	dataEncoded, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	err = rdb.Set(ctx, sessionKey, string(dataEncoded), 0).Err()
	if err != nil {
		log.Println(err)
		return nil, status.Error(500, err.Error())
	}

	err = rdb.Append(ctx, userKey, sessionID+",").Err()
	if err != nil {
		log.Println(err)
		rdb.Del(ctx, sessionKey)
		return nil, status.Error(500, err.Error())
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
		rdb.Del(ctx, sessionKey)
		rdb.Del(ctx, userKey)
		return nil, status.Error(500, err.Error())
	}

	return &auth.SessionDescriptor{
		UserID:    userID,
		SessionID: sessionID,
	}, nil
}
