/* 
	Marketplace application; user management service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

type SubscribeEarlyEmail struct {
	Email    string `json:"email" binding:"required" validate:"email"`
	Postcode string `json:"postcode" binding:"required" validate:"postcode_iso3166_alpha2"`
	Name     string `json:"name" binding:"required" validate:"alphanumunicode,max=255"`
	Referral string `json:"referral" binding:"required" validate:"max=255"`
}

const defaultNewsletterId = "early_newsletter"

// func subscribeEarly(c *gin.Context) {
// 	var json SubscribeEarlyEmail
// 	if err := c.ShouldBindJSON(&json); err != nil {
// 		c.JSON(400, gin.H{
// 			"error": err.Error(),
// 		})
// 		return
// 	}

// 	tx, err := db.Begin()
// 	if err != nil {
// 		log.Println(err)
// 		c.JSON(500, gin.H{
// 			"error": "internal server error",
// 		})
// 		return
// 	}
// 	defer tx.Rollback()

// 	// Check if email exists
// 	var exists bool
// 	err = tx.QueryRow("SELECT EXISTS(SELECT 1 FROM users WHERE email = $1)", json.Email).Scan(&exists)
// 	if err != nil {
// 		log.Println(err)
// 		c.JSON(500, gin.H{
// 			"error": "internal server error",
// 		})
// 		return
// 	}

// 	if exists {
// 		c.JSON(400, gin.H{
// 			"error": "email already exists",
// 		})
// 		return
// 	}

// 	var id string

// 	// Insert email
// 	err = tx.QueryRow("WITH t1 as (SELECT id FROM newsletter WHERE identifier=$5)"+
// 		", t2 AS (INSERT INTO users (first_name, email, postcode, registration_referrer) VALUES ($3, $1, $2, $4) RETURNING id)"+
// 		"INSERT INTO newsletter_subscriptions (newsletter_id, user_id)"+
// 		"SELECT t1.id, t2.id FROM t1, t2 RETURNING id", json.Email, json.Postcode, json.Name, json.Referral, defaultNewsletterId).Scan(&id)
// 	if err != nil {
// 		log.Println(err)
// 		c.JSON(500, gin.H{
// 			"error": "internal server error",
// 		})
// 		return
// 	}

// 	err = tx.Commit()
// 	if err != nil {
// 		log.Println(err)
// 		c.JSON(500, gin.H{
// 			"error": "internal server error",
// 		})
// 		return
// 	}

// 	c.JSON(200, gin.H{
// 		"message": "ok",
// 	})

// }
