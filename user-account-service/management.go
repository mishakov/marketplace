/* 
	Marketplace application; user management service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import "github.com/gin-gonic/gin"

type setNameData struct {
	FirstName  string `json:"first_name" binding:"required"`
	MiddleName string `json:"middle_name" binding:"-"`
	LastName   string `json:"last_name" binding:"required"`
}

func setName(c *gin.Context) {
	user_id := c.MustGet("user_id")

	var request setNameData
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  err.Error(),
		})
		return
	}

	rows, err := db.Exec("UPDATE users SET first_name = $1, middle_name = $2, last_name = $3 WHERE id = $4", request.FirstName, request.MiddleName, request.LastName, user_id)
	if err != nil {
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	if rowsAffected, _ := rows.RowsAffected(); rowsAffected == 0 {
		c.JSON(410, gin.H{
			"status": "error",
			"error":  "user does not exist",
		})
		return
	}

	c.JSON(201, gin.H{
		"status": "success",
	})
}
