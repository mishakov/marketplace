/* 
	Marketplace application; user management service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"net/smtp"
	"time"
)

var smtpAuth smtp.Auth

var smtp_hostname, smtp_port, smtp_username string

func sendMail(recipients []string, subject string, body string) error {
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"

	date := "Date: " + time.Now().Format(time.RFC1123Z) + "\n"
	from := "From: " + smtp_username + "\n"
	to := "To: "
	for i, recipient := range recipients {
		if i != 0 {
			to += ", "
		}
		to += recipient
	}
	to += "\n"

	subject = "Subject: " + subject + "\n"
	msg := []byte(date + from + to + subject + mime + "\n" + body)
	log.Println(smtp_hostname + ":" + smtp_port)
	return smtp.SendMail(smtp_hostname+":"+smtp_port, smtpAuth, smtp_username, recipients, msg)
}

func sendVerificationEmail(to string, token string) error {
	message := "Use the following code to verufy your account: " + token

	err := sendMail([]string{to}, "Create your account", message)

	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

func sendPasswordResetEmail(to string, token string) error {
	url := "http://localhost:8081/reset_password?token=" + token
	body := "<html><body><p>Please, click the following link to reset your password: <br><a href=\"" + url + "\">" + url + "</a></p></body></html>"

	err := sendMail([]string{to}, "Reset your password", body)

	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}
