/* 
	Marketplace application; user management service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"marketplace/user-account-service/auth"
	"net"
	"net/smtp"
	"os"

	"github.com/joho/godotenv"
	"google.golang.org/grpc"

	"database/sql"

	_ "github.com/lib/pq"
)

var db *sql.DB

var (
	port = flag.Int("port", 4002, "the server port")
)

func main() {
	godotenv.Load()
	flag.Parse()

	if os.Getenv("DB_USERNAME") == "" || os.Getenv("DB_PASS") == "" || os.Getenv("DB_HOST") == "" {
		panic("DB_USERNAME, DB_PASS, and DB_HOST must be set")
	}

	if os.Getenv("JWT_SECRET") == "" {
		panic("JWT_SECRET must be set")
	}

	if os.Getenv("SMTP_SERVER") == "" || os.Getenv("SMTP_USERNAME") == "" || os.Getenv("SMTP_PASSWORD") == "" || os.Getenv("SMTP_PORT") == "" {
		panic("SMTP_SERVER, SMTP_PORT, SMTP_USERNAME, and SMTP_PASSWORD must be set")
	}

	smtp_username = os.Getenv("SMTP_USERNAME")
	smtp_hostname = os.Getenv("SMTP_SERVER")
	smtp_port = os.Getenv("SMTP_PORT")

	smtpAuth = smtp.PlainAuth("", os.Getenv("SMTP_USERNAME"), os.Getenv("SMTP_PASSWORD"), os.Getenv("SMTP_SERVER"))

	jwtKey = []byte(os.Getenv("JWT_KEY"))

	var connStr = "postgresql://" + os.Getenv("DB_USERNAME") + ":" + os.Getenv("DB_PASS") + "@" + os.Getenv("DB_HOST") + "/playground?sslmode=disable"
	var err error
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	auth.RegisterAuthServiceServer(s, NewAuthServiceServer())
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
