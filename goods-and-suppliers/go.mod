module marketplace/goods-and-suppliers

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.9.1
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	google.golang.org/grpc v1.57.0
	google.golang.org/protobuf v1.30.0
	gopkg.in/guregu/null.v3 v3.5.0
)

