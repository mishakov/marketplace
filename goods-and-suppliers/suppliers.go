/*  Marketplace application; Goods and suppliers service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"context"
	"database/sql"
	"log"
	"marketplace/goods-and-suppliers/proto"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func NewSupplierServiceServer() proto.SupplierServiceServer {
	return &grpcServer{}
}

func (s *grpcServer) GetSupplierDescription(ctx context.Context, req *proto.SupplierRequest) (*proto.SupplierNameResponse, error) {
	supplierId := req.GetId()

	_, err := uuid.Parse(supplierId)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid supplier id")
	}

	lang := req.GetLanguageTag()

	err = db.QueryRow(`SELECT id FROM languages WHERE language_tag = $1`, lang).Scan(&lang)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.InvalidArgument, "invalid language")
		}

		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	defer tx.Rollback()

	var supplier proto.SupplierNameResponse
	err = tx.QueryRowContext(ctx, `
		SELECT
			COALESCE(translations.content, text_content.content) as name,
			suppliers.image_url,
			ST_X(suppliers.position::geometry) AS longitude,
			ST_Y(suppliers.position::geometry) AS latitude
		FROM suppliers
		LEFT JOIN text_content ON text_content.id = suppliers.name_text_id
		LEFT JOIN translations ON translations.text_content_id = text_content.id AND translations.language_id = $2
		WHERE suppliers.id = $1
	`, supplierId, lang).Scan(&supplier.Name, &supplier.ImageURL, &supplier.Longitude, &supplier.Latitude)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "supplier not found")
		}

		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &supplier, nil
}

func (s *grpcServer) GetSuppliers(ctx context.Context, req *proto.SuppliersRequest) (*proto.SuppliersList, error) {
	lang := req.GetLanguageTag()

	err := db.QueryRow(`SELECT id FROM languages WHERE language_tag = $1`, lang).Scan(&lang)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.InvalidArgument, "invalid language")
		}

		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	defer tx.Rollback()

	if req.CategoryID != nil {
		_, err = tx.ExecContext(ctx, `
			CREATE TEMPORARY TABLE categories_expanded ON COMMIT DROP AS (
				WITH RECURSIVE expanded AS (
					SELECT id
					FROM product_categories
					WHERE id = $1
					UNION
					SELECT pc.id
					FROM product_categories pc
					JOIN expanded e ON e.id = pc.parent
				)
				SELECT id FROM expanded
			)
		`, req.GetCategoryID())

		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
	}

	log.Println(req.DropoffPointIDs)

	var rows *sql.Rows
	if req.CategoryID == nil {
		rows, err = tx.QueryContext(ctx, `
			SELECT
				suppliers.id,
				COALESCE(translations.content, text_content.content) as name,
				suppliers.image_url,
				ST_X(suppliers.position::geometry) AS longitude,
				ST_Y(suppliers.position::geometry) AS latitude
			FROM suppliers
			LEFT JOIN text_content ON text_content.id = suppliers.name_text_id
			LEFT JOIN translations ON translations.text_content_id = text_content.id AND translations.language_id = $1
			WHERE suppliers.id IN (
				SELECT supplier_id FROM dropoff_supplier WHERE dropoff_point_id = ANY ($2)) AND suppliers.id IN (
					SELECT supplier_id FROM supplier_categories)
		`, lang, pq.Array(req.DropoffPointIDs))
	} else {
		rows, err = tx.QueryContext(ctx, `
		SELECT
			suppliers.id,
			COALESCE(translations.content, text_content.content) as name,
			suppliers.image_url,
			ST_X(suppliers.position::geometry) AS longitude,
			ST_Y(suppliers.position::geometry) AS latitude
		FROM suppliers
		LEFT JOIN text_content ON text_content.id = suppliers.name_text_id
		LEFT JOIN translations ON translations.text_content_id = text_content.id AND translations.language_id = $1
		LEFT JOIN supplier_categories ON supplier_categories.supplier_id = suppliers.id AND supplier_categories.has_public_products = true
		WHERE supplier_categories.category_id IN (SELECT id FROM categories_expanded) AND suppliers.id IN (
			SELECT supplier_id FROM dropoff_supplier WHERE dropoff_point_id = ANY ($2))
	`, lang, pq.Array(req.DropoffPointIDs))
	}
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	var suppliers []*proto.Supplier

	for rows.Next() {
		supplier := &proto.Supplier{}
		err := rows.Scan(&supplier.Id, &supplier.Name, &supplier.ImageUrl, &supplier.Longitude, &supplier.Latitude)
		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}

		suppliers = append(suppliers, supplier)
	}

	tx.Commit()

	return &proto.SuppliersList{Suppliers: suppliers}, nil
}

func (s *grpcServer) GetSupplierCategories(ctx context.Context, req *proto.SupplierCategoriesRequest) (*proto.CategoriesList, error) {
	lang := req.GetLanguageTag()

	err := db.QueryRowContext(ctx, `SELECT id FROM languages WHERE language_tag = $1`, lang).Scan(&lang)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.InvalidArgument, "invalid language")
		}

		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	rows, err := db.QueryContext(ctx, `
		SELECT 
			COALESCE(trans.content, t.content) AS name,
			pc.id AS id,
			pc.image_url AS image_url
		FROM product_categories pc
		LEFT JOIN text_content t ON pc.name_text_id = t.id
		LEFT JOIN translations trans ON t.id = trans.text_content_id AND trans.language_id = $2
		WHERE pc.id IN (
			SELECT category_id
			FROM supplier_categories
			WHERE supplier_id = $1
		)
		ORDER BY pc.order_position ASC
		LIMIT $3`, req.GetId(), lang, req.Limit)

	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	var categories []*proto.Category
	for rows.Next() {
		var category proto.Category
		err := rows.Scan(&category.Name, &category.Id, &category.ImageUrl)
		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}

		categories = append(categories, &category)
	}

	return &proto.CategoriesList{Categories: categories}, nil
}

func (s grpcServer) GetProductsSummary(ctx context.Context, req *proto.SupplierProductsRequest) (*proto.ProductsSummary, error) {
	lang := req.GetLanguageTag()

	err := db.QueryRowContext(ctx, `SELECT id FROM languages WHERE language_tag = $1`, lang).Scan(&lang)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.InvalidArgument, "invalid language")
		}

		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	defer tx.Rollback()

	if req.CategoryID != nil {
		_, err = tx.ExecContext(ctx, `
			CREATE TEMPORARY TABLE categories_expanded ON COMMIT DROP AS (
				WITH RECURSIVE expanded AS (
					SELECT id
					FROM product_categories
					WHERE id = $1
					UNION
					SELECT pc.id
					FROM product_categories pc
					JOIN expanded e ON e.id = pc.parent
				)
				SELECT id FROM expanded
			)
		`, req.GetCategoryID())

		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
	}

	var rows *sql.Rows

	if req.CategoryID == nil {
		rows, err = tx.QueryContext(ctx, `
			SELECT DISTINCT ON (p.id)
				p.id,
				p.description,
				p.name,
				p.price,
				i.image_url,
				COALESCE(pq.quantity, 0) as quantity
			FROM translated_public_products p
			LEFT JOIN product_quantities pq ON pq.product_id = p.id
			LEFT JOIN product_images i ON i.product_id = p.id
			WHERE p.supplier_id = $1 AND p.language_id = $2
			OFFSET $3 LIMIT $4`, req.GetId(), lang, req.Offset, req.GetLimit())

		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
	} else {
		rows, err = tx.QueryContext(ctx, `
		SELECT DISTINCT ON (p.id)
			p.id,
			p.description,
			p.name,
			p.price,
			i.image_url,
			COALESCE(pq.quantity, 0) as quantity
		FROM translated_public_products p
		LEFT JOIN product_quantities pq ON pq.product_id = p.id
		LEFT JOIN product_images i ON i.product_id = p.id
		WHERE
			p.supplier_id = $1
			AND p.language_id = $2
			AND p.category_id IN (SELECT id FROM categories_expanded)
		OFFSET $3 LIMIT $4`, req.GetId(), lang, req.Offset, req.GetLimit())

		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
	}

	var products []*proto.ProductDescriptionShort
	for rows.Next() {
		product := &proto.ProductDescriptionShort{}
		err := rows.Scan(&product.Id, &product.Description, &product.Name, &product.Price, &product.ImageUrl, &product.AvailableQuantity)
		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}

		products = append(products, product)
	}

	tx.Commit()

	return &proto.ProductsSummary{Products: products}, nil
}
