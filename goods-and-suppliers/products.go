/*  Marketplace application; Goods and suppliers service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"context"
	"database/sql"
	"log"

	"marketplace/goods-and-suppliers/proto"

	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type grpcServer struct {
	proto.UnimplementedProductServiceServer
	proto.UnimplementedSupplierServiceServer
	proto.UnimplementedCategoriesServiceServer
}

func NewProductServiceServer() proto.ProductServiceServer {
	return &grpcServer{}
}

func (s *grpcServer) GetProduct(ctx context.Context, req *proto.ProductRequest) (*proto.ProductResponse, error) {
	productId := req.GetId()

	_, err := uuid.Parse(productId)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid product id")
	}

	lang := req.GetLanguageTag()

	err = db.QueryRow(`SELECT id FROM languages WHERE language_tag = $1`, lang).Scan(&lang)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.InvalidArgument, "invalid language")
		}

		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	defer tx.Rollback()

	var product proto.ProductResponse
	err = tx.QueryRowContext(ctx, `
		SELECT
			id,
			category_id,
			supplier_id,
			TRUE as is_publiched,
			name,

			unit_name,
			unit_quantity,

			description,
			composition,
			storage_time_days,
			storage_conditions,

			price
		FROM translated_public_products
		WHERE id = $1 AND language_id = $2`,
		productId, lang).Scan(
		&product.Id,
		&product.CategoryId,
		&product.SupplierId,
		&product.IsPublished,
		&product.Name,
		&product.UnitName,
		&product.UnitQuantity,
		&product.Description,
		&product.Composition,
		&product.StorageTime,
		&product.StorageConditions,
		&product.Price,
	)

	switch err {
	case sql.ErrNoRows:
		return nil, status.Errorf(codes.NotFound, "product not found")
	case nil:
		break
	default:
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	imageRows, err := tx.QueryContext(ctx, `SELECT image_url FROM product_images WHERE product_id = $1`, productId)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	defer imageRows.Close()

	product.Images = make([]string, 0)
	for imageRows.Next() {
		var image string
		err := imageRows.Scan(&image)
		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		product.Images = append(product.Images, image)
	}

	if imageRows.Err() != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	bulletRows, err := tx.QueryContext(ctx, `SELECT text FROM translated_bullet_points WHERE product_id = $1 AND language_id = $2`, productId, lang)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	product.BulletPoints = make([]string, 0)
	for bulletRows.Next() {
		var bullet string
		err := bulletRows.Scan(&bullet)
		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		product.BulletPoints = append(product.BulletPoints, bullet)
	}

	if bulletRows.Err() != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	tx.Commit()

	return &product, nil
}

func (s *grpcServer) GetProductShort(ctx context.Context, req *proto.ProductRequest) (*proto.ProductResponseShort, error) {
	productID := req.GetId()
	lang_tag := req.GetLanguageTag()

	_, err := uuid.Parse(productID)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid product id")
	}

	err = db.QueryRowContext(ctx, `SELECT id FROM languages WHERE language_tag = $1`, lang_tag).Scan(&lang_tag)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.InvalidArgument, "invalid language")
		}

		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	defer tx.Rollback()

	var product proto.ProductResponseShort
	err = tx.QueryRowContext(ctx, `
		SELECT DISTINCT ON(p.id)
			p.id,
			TRUE as is_published,
			p.name,
			p.unit_name,
			p.unit_quantity,
			i.image_url
		FROM
			translated_public_products p
		LEFT JOIN product_images i ON i.product_id = p.id
		WHERE
			p.id = $1 AND p.language_id = $2`,
		productID, lang_tag).Scan(
		&product.Id, &product.IsPublished, &product.Name, &product.UnitName, &product.UnitQuantity, &product.ImageURL)

	switch err {
	case sql.ErrNoRows:
		return nil, status.Errorf(codes.NotFound, "product not found")
	case nil:
		break
	default:
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	tx.Commit()

	return &product, nil
}
