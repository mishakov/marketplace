/*  Marketplace application; Goods and suppliers service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"context"
	"database/sql"
	"log"
	"marketplace/goods-and-suppliers/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func newCategoriesServer() proto.CategoriesServiceServer {
	return &grpcServer{}
}

func (s *grpcServer) GetCategories(ctx context.Context, req *proto.CategoriesRequest) (*proto.CategoriesResponse, error) {
	var lang string = req.GetLanguageTag()

	query := `
		SELECT id
		FROM languages
		WHERE language_tag = $1`

	var lang_id string
	err := db.QueryRow(query, lang).Scan(&lang_id)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Errorf(codes.InvalidArgument, "invalid language")
	default:
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	query = `
		SELECT 
			pc.id,
			pc.image_url,
			COALESCE(trans.content, t.content) AS name
		FROM product_categories pc
		LEFT JOIN text_content t ON pc.name_text_id = t.id
		LEFT JOIN translations trans ON t.id = trans.text_content_id AND trans.language_id = $1
		WHERE pc.parent IS NULL
		ORDER BY pc.order_position ASC`

	rows, err := db.Query(query, lang_id)
	if err != nil {
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	categories := []*proto.Category{}

	for rows.Next() {
		category := &proto.Category{}
		err := rows.Scan(&category.Id, &category.ImageUrl, &category.Name)
		if err != nil {
			log.Print(err)
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		categories = append(categories, category)
	}

	return &proto.CategoriesResponse{Categories: categories}, nil
}

func (s *grpcServer) GetCategory(ctx context.Context, req *proto.CategoryRequest) (*proto.Category, error) {
	var lang string = req.GetLanguageTag()
	var category_id string = req.GetId()

	query := `
		SELECT id
		FROM languages
		WHERE language_tag = $1`

	var lang_id string
	err := db.QueryRow(query, lang).Scan(&lang_id)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Errorf(codes.InvalidArgument, "invalid language")
	default:
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	query = `
		SELECT
			pc.id,
			pc.image_url,
			COALESCE(trans.content, t.content) AS name
		FROM product_categories pc
		LEFT JOIN text_content t ON pc.name_text_id = t.id
		LEFT JOIN translations trans ON t.id = trans.text_content_id AND trans.language_id = $1
		WHERE pc.id = $2`

	category := &proto.Category{}
	err = db.QueryRow(query, lang_id, category_id).Scan(&category.Id, &category.ImageUrl, &category.Name)
	switch err {
	case nil:
		break
	case sql.ErrNoRows:
		return nil, status.Errorf(codes.InvalidArgument, "invalid category id")
	default:
		log.Print(err)
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return category, nil
}
