/*  Marketplace application; Goods and suppliers service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"errors"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type TokenClaims struct {
	UserID    string `json:"user_id"`
	SessionID string `json:"session_id"`
	jwt.StandardClaims
}

var jwtKey []byte

func extractBearerToken(c *gin.Context) (string, error) {
	// Extract the token from the Authorization header
	header := c.GetHeader("Authorization")

	token := strings.Split(header, " ")
	if len(token) != 2 || token[0] != "Bearer" {
		return "", errors.New("invalid authorization header")
	}

	return token[1], nil
}

func verifyJWT(c *gin.Context) {
	tokenString, err := extractBearerToken(c)
	if err != nil {
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "bad authorization header",
		})
		c.Abort()
		return
	}

	token, err := jwt.ParseWithClaims(tokenString, &TokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			c.JSON(401, gin.H{
				"status": "error",
				"error":  "invalid signature",
			})
			c.Abort()
			return
		}

		c.JSON(400, gin.H{
			"status": "error",
			"error":  "bad token",
		})
		c.Abort()
		return
	}

	if !token.Valid {
		c.JSON(401, gin.H{
			"status": "error",
			"error":  "invalid token",
		})
		c.Abort()
		return
	}

	claims, ok := token.Claims.(*TokenClaims)
	if !ok {
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		c.Abort()
		return
	}

	c.Set("user_id", claims.UserID)
	c.Set("session_id", claims.SessionID)
	c.Next()
}
