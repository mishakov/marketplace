/*  Marketplace application; Goods and suppliers service
	Copyright (C) 2023  Mikhail Kovalev

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package main

import (
	"database/sql"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gopkg.in/guregu/null.v3"
)

func getSuppliersInRadius(c *gin.Context) {
	longitude := c.Query("longitude")
	latitude := c.Query("latitude")
	radius := c.DefaultQuery("radius_km", "30")

	longitude_float, err := strconv.ParseFloat(longitude, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid longitude",
		})
		return
	}

	latitude_float, err := strconv.ParseFloat(latitude, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid latitude",
		})
		return
	}

	radius_int, err := strconv.Atoi(radius)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid radius",
		})
		return
	}

	// type Product struct {
	// 	ID       string  `DB:"product_id" json:"id"`
	// 	Name     string  `DB:"name" json:"name"`
	// 	ImageURL string  `DB:"image_url" json:"image"`
	// 	Price    float64 `DB:"price" json:"price"`
	// }

	type Supplier struct {
		ID          string      `DB:"ID" json:"id"`
		Name        string      `DB:"name" json:"name"`
		Description null.String `DB:"description" json:"description"`
		Longitude   float64     `DB:"longitude" json:"longitude"`
		Latitude    float64     `DB:"latitude" json:"latitude"`
		Distance    float64     `DB:"distance" json:"distance"`
	}

	radius_meters := radius_int * 1000

	// Atrocious (it was written at 3 AM)
	query_string := "SELECT id, name, description, ST_X(position::geometry) as longitude, ST_Y(position::geometry) as latitude, ST_Distance(position, ST_MakePoint($1, $2)) AS distance FROM get_suppliers_in_radius(ST_MakePoint($1, $2), $3)"
	rows, err := db.Query(query_string, longitude_float, latitude_float, radius_meters)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	var suppliers []Supplier = make([]Supplier, 0)
	for rows.Next() {
		var supplier Supplier
		err := rows.Scan(&supplier.ID, &supplier.Name, &supplier.Description, &supplier.Longitude, &supplier.Latitude, &supplier.Distance)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		suppliers = append(suppliers, supplier)
	}

	c.JSON(200, gin.H{
		"status":    "success",
		"suppliers": suppliers,
	})
}

func get_product_images(c *gin.Context) {
	product_id := c.Param("product_id")

	_, err := uuid.Parse(product_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid product id",
		})
		return
	}

	type ProductImage struct {
		ID          string      `DB:"ID" json:"id"`
		ImageURL    string      `DB:"image_url" json:"image_url"`
		Description null.String `DB:"description" json:"description"`
	}

	query_string := "SELECT id, image_url, description FROM public_product_images WHERE product_id = $1"
	rows, err := db.Query(query_string, product_id)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	var product_images []ProductImage = make([]ProductImage, 0)
	for rows.Next() {
		var product_image ProductImage
		err := rows.Scan(&product_image.ID, &product_image.ImageURL, &product_image.Description)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}
		product_images = append(product_images, product_image)
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   product_images,
	})
}

func getProductInfo(c *gin.Context) {
	product_id := c.Param("product_id")

	_, err := uuid.Parse(product_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid product id",
		})
		return
	}

	type Product struct {
		Name        string      `DB:"name" json:"name"`
		CategoryID  string      `DB:"category_id" json:"category_id"`
		SupplierID  string      `DB:"supplier_id" json:"supplier_id"`
		Description null.String `DB:"description" json:"description"`
	}

	var product Product
	query_string := "SELECT name, category_id, supplier_id, description FROM products WHERE id = $1 AND public = TRUE"
	err = db.QueryRow(query_string, product_id).Scan(&product.Name, &product.CategoryID, &product.SupplierID, &product.Description)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(404, gin.H{
				"status": "error",
				"error":  "product not found",
			})
			return
		}

		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   product,
	})
}

func getProducts(c *gin.Context) {
	supplier_id := c.Param("supplier_id")

	_, err := uuid.Parse(supplier_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid supplier id",
		})
		return
	}

	type Product struct {
		ID          string      `DB:"product_id" json:"id"`
		Name        string      `DB:"name" json:"name"`
		CategoryID  string      `DB:"category_id" json:"category_id"`
		Description null.String `DB:"description" json:"description"`
	}

	query_string := "SELECT id AS product_id, name, category_id, description FROM products WHERE supplier_id = $1 AND public = TRUE"
	rows, err := db.Query(query_string, supplier_id)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	var products []Product = make([]Product, 0)
	for rows.Next() {
		var product Product
		err := rows.Scan(&product.ID, &product.Name, &product.CategoryID, &product.Description)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}
		products = append(products, product)
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   products,
	})

	return
}

func getSupplierInfo(c *gin.Context) {
	supplier_id := c.Param("supplier_id")

	_, err := uuid.Parse(supplier_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid supplier id",
		})
		return
	}

	type Supplier struct {
		Name        string      `DB:"name" json:"name"`
		Description null.String `DB:"description" json:"description"`
		Latitude    float64     `DB:"latitude" json:"latitude"`
		Longitude   float64     `DB:"longitude" json:"longitude"`
	}

	var supplier Supplier
	query_string := "SELECT name, description, latitude, longitude FROM suppliers WHERE id = $1 AND public = TRUE AND deletion_date IS NULL"
	err = db.QueryRow(query_string, supplier_id).Scan(&supplier.Name, &supplier.Description, &supplier.Latitude, &supplier.Longitude)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(404, gin.H{
				"status": "error",
				"error":  "supplier not found",
			})
			return
		}

		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   supplier,
	})
}

func getProductsInRadius(c *gin.Context) {
	category_id := c.Param("category_id")
	_, err := uuid.Parse(category_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid category id",
		})
		return
	}

	latitude, err := strconv.ParseFloat(c.Query("latitude"), 64)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid latitude",
		})
		return
	}

	longitude, err := strconv.ParseFloat(c.Query("longitude"), 64)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid longitude",
		})
		return
	}

	radius_km, err := strconv.ParseFloat(c.DefaultQuery("radius_km", "30"), 64)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid radius",
		})
		return
	}

	radius := radius_km * 1000

	type Product struct {
		SupplierID  string      `DB:"supplier_id" json:"supplier_id"`
		ID          string      `DB:"product_id" json:"id"`
		Name        string      `DB:"name" json:"name"`
		CategoryID  string      `DB:"category_id" json:"category_id"`
		Description null.String `DB:"description" json:"description"`
	}

	query := `
		WITH RECURSIVE category_tree AS (
			SELECT id, parent
			FROM product_categories
			WHERE id = $1
			UNION
			SELECT pc.id, pc.parent
			FROM product_categories pc
			INNER JOIN category_tree ct ON ct.id = pc.parent
		)
		SELECT 
			s.id AS supplier_id,
			p.id AS product_id,
			p.name,
			p.category_id,
			p.description
		FROM
			suppliers s
			JOIN public_products p ON p.supplier_id = s.id
			JOIN category_tree ct ON ct.id = p.category_id
		WHERE
			ST_Dwithin(s.position, ST_MakePoint($2, $3)::geography, $4)`

	stmt, err := db.Prepare(query)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query(category_id, longitude, latitude, radius)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	var products []Product = make([]Product, 0)
	for rows.Next() {
		var product Product
		err := rows.Scan(&product.SupplierID, &product.ID, &product.Name, &product.CategoryID, &product.Description)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}
		products = append(products, product)
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   products,
	})
}

func getAllProducts(c *gin.Context) {
	user_id := c.MustGet("user_id").(string)
	supplier_id := c.Param("supplier_id")
	_, err := uuid.Parse(supplier_id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid supplier id",
		})
		return
	}

	type Product struct {
		ID          string      `DB:"product_id" json:"id"`
		Name        string      `DB:"name" json:"name"`
		CategoryID  string      `DB:"category_id" json:"category_id"`
		Description null.String `DB:"description" json:"description"`
		IsPublic    bool        `DB:"public" json:"public"`
	}

	is_manager_query := `
		SELECT COUNT(*) > 0
		FROM supplier_user_roles
		WHERE supplier_id = $1 AND user_id = $2 AND role = 'manager'`

	var is_manager bool
	err = db.QueryRow(is_manager_query, supplier_id, user_id).Scan(&is_manager)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	if !is_manager {
		c.JSON(403, gin.H{
			"status": "error",
			"error":  "not supplier manager",
		})
		return
	}

	query := `
		SELECT id, name, category_id, description, public
		FROM products
		WHERE supplier_id = $1`

	stmt, err := db.Prepare(query)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	rows, err := stmt.Query(supplier_id)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	var products []Product = make([]Product, 0)
	for rows.Next() {
		var product Product
		err := rows.Scan(&product.ID, &product.Name, &product.CategoryID, &product.Description, &product.IsPublic)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
		}
		products = append(products, product)
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   products,
	})
}

func getCategories(c *gin.Context) {
	var lang string = c.Param("lang")

	query := `
		SELECT COUNT(*) > 0
		FROM languages
		WHERE language_tag = $1`

	var lang_exists bool
	err := db.QueryRow(query, lang).Scan(&lang_exists)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	if !lang_exists {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid language",
		})
		return
	}

	type Category struct {
		ID    string `DB:"id" json:"id"`
		Name  string `DB:"name" json:"name"`
		Image string `DB:"image_url" json:"image"`
	}

	query = `
		SELECT 
			pc.id,
			pc.image_url,
			COALESCE(trans.content, t.content) AS name
		FROM product_categories pc
		LEFT JOIN text_content t ON pc.name_text_id = t.id
		LEFT JOIN translations trans ON t.id = trans.text_content_id AND trans.language_id = (SELECT id FROM languages WHERE language_tag = $1)
		WHERE pc.parent IS NULL
		ORDER BY pc.order_position ASC`

	rows, err := db.Query(query, lang)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}

	var categories []Category = make([]Category, 0)

	if rows == nil {
		c.JSON(200, gin.H{
			"status": "success",
			"data":   categories,
		})
		return
	}

	for rows.Next() {
		var category Category
		err := rows.Scan(&category.ID, &category.Image, &category.Name)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
		}
		categories = append(categories, category)
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   categories,
	})
}

func describeProduct(c *gin.Context) {
	// Tell the client that we respond with JSON
	c.Writer.Header().Set("Content-Type", "application/json")

	// Ask for a 10 minute cache
	c.Writer.Header().Set("Cache-Control", "public, max-age=600")

	var id string = c.Param("product_id")
	var lang string = c.Param("lang")

	type Product struct {
		ID                string      `DB:"product_id" json:"id"`
		CategoryName      string      `DB:"category_name" json:"category_name"`
		Name              string      `DB:"name" json:"name"`
		Images            []string    `DB:"images" json:"images"`
		Unit              string      `DB:"unit" json:"unit"`
		BulletPoints      []string    `DB:"bullet_points" json:"bullet_points"`
		Description       null.String `DB:"description" json:"description"`
		NutricionalValues []string    `DB:"nutricional_values" json:"nutricional_values"`
		Composition       null.String `DB:"composition" json:"composition"`
		StorageTime       int         `DB:"storage_time" json:"storage_time"`
		StorageInfo       null.String `DB:"storage_info" json:"storage_info"`
		Manufacturer      null.String `DB:"manufacturer" json:"manufacturer"`
		Price             float64     `DB:"price" json:"price"`
	}

	var last_modified time.Time

	_, err := uuid.Parse(id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid product id",
		})
		return
	}

	err = db.QueryRow(`SELECT id FROM languages WHERE language_tag = $1`, lang).Scan(&lang)
	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(400, gin.H{
				"status": "error",
				"error":  "invalid language",
			})
			return
		}

		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	var product Product
	err = db.QueryRow(`
		SELECT
			id,
			category_name,
			name,
			CONCAT(unit_quantity, ' ', unit_name) AS unit,
			description,
			composition,
			storage_time_days,
			storage_conditions,
			manufacturer,
			updated_at
		FROM translated_public_products
		WHERE id = $1 AND language_id = $2`,
		id, lang).Scan(
		&product.ID,
		&product.CategoryName,
		&product.Name,
		&product.Unit,
		&product.Description,
		&product.Composition,
		&product.StorageTime,
		&product.StorageInfo,
		&product.Manufacturer,
		&last_modified,
	)

	if err == sql.ErrNoRows {
		c.JSON(404, gin.H{
			"status": "error",
			"error":  "product not found",
		})
		return
	}

	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	if match := c.Request.Header.Get("If-Modified-Since"); match != "" {
		t, err := time.Parse(time.RFC1123, match)
		if err == nil && last_modified.Before(t.Add(1*time.Second)) {
			c.Status(304)
			return
		}
	}

	image_rows, err := db.Query(`SELECT image_url FROM product_images WHERE product_id = $1`, id)

	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}
	defer image_rows.Close()

	product.Images = make([]string, 0)
	for image_rows.Next() {
		var image string
		err := image_rows.Scan(&image)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
		}
		product.Images = append(product.Images, image)
	}

	if image_rows.Err() != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	bullet_rows, err := db.Query(`SELECT text FROM translated_bullet_points WHERE product_id = $1 AND language_id = $2`, id, lang)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
	}
	defer bullet_rows.Close()

	product.BulletPoints = make([]string, 0)
	for bullet_rows.Next() {
		var bullet string
		err := bullet_rows.Scan(&bullet)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
		}
		product.BulletPoints = append(product.BulletPoints, bullet)
	}

	if bullet_rows.Err() != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	// TODO: Nutricional values

	c.Writer.Header().Set("Last-Modified", last_modified.Format(time.RFC1123))
	c.JSON(200, gin.H{
		"status": "success",
		"data":   product,
	})
}

func supplierProducts(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "application/json")

	var id string = c.Param("supplier_id")
	_, err := uuid.Parse(id)
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid supplier id",
		})
		return
	}

	per_page, err := strconv.Atoi(c.DefaultQuery("per_page", "0"))
	if err != nil {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid per_page parameter",
		})
		return
	}

	page := c.DefaultQuery("page", "1")
	page_int, err := strconv.Atoi(page)
	if err != nil || page_int < 1 {
		c.JSON(400, gin.H{
			"status": "error",
			"error":  "invalid page parameter",
		})
		return
	}

	categories := c.QueryArray("categories")
	for _, category := range categories {
		_, err := uuid.Parse(category)
		if err != nil {
			c.JSON(400, gin.H{
				"status": "error",
				"error":  "invalid categories id",
			})
			return
		}
	}

	transaction, err := db.Begin()
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}
	defer transaction.Rollback()

	var last_updated time.Time
	err = transaction.QueryRow(`SELECT products_updated_at FROM public_suppliers WHERE id = $1`, id).Scan(&last_updated)
	switch err {
	case sql.ErrNoRows:
		c.JSON(404, gin.H{
			"status": "error",
			"error":  "supplier not found",
		})
		return
	case nil:
		break
	default:
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	if match := c.Request.Header.Get("If-Modified-Since"); match != "" {
		t, err := time.Parse(time.RFC1123, match)
		if err == nil && last_updated.Before(t.Add(1*time.Second)) {
			c.Status(304)
			return
		}
	}

	if len(categories) > 0 {
		_, err := transaction.Exec(`CREATE TEMPORARY TABLE temp_categories (id UUID PRIMARY KEY) ON COMMIT DROP`)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}

		query := `INSERT INTO temp_categories VALUES `

		for i := range categories {
			query += `($` + strconv.Itoa(i+1) + `)`

			if i != len(categories)-1 {
				query += `, `
			}
		}

		values := make([]interface{}, len(categories))
		for i, v := range categories {
			values[i] = v
		}

		_, err = transaction.Exec(query, values...)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})
			return
		}
	}

	query := `
		SELECT COUNT(DISTINCT product_id) FROM public_products_with_parent_categories
		WHERE supplier_id = $1
		`

	if len(categories) > 0 {
		query += `
			AND category_id IN (SELECT id FROM temp_categories)
			`
	}

	var total int
	err = transaction.QueryRow(query, id).Scan(&total)
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})
		return
	}

	offset := per_page * (page_int - 1)

	query = `
		SELECT
			DISTINCT product_id
		FROM public_products_with_parent_categories
		WHERE supplier_id = $1
		`

	if len(categories) > 0 {
		query += `
			AND category_id IN (SELECT id FROM temp_categories)
			`
	}

	if per_page > 0 {
		query += `
			LIMIT $2
			OFFSET $3
			`
	}

	var rows *sql.Rows
	if per_page > 0 {
		rows, err = transaction.Query(query, id, per_page, offset)
	} else {
		rows, err = transaction.Query(query, id)
	}
	if err != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}
	defer rows.Close()

	var products []string = make([]string, 0)
	for rows.Next() {
		var product string
		err := rows.Scan(&product)
		if err != nil {
			log.Print(err)
			c.JSON(500, gin.H{
				"status": "error",
				"error":  "internal server error",
			})

			return
		}
		products = append(products, product)
	}

	if rows.Err() != nil {
		log.Print(err)
		c.JSON(500, gin.H{
			"status": "error",
			"error":  "internal server error",
		})

		return
	}

	transaction.Commit()

	c.Writer.Header().Set("Last-Modified", last_updated.Format(time.RFC1123))
	c.Writer.Header().Set("X-Total-Count", strconv.Itoa(total))

	if per_page > 0 {
		totalPageCount := total / per_page
		if total%per_page != 0 {
			totalPageCount++
		}

		c.Writer.Header().Set("X-Total-Pages", strconv.Itoa(totalPageCount))
		c.Writer.Header().Set("X-Per-Page", strconv.Itoa(per_page))
		c.Writer.Header().Set("X-Current-Page", strconv.Itoa(page_int))

		link_params := ""
		if len(categories) > 0 {
			link_params += "&categories=" + strings.Join(categories, ",")
		}

		request_path := c.Request.Host + c.Request.URL.Path

		link := "<" + request_path + link_params + "?page=1&per_page=" + strconv.Itoa(per_page) + ">; rel=\"first\", "
		link += "<" + request_path + link_params + "?page=" + strconv.Itoa(totalPageCount) + "&per_page=" + strconv.Itoa(per_page) + ">; rel=\"last\""
		if page_int > 1 {
			link += ", <" + request_path + link_params + "?page=" + strconv.Itoa(page_int-1) + "&per_page=" + strconv.Itoa(per_page) + ">; rel=\"prev\""
		}

		if page_int < totalPageCount {
			link += ", <" + request_path + link_params + "?page=" + strconv.Itoa(page_int+1) + "&per_page=" + strconv.Itoa(per_page) + ">; rel=\"next\""
		}

		c.Writer.Header().Set("Link", link)
	}

	c.JSON(200, gin.H{
		"status": "success",
		"data":   products,
	})
}
